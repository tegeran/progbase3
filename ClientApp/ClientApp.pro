#-------------------------------------------------
#
# Project created by QtCreator 2018-05-03T20:24:03
#
#-------------------------------------------------

QT += network xml sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ClientApp
TEMPLATE = app

CONFIG += rtti
CONFIG += c++2a
QMAKE_CXXFLAGS = -std=c++2a -Wall -Wextra -Wno-variadic-macros


# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
        chat.cpp \
    main_client.cpp \
    main_stacked_widget.cpp \
    connecting_dialog.cpp \
    authorization_dialog.cpp \
    registration_dialog.cpp \
    user_color_setup_dialog.cpp \
    awaitable.cpp \
    clickable_rect.cpp \
    color_picker_rect.cpp \
    color_picker_dialog.cpp \
    pad_main_window.cpp \
    userlist_model.cpp \
    text_editor.cpp \
    pads_dialog.cpp \
    pads_model.cpp \
    creating_new_pad_dialog.cpp \
    editor_commands.cpp \
    plain_text_editor.cpp \
    revisions_viewer.cpp \
    user_pads_model.cpp \
    user_pads_dialog.cpp \
    admin_dialog.cpp


HEADERS += \
    ../stylesheet/stylesheet.h\
        chat.h \
    ../SharedLib/tcpsocket.h \
    ../SharedLib/tcpserver.h \
    ../SharedLib/std_ext.h \
    ../SharedLib/server_user.h \
    ../SharedLib/server_pad.h \
    ../SharedLib/range.h \
    ../SharedLib/qnet.h \
    ../SharedLib/qext.h \
    ../SharedLib/netpacket.h \
    ../SharedLib/message_exception.h \
    ../SharedLib/location.h \
    ../SharedLib/error.h \
    ../SharedLib/data.h \
    ../SharedLib/client_user.h \
    ../SharedLib/client_pad.h \
    main_client.h \
    main_stacked_widget.h \
    connecting_dialog.h \
    authorization_dialog.h \
    registration_dialog.h \
    user_color_setup_dialog.h \
    awaitable.h \
    clickable_rect.h \
    color_picker_rect.h \
    color_picker_dialog.h \
    pad_main_window.h \
    userlist_model.h \
    text_editor.h \
    pads_dialog.h \
    pads_model.h \
    creating_new_pad_dialog.h \
    editor_commands.h \
    plain_text_editor.h \
    revisions_viewer.h \
    user_pads_model.h \
    user_pads_dialog.h \
    admin_dialog.h



FORMS += \
        chat.ui \
    main_stacked_widget.ui \
    connecting_dialog.ui \
    authorization_dialog.ui \
    registration_dialog.ui \
    user_color_setup_dialog.ui \
    color_picker_dialog.ui \
    pad_main_window.ui \
    pads_dialog.ui \
    creating_new_pad_dialog.ui \
    revisions_viewer.ui \
    user_pads_dialog.ui \
    admin_dialog.ui

INCLUDEPATH += ../stylesheet

unix:!macx: LIBS += -L$$PWD/../SharedLib/build/ -lSharedLib

INCLUDEPATH += $$PWD/../SharedLib
DEPENDPATH += $$PWD/../SharedLib/build

RESOURCES += \
    icons.qrc
