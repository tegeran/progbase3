#include "admin_dialog.h"
#include "ui_admin_dialog.h"
#include "qext.h"
#include "clickable_rect.h"

AdminDialog::AdminDialog(MainClient * const & client,
						 const Client::User & targetUser,
						 QWidget * const & parent) :
	QDialog(parent),
	ui(new Ui::AdminDialog),
	m_client(client),
	m_targetUser(targetUser)
{
	ui->setupUi(this);
	setWindowTitle("Change user level");
	ui->lbl_targetUserLevel->setAlignment(Qt::AlignHCenter);
	ui->lbl_targetUserName->setAlignment(Qt::AlignHCenter);
	ui->lbl_targetUserLevel->setText(targetUser.levelName());
	ui->lbl_targetUserName->setText(targetUser.name());
	this->setModal(true);
	ui->crect_userColor->setFillColor(targetUser.color());
	ui->crect_userColor->setCursor(QCursor(Qt::ArrowCursor));

	Qext::Object::connectMultipleSignalsToSlots(
		client,
		this,
		{
			SIGNAL			  (updateUserChangedColor(Client::User)),
			SLOT(handle_client_updateUserChangedColor(Client::User)),

			SIGNAL	 (updateUserLeftPad(Client::User)),
			SLOT(handle_client_userLeft(Client::User)),

			SIGNAL    (userDisconnected(Client::User)),
			SLOT(handle_client_userLeft(Client::User)),

			SIGNAL			  (currentLevelChanged(Client::User,Client::User::Level)),
			SLOT(handle_client_currentLevelChanged()),

			SIGNAL			  (someUserChangedLevel(Client::User,Client::User,Client::User::Level)),
			SLOT(handle_client_someUserChangedLevel(Client::User,Client::User,Client::User::Level))
		}
	);
	Qext::Object::connectMultipleSignals(
		client,
		{
			SIGNAL(createdNewPad(Client::Pad)),
			SIGNAL(disconnectedFromPad()),
			SIGNAL(connectedToPad(Client::Pad,QHash<id_t,Client::User>,QString)),
			SIGNAL(disconnectedFromServer())
		},
		this,
		SLOT(handle_client_disconnected())
	);
	Qext::Dialog::closeOnFinished(this);
	updateLevelChanges();
	QLOG("created admin dialog");
}

AdminDialog::~AdminDialog(){
	delete ui;
	QLOG("deleted admin dialog");
}

void AdminDialog::handle_client_userLeft(const Client::User & leaver){
	QString message { QString("AHTUNG! User %1 left pad").arg(leaver.name()) };
	if (m_targetUser.id() == leaver.id()){
		connect(
			Qext::throwNewMessageBox(
				QMessageBox::Warning,
				message,
				message,
				QMessageBox::Close,
				this
			),
			SIGNAL(finished(int)),
			this,
			SLOT(reject())
		);
	}
}

void AdminDialog::handle_client_currentLevelChanged(){
	if (!updateLevelChanges()){
		connect(
			Qext::throwNewMessageBox(
				QMessageBox::Warning,
				"AHTUNG! You have no more rights...",
				"AHTUNT! You have no more rights to modify user's level",
				QMessageBox::Close,
				this
			),
			SIGNAL(finished(int)),
			this,
			SLOT(reject())
		);
	}
}

void AdminDialog::handle_client_someUserChangedLevel(const Client::User &,
													 const Client::User & targetUser,
													 const Client::User::Level &){
	if (targetUser.id() == m_targetUser.id()){
		m_targetUser = targetUser;
		updateLevelChanges();
		ui->lbl_targetUserLevel->setText(targetUser.levelName());
	}
}

void AdminDialog::handle_client_updateUserChangedColor(const Client::User & user){
	if (user.id() == m_targetUser.id()){
		m_targetUser = user;
		ui->crect_userColor->setFillColor(m_targetUser.color());
	}
}

void AdminDialog::handle_client_disconnected(){
	reject();
}

bool AdminDialog::updateLevelChanges(){
	if (m_client->clientUser().isOwner()){
		switch (m_targetUser.level()){
			case Client::User::Admin:{
				ui->pb_admin->hide();
				ui->pb_author->setText("Demote to author");
				ui->pb_guest->setText ("Demote to guest");
				ui->pb_author->show();
				ui->pb_guest->show();
				return true;
			}
			case Client::User::Author:{
				ui->pb_author->hide();
				ui->pb_admin->setText("Promote to admin");
				ui->pb_guest->setText("Demote to guest");
				ui->pb_admin->show();
				ui->pb_guest->show();
				return true;
			}
			case Client::User::Guest:{
				ui->pb_guest->hide();
				ui->pb_admin->setText("Promote to admin");
				ui->pb_author->setText("Promote to author");
				ui->pb_admin->show();
				ui->pb_author->show();
				return true;
			}
			default:{
				QSHUTDOWN("undefined behaviour");
			}
		}
	}
	if (m_client->clientUser().hasAdminRights()){
		switch (m_targetUser.level()){
			case Client::User::Author:{
				ui->pb_author->hide();
				ui->pb_admin->hide();
				ui->pb_guest->setText("Demote to guest");
				ui->pb_guest->show();
				return true;
			}
			case Client::User::Guest:{
				ui->pb_guest->hide();
				ui->pb_admin->hide();
				ui->pb_author->setText("Promote to author");
				ui->pb_author->show();
				return true;
			}
			default:{
				QSHUTDOWN("undefined behaviour");
			}
		}
	}
	return false;
}

void AdminDialog::on_pb_admin_clicked(){
	m_client->sendChangeUserLevelRequest(m_targetUser.id(), Client::User::Admin);
	reject();
}

void AdminDialog::on_pb_author_clicked(){
	m_client->sendChangeUserLevelRequest(m_targetUser.id(), Client::User::Author);
	reject();
}

void AdminDialog::on_pb_guest_clicked(){
	m_client->sendChangeUserLevelRequest(m_targetUser.id(), Client::User::Guest);
	reject();
}
