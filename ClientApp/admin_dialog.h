#ifndef ADMIN_DIALOG_H
#define ADMIN_DIALOG_H

#include <QDialog>
#include "main_client.h"

namespace Ui {
	class AdminDialog;
}

class AdminDialog : public QDialog
{
	Q_OBJECT

public:
	explicit AdminDialog(MainClient * const & client,
						 const Client::User & targetUser,
						 QWidget * const & parent = nullptr);
	~AdminDialog();

private slots:
	void handle_client_userLeft(const Client::User & leaver);
	void handle_client_currentLevelChanged();
	void handle_client_someUserChangedLevel(const Client::User &,
											const Client::User & targetUser,
											const Client::User::Level & newLevel);

	void handle_client_updateUserChangedColor(const Client::User & user);
	void handle_client_disconnected();
	void on_pb_admin_clicked();

	void on_pb_author_clicked();

	void on_pb_guest_clicked();

private:
	Ui::AdminDialog *ui;
	MainClient * const m_client;
	Client::User m_targetUser;

	bool updateLevelChanges();

};

#endif // ADMIN_DIALOG_H
