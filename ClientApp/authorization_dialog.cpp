#include <QRegularExpressionValidator>
#include <QRegularExpression>

#include "authorization_dialog.h"
#include "ui_authorization_dialog.h"
#include "client_user.h"
#include "qext.h"

AuthorizationDialog::AuthorizationDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::AuthorizationDialog)
{
	ui->setupUi(this);
	Qext::LineEdit::setValidator(ui->ledit_login,    Client::User::LOGIN_REG_EXP);
	Qext::LineEdit::setValidator(ui->ledit_password, Client::User::PASSWORD_REG_EXP);
	Qext::Object::connectMultipleSenders(
		{
			ui->ledit_login,
			ui->ledit_password
		},
		SIGNAL(textChanged(QString)),
		this,
		SLOT(tryEnableSignIn())
	);
	tryEnableSignIn();
}

AuthorizationDialog::~AuthorizationDialog()
{
	delete ui;
}

QString AuthorizationDialog::chosenLogin() const{
	return ui->ledit_login->text();
}

QString AuthorizationDialog::chosenPassword() const{
	return ui->ledit_password->text();
}

void AuthorizationDialog::tryEnableSignIn(){
	ui->pb_signIn->setEnabled(
		!Awaitable::isAwaiting()
		&& ui->ledit_login->hasAcceptableInput()
		&& ui->ledit_password->hasAcceptableInput()
	);
}


void AuthorizationDialog::on_pb_signIn_clicked(){
	emit signInRequest(chosenLogin(), chosenPassword());
}

void AuthorizationDialog::on_pb_newAccount_clicked(){
	emit newAccountRequest();
}

void AuthorizationDialog::startAwaiting(){
	ui->pb_signIn->setEnabled(false);
	ui->pb_newAccount->setEnabled(false);
}

void AuthorizationDialog::stopAwaiting(){
	tryEnableSignIn();
	ui->pb_signIn->setEnabled(true);
	ui->pb_newAccount->setEnabled(true);
}
