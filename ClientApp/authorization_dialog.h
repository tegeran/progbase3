#ifndef AUTHORIZATION_DIALOG_H
#define AUTHORIZATION_DIALOG_H

#include <QDialog>
#include <QLineEdit>

#include "awaitable.h"

namespace Ui {
	class AuthorizationDialog;
}

class AuthorizationDialog : public QDialog, public Awaitable
{
	Q_OBJECT

public:
	explicit AuthorizationDialog(QWidget *parent = 0);
	~AuthorizationDialog();
	QString chosenLogin() const;
	QString chosenPassword() const;

signals:
	void signInRequest(const QString & login, const QString & password);
	void newAccountRequest();

private slots:
	void tryEnableSignIn();

	void on_pb_signIn_clicked();

	void on_pb_newAccount_clicked();

private:
	Ui::AuthorizationDialog *ui;

	// Awaitable interface
protected:
	void startAwaiting() override;
	void stopAwaiting() override;
};

#endif // AUTHORIZATION_DIALOG_H
