#include "awaitable.h"
#include "error.h"


void Awaitable::await(){
	if (!m_isAwaiting){
		m_isAwaiting = true;
		startAwaiting();
	}
}

void Awaitable::enableInteraction(){
	if (m_isAwaiting){
		m_isAwaiting = false;
		stopAwaiting();
	}
}

bool Awaitable::isAwaiting() const{
	return m_isAwaiting;
}
