#ifndef AWAITABLE_H
#define AWAITABLE_H

class Awaitable {
public:
	virtual ~Awaitable() = default;
	void await();
	void enableInteraction();

protected:
	virtual void startAwaiting() = 0;
	virtual void stopAwaiting() = 0;

	bool isAwaiting() const;
private:
	bool m_isAwaiting = false;

};

#endif // AWAITABLE_H
