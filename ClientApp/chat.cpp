#include <QTextCursor>
#include <QPlainTextEdit>
#include <QTextCharFormat>
#include <QTextBlockFormat>
#include <QTextBlock>
#include <QTextDocument>
#include <QScrollBar>

#include "chat.h"
#include "ui_chat.h"
#include "error.h"
#include "qext.h"

using namespace Client;

Chat::Chat(QWidget * const &parent)
	: QWidget(parent),
	  ui(new Ui::Chat),
	  m_client(nullptr)
{
	ui->setupUi(this);
	ui->tedit_chat->setReadOnly(true);
	ui->tedit_chat->setUndoRedoEnabled(false);
}

Chat::~Chat(){
	delete ui;
}

void Chat::setMainClient(MainClient * const & client){
	m_client = client;
	Qext::Object::connectMultipleSignalsToSlots(
		m_client,
		this,
		{

			SIGNAL			  (disconnectedFromPad()),
			SLOT(handle_client_disconnectedFromPad()),

			SIGNAL			  (someUserChangedLevel(Client::User,Client::User,Client::User::Level)),
			SLOT(handle_client_someUserChangedLevel(Client::User,Client::User,Client::User::Level)),

			SIGNAL			  (currentLevelChanged(Client::User,Client::User::Level)),
			SLOT(handle_client_currentLevelChanged(Client::User,Client::User::Level)),

			SIGNAL			  (updateSomeUserRestoredPad(Client::User,QString)),
			SLOT(handle_client_updateSomeUserRestoredPad(Client::User,QString)),

			SIGNAL			  (restoredPad(QString)),
			SLOT(handle_client_restoredPad()),

			SIGNAL(userConnected(Client::User)),
			SLOT    (connectUser(Client::User)),

			SIGNAL(userDisconnected(Client::User)),
			SLOT    (disconnectUser(Client::User)),

			SIGNAL(receivedChatMessage(Client::User,QString)),
			SLOT      (sendUserMessage(Client::User,QString)),

			SIGNAL(deliveredChatMessage(Client::User,QString)),
			SLOT       (sendUserMessage(Client::User,QString)),

			SIGNAL            (updateUserLeftPad(Client::User)),
			SLOT(handle_client_updateUserLeftPad(Client::User)),

			SIGNAL(			   connectedToPad(Client::Pad,QHash<id_t,Client::User>,QString)),
			SLOT(handle_client_connectedToPad()),

			SIGNAL			  (createdNewPad(Client::Pad)),
			SLOT(handle_client_createdNewPad())
		}
	);
	connect(
		this,
		SIGNAL(sentMessage(QString)),
		m_client,
		SLOT(sendChatMessage(QString))
	);
	if (!m_client->isConnectedToPad()){
		Awaitable::await();
	}
}

void Chat::sendUserMessage(const User & user, const QString & message){
	bool barWasDown(isScrollBarDown());
	QTextCursor cursor(ui->tedit_chat->textCursor());
	cursor.movePosition(QTextCursor::End);
	if (cursor.position()){
		cursor.insertBlock();
	}
	cursor.setBlockFormat(user.textBlockFormat());

	QTextCharFormat fmt { user.textCharFormat() };
	fmt.setFontWeight(QFont::Bold);

	cursor.insertText(QString("%1: ").arg(user.name()), fmt);

	fmt.setFontWeight(QFont::Normal);
	cursor.insertText(message, fmt);
	if (barWasDown){ scrollDown(); }
}

void Chat::connectUser(const User &user){
	printInfoFor(user, tr("%1 has connected").arg(user.name()));
}

void Chat::disconnectUser(const User & user){
	printInfoFor(user, tr("%1 has disconnected").arg(user.name()));
}

void Chat::handle_client_someUserChangedLevel(const User & initiator,
											  const User & target,
											  const User::Level &){
	printInfoFor(initiator, tr("%1 has set %2 level to %3")
				 .arg(initiator.name()).arg(target.name())
				 .arg(target.levelName()));
}

void Chat::handle_client_currentLevelChanged(const User & initiator,
											 const User::Level & newLevel){
	handle_client_someUserChangedLevel(initiator, m_client->clientUser(), newLevel);
}

void Chat::handle_client_updateUserLeftPad(const User & user){
	printInfoFor(user, tr("%1 has left").arg(user.name()));
}

void Chat::handle_client_connectedToPad(){
	ui->tedit_chat->clear();
	printInfoFor(
		m_client->clientUser(),
		QString("%1 has connected").arg(m_client->clientUser().name())
	);
	Awaitable::enableInteraction();
}

void Chat::handle_client_createdNewPad(){
	ui->tedit_chat->clear();
	printInfoFor(
		m_client->clientUser(),
		QString("%1 welcome to your new pad").arg(m_client->clientUser().name())
	);
	Awaitable::enableInteraction();
}

void Chat::handle_client_restoredPad(){
	printInfoFor(
		m_client->clientUser(),
		tr("%1 has restored pad").arg(m_client->clientUser().name())
				);
}

void Chat::handle_client_disconnectedFromPad(){
	Awaitable::await();
}

void Chat::handle_client_updateSomeUserRestoredPad(
	const User & user,
	const QString &){
	printInfoFor(user, tr("%1 restored pad").arg(user.name()));
}

void Chat::printInfoFor(const User & user, const QString & info){
	bool barWasDown(isScrollBarDown());
	QTextCursor cursor(ui->tedit_chat->textCursor());
	cursor.movePosition(QTextCursor::End);
	if (cursor.position()){
		cursor.insertBlock();
	}
	cursor.setBlockFormat(user.textBlockFormat());

	QTextCharFormat fmt { user.textCharFormat() };
	fmt.setFontItalic(true);
	cursor.insertText(info, fmt);
	if (barWasDown) { scrollDown(); }
}


void Chat::on_ledit_input_returnPressed(){
	if (!m_client->isConnectedToPad()){
		return;
	}
	QString msg(ui->ledit_input->text());
	if (msg.isEmpty()){ return; }
	emit sentMessage(msg);
	ui->ledit_input->clear();
}

void Chat::on_pb_send_clicked(){ on_ledit_input_returnPressed(); }

void Chat::scrollDown(){
	QScrollBar * bar(ui->tedit_chat->verticalScrollBar());
	bar->setValue(bar->maximum());
}

bool Chat::isScrollBarDown(){
	QScrollBar * bar(ui->tedit_chat->verticalScrollBar());
	return bar->value() == bar->maximum();
}

void Chat::startAwaiting(){
//	ui->ledit_input->setReadOnly(true);
	ui->pb_send->setEnabled(false);
}

void Chat::stopAwaiting(){
//	ui->ledit_input->setReadOnly(false);
	ui->pb_send->setEnabled(true);
}
