#ifndef CHAT_H
#define CHAT_H

#include <QWidget>
#include <QTextCursor>

#include "client_user.h"
#include "main_client.h"
#include "awaitable.h"

namespace Ui {
	class Chat;
}

class Chat : public QWidget, public Awaitable {
	Q_OBJECT

public:
	explicit Chat(QWidget * const &parent = nullptr);
	~Chat();

	void setMainClient(MainClient * const & client);

signals:
	void sentMessage(QString message);
public slots:
	void sendUserMessage(const Client::User &user, const QString &message);
	void connectUser(const Client::User &user);
	void disconnectUser(const Client::User &user);
private slots:
	void handle_client_someUserChangedLevel(const Client::User & initiator,
											const Client::User & target,
											const Client::User::Level & newLevel);
	void handle_client_currentLevelChanged(const Client::User & initiator, const Client::User::Level & newlevel);
	void handle_client_updateUserLeftPad(const Client::User & user);
	void handle_client_connectedToPad();
	void handle_client_createdNewPad();
	void handle_client_restoredPad();
	void handle_client_disconnectedFromPad();

	void handle_client_updateSomeUserRestoredPad(
		const Client::User & user,
		const QString &
	);

	void on_ledit_input_returnPressed();

	void on_pb_send_clicked();
private:
	Ui::Chat * ui;
	MainClient * m_client;

	void printInfoFor(const Client::User & user, const QString & info);
	void scrollDown();
	bool isScrollBarDown();

	// Awaitable interface
protected:
	void startAwaiting() override;
	void stopAwaiting() override;
};

#endif // CHAT_H
