#include <QPaintEvent>
#include <QPainter>
#include <QPen>

#include <utility>

#include "clickable_rect.h"
#include "std_ext.h"

ClickableRect::ClickableRect(QWidget *parent,
							 QColor && fillColor,
							 QColor && borderColor,
							 const qreal & borderAreaPercent)
	: QWidget(parent),
	  m_fillColor(std::move(fillColor)),
	  m_borderColor(borderColor),
	  m_borderAreaPercent(borderAreaPercent)
{
	QWidget::setCursor(Qt::PointingHandCursor);
}

void ClickableRect::setFillColor(const QColor & color){
	m_fillColor = color;
	QWidget::repaint();
}

void ClickableRect::setBorderColor(const QColor & color){
	m_borderColor = color;
	QWidget::repaint();
}

const QColor &ClickableRect::fillColor() const{
	return m_fillColor;
}

const QColor & ClickableRect::borderColor() const{
	return m_borderColor;
}

void ClickableRect::mousePressEvent(QMouseEvent * event){
	QWidget::mousePressEvent(event);
	emit clicked();
}

void ClickableRect::paintEvent(QPaintEvent * event){
	QPainter painter(this);
	QRect rect { event->rect() };
	painter.setRenderHint(QPainter::Antialiasing);
	painter.fillRect(rect, m_fillColor);
	painter.setPen(
		QPen(
			m_borderColor,
			std_ext::average(
				static_cast<qreal>(rect.height()),
				static_cast<qreal>(rect.width())
			) * m_borderAreaPercent
		)
	);
	painter.drawRect(rect);
}
