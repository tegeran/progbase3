#ifndef CLICKABLE_RECT_H
#define CLICKABLE_RECT_H

#include <QWidget>
#include <QColor>
#include <QPen>

#include "client_user.h"

class ClickableRect : public QWidget
{
	Q_OBJECT
public:
	explicit ClickableRect(QWidget *parent = nullptr,
						   QColor && fillColor = QColor(Client::User::DEFAULT_COLOR),
						   QColor && borderColor = Qt::black,
						   const qreal & borderAreaPercent = 0.04);

signals:
	void clicked();
public slots:
	void setFillColor(const QColor & color);
	void setBorderColor(const QColor & color);

	const QColor &fillColor() const;
	const QColor &borderColor() const;

	// QWidget interface
protected:
	void mousePressEvent(QMouseEvent * event) override;
	void paintEvent(QPaintEvent * event) override;
private:
	QColor m_fillColor;
	QColor m_borderColor;
	qreal m_borderAreaPercent;
};

#endif // CLICKABLE_RECT_H
