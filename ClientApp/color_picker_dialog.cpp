#include "color_picker_dialog.h"
#include "ui_color_picker_dialog.h"
#include "qext.h"

ColorPickerDialog::ColorPickerDialog(QWidget * const & parent) :
	QDialog(parent),
	ui(new Ui::ColorPickerDialog)
{
	ui->setupUi(this);
	QDialog::setWindowTitle("Color picker");
	ui->crect_blue->   setFillColor(QColor::fromRgb(105, 82,  164));
	ui->crect_cyan->   setFillColor(QColor::fromRgb(154, 219, 248));
	ui->crect_green->  setFillColor(Qt::green);
	ui->crect_orange-> setFillColor(QColor::fromRgb(255, 185, 91));
	ui->crect_pink->   setFillColor(QColor::fromRgb(244, 66, 241));
	ui->crect_purple-> setFillColor(QColor::fromRgb(101, 43, 145));
	ui->crect_red->    setFillColor(QColor::fromRgb(232, 58, 58));
	ui->crect_yellow-> setFillColor(Qt::yellow);

	Qext::Object::connectMultipleSenders(
		{
			ui->crect_blue,
			ui->crect_cyan,
			ui->crect_green,
			ui->crect_orange,
			ui->crect_pink,
			ui->crect_purple,
			ui->crect_red,
			ui->crect_yellow
		},
		SIGNAL(clicked()),
		this,
		SLOT(handle_colorRect_clicked())
	);
}

ColorPickerDialog::~ColorPickerDialog(){
	delete ui;
}


void ColorPickerDialog::handle_colorRect_clicked(){
	QDialog::close();
	emit choseColor(QSENDER(ClickableRect *)->fillColor());
}
