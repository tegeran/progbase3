#ifndef COLOR_PICKER_DIALOG_H
#define COLOR_PICKER_DIALOG_H

#include <QDialog>

namespace Ui {
	class ColorPickerDialog;
}

class ColorPickerDialog : public QDialog{
	Q_OBJECT
public:
	explicit ColorPickerDialog(QWidget * const & parent = nullptr);
	~ColorPickerDialog();

signals:
	void choseColor(const QColor & color);
private slots:
	void handle_colorRect_clicked();

private:
	Ui::ColorPickerDialog *ui;
};

#endif // COLOR_PICKER_DIALOG_H
