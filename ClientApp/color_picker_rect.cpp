#include "color_picker_rect.h"


ColorPickerRect::ColorPickerRect(QWidget * parent, QColor && initColor)
	: ClickableRect(parent, std::move(initColor)),
	  m_colorPicker(new ColorPickerDialog(this))
{
	m_colorPicker->setModal(true);
	connect(
		this,
		SIGNAL(clicked()),
		this,
		SLOT(handle_super_clicked())
	);
	connect(
		m_colorPicker,
		SIGNAL(choseColor(QColor)),
		this,
		SLOT(handle_colorPicker_choseColor(QColor))
	);
}


ColorPickerRect::~ColorPickerRect(){}

void ColorPickerRect::setMainClient(MainClient * const & client){
	ClickableRect::setFillColor(client->clientUser().color());
	connect(
		this,
		SIGNAL(colorChanged(QColor)),
		client,
		SLOT(sendColorChangeRequest(QColor))
	);
	connect(
		client,
		SIGNAL(changedColor(QColor)),
		this,
		SLOT(handle_client_changedColor(QColor))
	);
}

void ColorPickerRect::handle_super_clicked(){
	m_colorPicker->show();
}

void ColorPickerRect::handle_colorPicker_choseColor(const QColor & color){
	ClickableRect::setFillColor(color);
	emit colorChanged(color);
}

void ColorPickerRect::handle_client_changedColor(const QColor & color){
	ClickableRect::setFillColor(color);
}
