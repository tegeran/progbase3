﻿#ifndef COLOR_PICKER_RECT_H
#define COLOR_PICKER_RECT_H

#include <QWidget>
#include "client_user.h"
#include "clickable_rect.h"
#include "color_picker_dialog.h"
#include "main_client.h"


class ColorPickerRect : public ClickableRect{
	Q_OBJECT

public:
	explicit ColorPickerRect(QWidget *parent = 0,
							 QColor && initColor = QColor(Client::User::DEFAULT_COLOR));
	~ColorPickerRect();
	void setMainClient(MainClient * const &client);

signals:
	void colorChanged(const QColor & color);

private slots:
	void handle_super_clicked();
	void handle_colorPicker_choseColor(const QColor & color);
	void handle_client_changedColor(const QColor & color);
private:
	ColorPickerDialog * m_colorPicker;
};

#endif // COLOR_PICKER_RECT_H
