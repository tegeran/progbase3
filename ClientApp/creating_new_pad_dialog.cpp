#include "creating_new_pad_dialog.h"
#include "ui_creating_new_pad_dialog.h"
#include "qext.h"
#include "client_pad.h"

CreatingNewPadDialog::CreatingNewPadDialog(MainClient * const & client,
										   TextEditor * const & editor,
										   QWidget * const & parent) :
	QDialog(parent),
	ui(new Ui::CreatingNewPadDialog),
	m_client(client),
	m_editor(editor)
{
	ui->setupUi(this);
	Qext::LineEdit::setValidator(ui->ledit_padname, Client::Pad::NAME_REG_EXP);
	validateCreateButton();
	connect(
		ui->ledit_padname,
		SIGNAL(textChanged(QString)),
		this,
		SLOT(validateCreateButton())
	);
	Qext::Dialog::closeOnFinished(this);
	Qext::Object::connectMultipleSignals(
		m_client,
		{
			SIGNAL(connectedToPad(Client::Pad,QHash<id_t,Client::User>,QString)),
			SIGNAL(createdNewPad(Client::Pad)),
			SIGNAL(disconnectedFromServer())
		},
		this,
		SLOT(reject())
	);
}

CreatingNewPadDialog::~CreatingNewPadDialog(){
	delete ui;
}

void CreatingNewPadDialog::validateCreateButton(){
	ui->pb_create->setEnabled(
		!Awaitable::isAwaiting()
		&& ui->ledit_padname->hasAcceptableInput()
	);
}

void CreatingNewPadDialog::on_pb_create_clicked(){
	m_client->createNewPad(
		ui->ledit_padname->text(),
		ui->cbox_isPublic->isChecked(),
		m_editor->prepareDocumentForNewPad()
	);
}

void CreatingNewPadDialog::on_pb_cancel_clicked(){
	QDialog::reject();
}

void CreatingNewPadDialog::startAwaiting(){
	Qext::Widget::disableWidgets(ui->pb_create, ui->pb_cancel);
}

void CreatingNewPadDialog::stopAwaiting(){
	validateCreateButton();
	ui->pb_cancel->setEnabled(true);
}
