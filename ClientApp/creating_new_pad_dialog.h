#ifndef CREATING_NEW_PAD_DIALOG_H
#define CREATING_NEW_PAD_DIALOG_H

#include <QDialog>
#include <QTextDocument>

#include "awaitable.h"
#include "main_client.h"
#include "text_editor.h"

namespace Ui {
	class CreatingNewPadDialog;
}

class CreatingNewPadDialog : public QDialog, public Awaitable{
	Q_OBJECT

public:
	explicit CreatingNewPadDialog(
			MainClient * const & client,
			TextEditor * const & editor,
			QWidget * const & parent = nullptr
	);
	~CreatingNewPadDialog();

private slots:
	void validateCreateButton();

	void on_pb_create_clicked();
	void on_pb_cancel_clicked();

private:
	Ui::CreatingNewPadDialog *ui;
	MainClient * const m_client;
	TextEditor * const m_editor;

	// Awaitable interface
protected:
	void startAwaiting() override;
	void stopAwaiting() override;
};

#endif // CREATING_NEW_PAD_DIALOG_H
