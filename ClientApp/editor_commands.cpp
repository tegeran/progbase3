#include <QTextCursor>

#include "editor_commands.h"
#include "qext.h"

namespace EditorCommand {

AddCharacters::AddCharacters(TextEditor * const & editor,
							 const int & position,
							 const int & amount)
	: m_editor(editor), m_position(position), m_amount(amount){
	QTextCursor cursor { editor->document() };
	cursor.setPosition(position);
	cursor.anchor();
	cursor.setPosition(position + amount);
	m_fragment = cursor.selection();
}


void AddCharacters::undo(){
	Qext::TextDocument::removeText(m_editor->document(), m_position, m_amount);
	m_editor->m_client->sendCharactersRemoved(m_position, m_amount);
}

void AddCharacters::redo(){
	int lastIndexInDoc {
		Qext::TextDocument::lastIndex(m_editor->document())
	};
	if (m_position > lastIndexInDoc){
		m_position = lastIndexInDoc;
	}
	Qext::TextDocument::insertFragment(
		m_editor->document(),
		m_fragment,
		m_position
	);
	m_editor->m_client->sendCharactersAdded(m_position, m_fragment);
	isDone = true;
}

void AddCharacters::handle_editor_realContentsChange(const int & position,
													 const int & charsRemoved,
													 const int & charsAdded){
	if (position > m_position + m_amount){
		return;
	}
	if (isDone){
		if (charsRemoved){
			if (position + charsRemoved >= m_position ){
				QUndoCommand::setObsolete(true);
			} else {
				m_position -= charsRemoved;
			}
		}
		if (charsAdded){
			if (position >= m_position){
				QUndoCommand::setObsolete(true);
			} else {
				m_position += charsAdded;
			}
		}
	} else {
		m_position += charsAdded - charsRemoved;
	}
}













}
