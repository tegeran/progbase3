#ifndef EDITOR_COMMANDS_H
#define EDITOR_COMMANDS_H

#include <QUndoCommand>
#include <QTextDocumentFragment>
#include <QObject>

#include "text_editor.h"

class TextEditor;

namespace EditorCommand{

	class AddCharacters : public QObject, public QUndoCommand{
		Q_OBJECT
	public:
		AddCharacters(TextEditor * const & editor,
					  const int & position,
					  const int & amount);
		~AddCharacters() = default;

		void undo() override;
		void redo() override;
	private slots:
		void handle_editor_realContentsChange(
			const int & position,
			const int & charsRemoved,
			const int & charsAdded
		);

	private:
		TextEditor * m_editor;
		int m_position;
		int m_amount;
		QTextDocumentFragment m_fragment;
		bool isDone;
	};

	class RemoveCharacters : public QUndoCommand{
	public:
		RemoveCharacters(TextEditor * const & editor,
						 const int & position,
						 const int & amount);



		// QUndoCommand interface
	public:/*
		void undo() override;
		void redo() override;*/
	};



}

#endif // EDITOR_COMMANDS_H
