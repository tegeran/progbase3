#include <QApplication>

#include "main_stacked_widget.h"
#include "user_color_setup_dialog.h"
#include "pad_main_window.h"
#include "stylesheet.h"

int main(int argc, char * argv[]){
	QApplication app(argc, argv);
	app.setStyleSheet(GLOBAL_STYLESHEET);
	app.setApplicationDisplayName("CommonPad");

	MainStackedWidget mainWidget;
	mainWidget.show();
	return app.exec();
}
