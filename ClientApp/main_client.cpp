﻿#include "main_client.h"
#include <QTcpSocket>
#include "error.h"
#include "std_ext.h"
#include "qext.h"
#include "data.h"

MainClient::MainClient(QObject * const & parent)
	: QObject(parent), m_socket(new TcpSocket(this))
{
	connect(
		m_socket,
		SIGNAL(receivedNetpacket(Netpacket)),
		this,
		SLOT(handle_socket_receivedNetpacket(Netpacket))
	);
	connect(
		m_socket,
		SIGNAL(error(QAbstractSocket::SocketError)),
		this,
		SLOT(handle_socket_error(QAbstractSocket::SocketError))
	);
	connect(
		m_socket,
		SIGNAL(connected()),
		this,
		SLOT(handle_socket_connected())
	);
	connect(
		m_socket,
		SIGNAL(disconnected()),
		this,
		SLOT(handle_socket_disconnected())
	);
}


void MainClient::handle_socket_connected(){
	emit connectedToServer();
}

MainClient::~MainClient(){
	closeConnection();
}

void MainClient::openConnection(const QHostAddress & serverAddress,
								const quint16 & port)
{
	m_socket->connectToHost(serverAddress, port, QTcpSocket::ReadWrite);
	m_socket->setSocketOption(QAbstractSocket::LowDelayOption, QVariant(true));
}

void MainClient::closeConnection(){
	if (m_socket->isOpen()) {
		if (m_socket->isConnected()){
			m_socket->disconnectFromHost();
		}
		m_socket->close();
	}
}


void MainClient::handle_socket_error(const QAbstractSocket::SocketError & errorCode){
	QLOG("socket error -> " << m_socket->error());
	emit socketError(
		tr("error code %1 (%2)")
			.arg(errorCode)
				.arg(m_socket->errorString())
	);
}

void MainClient::handle_socket_disconnected(){
	emit disconnectedFromServer();
}

const Client::User & MainClient::userOnlineFromId(const id_t & id) const{
	try {
		return std_ext::tryFindReference(m_usersInPad, id);
	}
	catch (const MessageException &){
		QLOG_ERROR("failed to find user with id " << id);
		throw MessageException("failed to find user with specified id");
	}
}



void MainClient::handle_socket_receivedNetpacket(const Netpacket & netpacket){
	try {
		switch (netpacket.type()){
			case Netpacket::Answer_Authorization:
			{ processAnswer_Authorization(netpacket);            return; }

			case Netpacket::Update_NewChatMessage:
			{ processUpdate_NewChatMessage(netpacket);           return; }

			case Netpacket::Update_UserDisconnected:
			{ processUpdate_UserDisconnected(netpacket);         return; }

			case Netpacket::Update_UserConnectedToPad:
			{ processUpdate_UserConnectedToPad(netpacket);       return; }

			case Netpacket::Answer_DeliveredChatMessage:
			{ processAnswer_DeliveredChatMessage(netpacket);     return; }

			case Netpacket::Answer_Registration:
			{ processAnswer_Registration(netpacket);		     return; }

			case Netpacket::Answer_Error:
			{ processAnswer_Error(netpacket);		             return; }

			case Netpacket::Answer_RegistrationError:
			{ processAnswer_RegistrationError(netpacket);        return; }

			case Netpacket::Answer_AuthorizationError:
			{ processAnswer_AuthorizationError(netpacket);       return; }

			case Netpacket::Answer_AddedCharacters:
			{ processAnswer_AddedCharacters(netpacket);          return; }

			case Netpacket::Answer_RemovedCharacters:
			{ processAnswer_RemovedCharacters(netpacket);        return; }

			case Netpacket::Update_AddedCharacters:
			{ processUpdate_AddedCharacters(netpacket);          return; }

			case Netpacket::Update_RemovedCharacters:
			{ processUpdate_RemovedCharacters(netpacket);        return; }

			case Netpacket::Answer_CreatedNewPad:
			{ processAnswer_CreatedNewPad(netpacket);            return; }

			case Netpacket::Update_AddedPadOnline:
			{ processUpdate_AddedPadOnline(netpacket);           return; }

			case Netpacket::Update_RemovedPadOnline:
			{ processUpdate_RemovedPadOnline(netpacket);         return; }

			case Netpacket::Answer_ConnectedToPad:
			{ processAnswer_ConnectedToPad(netpacket);           return; }

			case Netpacket::Answer_ChangedColor:
			{ processAnswer_ChangedColor(netpacket);             return; }

			case Netpacket::Update_UserLeftPad:
			{ processUpdate_UserLeftPad(netpacket);              return; }

			case Netpacket::Update_UserChangedColor:
			{ processUpdate_UserChangedColor(netpacket);         return; }

			case Netpacket::Answer_AvailableRevisions:
			{ processAnswer_AvailableRevisions(netpacket);       return; }

			case Netpacket::Answer_RevisionsRange:
			{ processAnswer_RevisionsRange(netpacket);		     return; }

			case Netpacket::Answer_RestoredPad:
			{ processAnswer_RestoredPad(netpacket);			     return; }

			case Netpacket::Update_RestoredPad:
			{ processUpdate_RestoredPad(netpacket);				 return; }

			case Netpacket::Answer_FirstDocRevision:
			{ processAnswer_FirstDocVersionRevisions(netpacket); return; }

			case Netpacket::Answer_AddingCharsError:
			{ processAnswer_AddingCharsError(netpacket);		 return; }

			case Netpacket::Answer_RemovingCharsError:
			{ processAnswer_RemovingCharsError(netpacket);		 return; }

			case Netpacket::Update_ChangedUserLevel:
			{ processUpdate_ChangedUserLevel(netpacket);		 return; }

			case Netpacket::Update_ForeignChangeUserLevel:
			{ processUpdate_ForeignChangeUserLevel(netpacket);   return; }

			case Netpacket::Answer_DisconnectedFromPad:
			{ processAnswer_DisconnectedFromPad(netpacket);      return; }

			default: {
				QLOG_ERROR("undefined packet type received: " << netpacket.type());
				return;
			}
		}
	} catch (const MessageException & exception){
		QLOG_ERROR("received invalid packet");
	}
}



void MainClient::processUpdate_NewChatMessage(const Netpacket & netpacket){
	Data::IdAndString userIdAndMsg {
		netpacket.dataPacket<Data::IdAndString>()
	};
	try {
		emit receivedChatMessage(
			std_ext::tryFindReference(m_usersInPad, userIdAndMsg.id),
			userIdAndMsg.string
		);
	} catch (const MessageException &){
		emit receivedChatMessage(
			Client::User("<unknown_user>"),
			userIdAndMsg.string
		);
	}
}

void MainClient::processAnswer_DeliveredChatMessage(const Netpacket & netpacket){
	emit deliveredChatMessage(m_user, netpacket.dataPacket<Data::String>().string);
}

void MainClient::processUpdate_UserDisconnected(const Netpacket & netpacket){
	Data::Id userIdData {
		netpacket.dataPacket<Data::Id>()
	};

	if (!m_usersInPad.contains(userIdData.id)){
		QLOG_ERROR("invalid user disconnected");
		return;
	}
	emit userDisconnected(m_usersInPad.take(userIdData.id));
}

void MainClient::processUpdate_UserConnectedToPad(const Netpacket & netpacket){
	Client::User connectedUser { netpacket.dataPacket<Data::ClientUser>().user };
	m_usersInPad.insert(connectedUser.id(), connectedUser); /* undocumented return value */
	emit userConnected(connectedUser);
}

void MainClient::processUpdate_AddedCharacters(const Netpacket & netpacket){
	Data::IndexAndTextFragment dataPacket {
		netpacket.dataPacket<Data::IndexAndTextFragment>()
	};
	emit updateCharactersAdded(dataPacket.index, dataPacket.fragment);
}

void MainClient::processUpdate_RemovedCharacters(const Netpacket & netpacket){
	Data::IndexAndAmount dataPacket {
		netpacket.dataPacket<Data::IndexAndAmount>()
	};
	emit updateCharactersRemoved(dataPacket.index, dataPacket.amount);
}

void MainClient::processAnswer_AddedCharacters(const Netpacket & netpacket){
	processUpdate_AddedCharacters(netpacket);
}

void MainClient::processAnswer_RemovedCharacters(const Netpacket & netpacket){
	processUpdate_RemovedCharacters(netpacket);
}

void MainClient::processUpdate_AddedPadOnline(const Netpacket & netpacket){
	Client::Pad pad { netpacket.dataPacket<Data::ClientPad>().pad };
	m_padsOnline.insert(pad.id(), pad);
	emit updateAddedPadOnline(pad);
}

void MainClient::processUpdate_RemovedPadOnline(const Netpacket & netpacket){
	// SHOULD CHECK FOR REMOVAL
	Data::Id dataPacket { netpacket.dataPacket<Data::Id>() };
	if (!m_padsOnline.contains(dataPacket.id)){
		QLOG_ERROR("failed to remove unknow pad under id " << dataPacket.id);
		return;
	}
	emit updateRemovedPadOnline(m_padsOnline.take(dataPacket.id));
}

void MainClient::processUpdate_UserLeftPad(const Netpacket & netpacket){
	Data::Id dataPacket {
		netpacket.dataPacket<Data::Id>()
	};
	emit updateUserLeftPad(m_usersInPad.take(dataPacket.id));
}

void MainClient::processUpdate_UserChangedColor(const Netpacket & netpacket){
	Data::IdAndColor dataPacket {
		netpacket.dataPacket<Data::IdAndColor>()
	};
	Client::User & user {
		*m_usersInPad.find(dataPacket.id)
	};
	user.setColor(std::move(dataPacket.color));
	emit updateUserChangedColor(user);
}

void MainClient::processAnswer_CreatedNewPad(const Netpacket & netpacket){
	m_currentPad = netpacket.dataPacket<Data::ClientPad>().pad;
	m_padsOnline.insert(m_currentPad.id(), m_currentPad);
	m_user.promoteToOwner();
	m_ownerPads.push_front(m_currentPad);
	emit createdNewPad(m_currentPad);
}

void MainClient::processAnswer_ConnectedToPad(const Netpacket & netpacket){
	Data::PadEnterData dataPacket {
		netpacket.dataPacket<Data::PadEnterData>()
	};
	m_user.setLevel(dataPacket.level);
	m_usersInPad = std::move(dataPacket.users);
	try {
		m_currentPad = std_ext::tryFindReference(m_padsOnline, dataPacket.id);
	} catch (const MessageException &){
		id_t targetId(dataPacket.id);
		const auto predicate([&targetId](const Client::Pad & pad){return pad.id() == targetId;});
		try {
			m_currentPad = std_ext::tryFindReferenceIf(m_ownerPads, predicate);
		} catch (const MessageException &){
			try {
				m_currentPad = std_ext::tryFindReferenceIf(m_adminPads, predicate);
			} catch (const MessageException &){
				m_currentPad = std_ext::tryFindReferenceIf(m_authorPads, predicate);
			}	// ^~~ this must not fail, abort workflow otherwise ~~^
		}
		m_padsOnline.insert(m_currentPad.id(), m_currentPad);
	}
	emit connectedToPad(m_currentPad, m_usersInPad, dataPacket.documentHtml);
}

void MainClient::processAnswer_DisconnectedFromPad(const Netpacket &){
	m_currentPad = Client::Pad::nullobj();
	m_user.setLevel(Client::User::Owner);
	emit disconnectedFromPad();
}

void MainClient::processAnswer_Authorization(const Netpacket & netpacket){
	Data::UserAndAllPads dataPacket { netpacket.dataPacket<Data::UserAndAllPads>() };
	m_user		 = std::move(dataPacket.user);
	m_padsOnline = std::move(dataPacket.padsOnline);
	m_ownerPads  = std::move(dataPacket.ownerPads);
	m_adminPads  = std::move(dataPacket.adminPads);
	m_authorPads = std::move(dataPacket.authorPads);
	emit authorized(m_user, m_padsOnline);
}

void MainClient::processAnswer_Registration(const Netpacket & netpacket){
	Data::UserAndAllPads dataPacket { netpacket.dataPacket<Data::UserAndAllPads>() };
	m_user		 = std::move(dataPacket.user);
	m_padsOnline = std::move(dataPacket.padsOnline);
	m_ownerPads  = std::move(dataPacket.ownerPads);
	m_adminPads  = std::move(dataPacket.adminPads);
	m_authorPads = std::move(dataPacket.authorPads);
	emit registered(m_user, m_padsOnline);
}

void MainClient::processAnswer_Error(const Netpacket & netpacket){
	QString errorString { netpacket.dataPacket<Data::String>().string };
	QLOG_ERROR("server error report -> " << errorString);
	emit receivedErrorReport(errorString);
}

void MainClient::processAnswer_RegistrationError(const Netpacket & netpacket){
	emit registrationError(netpacket.dataPacket<Data::String>().string);
}

void MainClient::processAnswer_AuthorizationError(const Netpacket & netpacket){
	emit authorizationError(netpacket.dataPacket<Data::String>().string);
}

void MainClient::processAnswer_ChangedColor(const Netpacket & netpacket){
	m_user.setColor(netpacket.dataPacket<Data::Color>().color);
	emit changedColor(m_user.color());
}

void MainClient::processAnswer_AvailableRevisions(const Netpacket & netpacket){
	emit receivedAvailableRevisions(netpacket.dataPacket<Data::Integer<timeid_t>>().integer);
}

void MainClient::processAnswer_FirstDocVersionRevisions(const Netpacket & netpacket){
	Data::DocumentAndRevisionsRange dataPacket{
		netpacket.dataPacket<Data::DocumentAndRevisionsRange>()
	};
	emit receivedFirstDocVersionRevisions(
		dataPacket.documentHtml,
		dataPacket.revisions.firstTimeId,
		dataPacket.revisions.lastTimeId,
		dataPacket.revisions.commands
	);
}

void MainClient::processAnswer_AddingCharsError(const Netpacket & netpacket){
	QLOG_ERROR("Add chars error "
			   << netpacket.dataPacket<Data::String>().string);
}

void MainClient::processAnswer_RemovingCharsError(const Netpacket & netpacket){
	QLOG_ERROR("Removing chars error "
			   << netpacket.dataPacket<Data::String>().string);
}

void MainClient::processUpdate_ChangedUserLevel(const Netpacket & netpacket){
	Data::SenderIdAnd<Data::IdAndLevel> dataPacket{
		netpacket.dataPacket<Data::SenderIdAnd<Data::IdAndLevel>>()
	};
	if (dataPacket.data.id == m_user.id()){
		if (!m_user.isGuest()){
			takeUserPad(m_currentPad.id());
		}
		m_user.setLevel(dataPacket.data.level);
		if (dataPacket.data.level != Client::User::Guest){
			addUserPad(m_currentPad, dataPacket.data.level);
		}
		try {
			emit currentLevelChanged(
				std_ext::tryFindReference(m_usersInPad, dataPacket.id),
				dataPacket.data.level
			);
		} catch (const MessageException &){
			QLOG_ERROR("undefined user changed client's user level");
			emit currentLevelChanged(
				Client::User("<undefined_user>"),
				dataPacket.data.level
			);
		}
	} else {
		auto targetUser { m_usersInPad.find(dataPacket.data.id) };
		if (targetUser == m_usersInPad.end()){
			QLOG_ERROR("undefined behaviour");
			return;
		}
		targetUser->setLevel(dataPacket.data.level);
		emit someUserChangedLevel(
			std_ext::tryFindReference(m_usersInPad, dataPacket.id),
			*targetUser,
			dataPacket.data.level
		);
	}
}

void MainClient::processUpdate_ForeignChangeUserLevel(const Netpacket & netpacket){
	Data::SenderIdAnd<Data::IdAndLevel> dataPacket{
		netpacket.dataPacket<Data::SenderIdAnd<Data::IdAndLevel>>()
	};
	Client::Pad changeling { takeUserPad(dataPacket.data.id) };
	if (dataPacket.data.level != Client::User::Guest){
		addUserPad(changeling, dataPacket.data.level);
	}
	try {
		emit foreignLevelChange(
			std_ext::tryFindReference(m_usersInPad, dataPacket.id),
			changeling,
			dataPacket.data.level
		);
	} catch (const MessageException &){
		QLOG_ERROR("undefined user changed client user's level in pad " << changeling.name());
		emit foreignLevelChange(
			Client::User("<undefined_user>"),
			changeling,
			dataPacket.data.level
		);
	}
}


void MainClient::processAnswer_RevisionsRange(const Netpacket & netpacket){
	Data::RevisionsRange dataPacket{
		netpacket.dataPacket<Data::RevisionsRange>()
	};
	emit receivedRevisionsRange(
		dataPacket.firstTimeId,
		dataPacket.lastTimeId,
		dataPacket.commands
	);
}

void MainClient::processAnswer_RestoredPad(const Netpacket & netpacket){
	emit restoredPad(netpacket.dataPacket<Data::String>().string);
}

void MainClient::processUpdate_RestoredPad(const Netpacket & netpacket){
	Data::IdAndString dataPacket{
		netpacket.dataPacket<Data::IdAndString>()
	};
	try {
		emit updateSomeUserRestoredPad(
			std_ext::tryFindReference(m_usersInPad, dataPacket.id),
			dataPacket.string
		);
	} catch (const MessageException &){
		emit updateSomeUserRestoredPad(Client::User("<unknown_user>"), dataPacket.string);
	}

}


void MainClient::sendChatMessage(const QString & message){
	m_socket->sendNetpacket<Data::String>(
		Netpacket::Request_SendChatMessage,
		message
	);
}

void MainClient::sendAuthorizationRequest(const QString & login,
										  const QString & password){
	QSUPPOSE(!isAuthorized());
	m_socket->sendNetpacket<Data::Authorization>(
		Netpacket::Request_Authorization,
		login,
		password
	);
//	m_socket->expectFastRespose(RESPONSE_TIMEOUT);
}

void MainClient::sendRegistrationRequest(const QString & name,
										 const QString & login,
										 const QString & password){
	m_socket->sendNetpacket<Data::Registration>(
		Netpacket::Request_Registration,
		login,
		password,
		name
	);
	//	m_socket->expectFastRespose(RESPONSE_TIMEOUT);
}

void MainClient::sendColorChangeRequest(const QColor & color){
	m_socket->sendNetpacket<Data::Color>(
		Netpacket::Request_ChangeColor,
		color
	);
}

void MainClient::sendCharactersRemoved(const int index, const int & amount){
	m_socket->sendNetpacket<Data::IndexAndAmount>(
		Netpacket::Request_RemoveCharacters,
		index,
		amount
	);
}

void MainClient::sendCharactersAdded(const int & index,
									 const QTextDocumentFragment & fragment){
	m_socket->sendNetpacket<Data::IndexAndTextFragment>(
		Netpacket::Request_AddCharacters,
		index,
		fragment
	);
}

void MainClient::createNewPad(const QString & name,
							  const bool & isPublic,
							  const QString & documentHtml){
	m_socket->sendNetpacket<Data::NewPadData>(
		Netpacket::Request_CreateNewPad,
		name,
		isPublic,
		documentHtml
	);
}

void MainClient::connectToPad(const id_t & id){
	m_socket->sendNetpacket<Data::Id>(
		Netpacket::Request_ConnectToPad,
		id
	);
}

void MainClient::disconnectFromPad(){
	m_socket->sendNetpacketSignal(Netpacket::Request_DisconnectFromPad);
}

void MainClient::requireRestorePad(const timeid_t & restorationPoint){
	m_socket->sendNetpacket<Data::Integer<timeid_t>>(
		Netpacket::Request_RestorePad,
		restorationPoint
	);
}

const Client::User & MainClient::clientUser() const { return m_user; }
id_t MainClient::currentPadId() const			    { return m_currentPad.id(); }
const Client::Pad &MainClient::currentPad() const   { return m_currentPad; }

const QHash<id_t, Client::Pad> &MainClient::padsOnline() const{
	return m_padsOnline;
}

bool MainClient::isAuthorized() const{
	return m_user.hasValidId();
}

bool MainClient::waitForAnswer(const unsigned int & delayMs){
	return m_socket->waitForReadyRead(delayMs);
}

bool MainClient::isConnectedToPad(){
	return m_currentPad.isValid();
}

void MainClient::sendFirstDocVersionRevisionsRequest(const timeid_t & firstRevision,
													 const timeid_t & lastRevision){
	m_socket->sendNetpacket<Data::TimeIdRange>(
		Netpacket::Request_FirstDocRevision,
		firstRevision,
		lastRevision
	);
}

void MainClient::sendRevisionsRangeRequest(const timeid_t & firstRevision,
										   const timeid_t & lastRevision){
	m_socket->sendNetpacket<Data::TimeIdRange>(
		Netpacket::Request_RevisionsRange,
		firstRevision,
		lastRevision
	);
}

void MainClient::sendAvailableRevisionsRequest(){
	m_socket->sendNetpacketSignal(Netpacket::Request_AvailableRevisions);
}


const QVector<Client::Pad> & MainClient::ownerPads() const{
	return m_ownerPads;
}

const QVector<Client::Pad> &MainClient::adminPads() const{
	return m_adminPads;
}

const QVector<Client::Pad> &MainClient::authorPads() const{
	return m_authorPads;
}

void MainClient::sendChangeUserLevelRequest(const id_t & userId,
											const Client::User::Level & newLevel){
	m_socket->sendNetpacket<Data::IdAndLevel>(
		Netpacket::Request_ChangeUserLevel,
		userId,
		newLevel
	);
}


Client::Pad MainClient::takeUserPad(const id_t & padId){
	auto predicate {
		[padId](const Client::Pad & pad){ return padId == pad.id(); }
	};
	auto adminPadsIterator{
		std::find_if(m_adminPads.begin(), m_adminPads.end(), predicate)
	};
	if (adminPadsIterator != m_adminPads.end()){
		return m_adminPads.takeAt(adminPadsIterator - m_adminPads.begin());
	}

	auto authorPadsIterator{
		std::find_if(m_authorPads.begin(), m_authorPads.end(), predicate)
	};
	if (authorPadsIterator != m_authorPads.end()){
		return m_authorPads.takeAt(authorPadsIterator - m_authorPads.begin());
	}
	auto ownerPadsIterator{
		std::find_if(m_ownerPads.begin(), m_ownerPads.end(), predicate)
	};
	if (ownerPadsIterator != m_ownerPads.end()){
		return m_ownerPads.takeAt(ownerPadsIterator - m_ownerPads.begin());
	}
	QLOG_ERROR("undefined behaviour");
	return Client::Pad("<undefined_behaviour>");
}

void MainClient::addUserPad(const Client::Pad & pad, const Client::User::Level & level){
	switch (level){
		case Client::User::Owner:{
			m_ownerPads.prepend(pad);
			return;
		}
		case Client::User::Admin:{
			m_adminPads.prepend(pad);
			return;
		}
		case Client::User::Author:{
			m_authorPads.prepend(pad);
			return;
		}
		default:{
			QLOG_ERROR("undefined behaviour");
		}
	}
}












