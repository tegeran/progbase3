﻿#ifndef CLIENT_H
#define CLIENT_H

#include <QString>
#include <QHostAddress>
#include <QTextDocumentFragment>

#include <chrono>


#include "client_user.h"
#include "client_pad.h"
#include "std_ext.h"
#include "tcpserver.h"
#include "netpacket.h"

class MainClient : public QObject {
	Q_OBJECT
	DECL_UNCOPIABLE(MainClient)

public:
	static constexpr const quint16 SERVER_PORT  { 2000 };
	static constexpr const std::chrono::milliseconds RESPONSE_TIMEOUT
						 = std::chrono::milliseconds(2000);

	explicit MainClient(QObject * const & parent = nullptr);
	~MainClient();

	void openConnection(const QHostAddress & serverAddress,
						const quint16 & port = SERVER_PORT);
	void closeConnection();

	const Client::User & clientUser() const;

	void setSelf(Client::User && userOnlineFromId);

	/* throws MessageException */
	const Client::User & userOnlineFromId(const id_t & id) const;

	id_t currentPadId() const;
	const Client::Pad & currentPad() const;

	const QHash<id_t, Client::Pad> & padsOnline() const;

	bool isAuthorized() const;

	bool waitForAnswer(const unsigned int & delayMs = 1000);
	bool isConnectedToPad();
	const QVector<Client::Pad> & ownerPads() const;
	const QVector<Client::Pad> & adminPads() const;
	const QVector<Client::Pad> & authorPads() const;

signals:
	void connectedToServer();
	void socketError(const QString & reason);
	void receivedErrorReport(const QString & report);
	void disconnectedFromServer();
	void registrationError(const QString & reason);
	void authorizationError(const QString & reason);

	void authorized(const Client::User & clientUser, const QHash<id_t, Client::Pad> & padsOnline);
	void registered(const Client::User & newUser, const QHash<id_t, Client::Pad> & padsOnline);

	void userConnected(Client::User);
	void userDisconnected(const Client::User & user);

	void receivedChatMessage(const Client::User & userId, const QString & message);
	void deliveredChatMessage(const Client::User & selfUser, const QString & message);

	void changedColor(const QColor & color);

	void updateCharactersRemoved(const int index, const int & amount);
	void updateCharactersAdded(const int & index, const QTextDocumentFragment & fragment);

	void updateAddedPadOnline(const Client::Pad & pad);
	void updateRemovedPadOnline(const Client::Pad & pad);

	void updateUserLeftPad(const Client::User & user);

	void createdNewPad(const Client::Pad & pad);
	void connectedToPad(const Client::Pad & pad,
						const QHash<id_t, Client::User> & connectedUsers,
						const QString newCurrentDocumentHtml);
	void disconnectedFromPad();
	void updateUserChangedColor(const Client::User & user);

	void receivedAvailableRevisions(const timeid_t & amount);
	void receivedFirstDocVersionRevisions(const QString & docHtml,
										  const timeid_t & firstTimeId,
										  const timeid_t & lastTimeId,
										  const QVector<Netpacket> & revisions);

	void receivedRevisionsRange(const timeid_t & firstTimeId,
								const timeid_t & lastTimeId,
								const QVector<Netpacket> & revisions);

	void updateSomeUserRestoredPad(const Client::User & guilty,
								   const QString & newDocumentHtml);
	void restoredPad(const QString & newDocumentHtml);

	void currentLevelChanged(const Client::User & initiator,
							 const Client::User::Level & newLevel);

	void someUserChangedLevel(const Client::User & initiator,
							  const Client::User & targetUser,
							  const Client::User::Level & newLevel);

	void foreignLevelChange(const Client::User & initiator,
							const Client::Pad & pad,
							const Client::User::Level & newLevel);

public slots:
	void sendChangeUserLevelRequest(const id_t & userId, const Client::User::Level & newLevel);

	void sendFirstDocVersionRevisionsRequest(const timeid_t & firstRevision,
											 const timeid_t & lastRevision);

	void sendRevisionsRangeRequest(const timeid_t & firstRevision,
								   const timeid_t & lastRevision);

	void sendAvailableRevisionsRequest();
	void sendChatMessage(const QString & message);
	void sendAuthorizationRequest(const QString & login,
								  const QString & password);

	void sendRegistrationRequest(const QString & name,
								 const QString & login,
								 const QString & password);
	void sendColorChangeRequest(const QColor & color);

	void sendCharactersRemoved(const int index, const int & amount);
	void sendCharactersAdded(const int & index, const QTextDocumentFragment & fragment);

	void createNewPad(const QString & name,
					  const bool & isPublic,
					  const QString & documentHtml);
	void connectToPad(const id_t & id);
	void disconnectFromPad();
	void requireRestorePad(const timeid_t & restorationPoint);

private slots:
	void handle_socket_connected();
	void handle_socket_receivedNetpacket(const Netpacket & netpacket);
	void handle_socket_error(const QAbstractSocket::SocketError & errorCode);
	void handle_socket_disconnected();
private:
	Client::User m_user;
	TcpSocket * m_socket;
	Client::Pad m_currentPad;
	QHash<id_t, Client::User> m_usersInPad; /* excluding client user */
	QHash<id_t, Client::Pad> m_padsOnline;
	QVector<Client::Pad> m_ownerPads;
	QVector<Client::Pad> m_adminPads;
	QVector<Client::Pad> m_authorPads;

	void processUpdate_NewChatMessage          (const Netpacket & netpacket);
	void processUpdate_UserDisconnected        (const Netpacket & netpacket);
	void processUpdate_UserConnectedToPad      (const Netpacket & netpacket);
	void processUpdate_AddedCharacters         (const Netpacket & netpacket);
	void processUpdate_RemovedCharacters       (const Netpacket & netpacket);
	void processAnswer_AddedCharacters         (const Netpacket & netpacket);
	void processAnswer_RemovedCharacters       (const Netpacket & netpacket);
	void processUpdate_AddedPadOnline          (const Netpacket & netpacket);
	void processUpdate_RemovedPadOnline        (const Netpacket & netpacket);
	void processUpdate_UserLeftPad		       (const Netpacket & netpacket);
	void processUpdate_UserChangedColor        (const Netpacket & netpacket);
	void processAnswer_CreatedNewPad	       (const Netpacket & netpacket);
	void processAnswer_ConnectedToPad          (const Netpacket & netpacket);
	void processAnswer_DisconnectedFromPad     (const Netpacket & netpacket);
	void processAnswer_PadsOnline		       (const Netpacket & netpacket);
	void processAnswer_DeliveredChatMessage    (const Netpacket & netpacket);
	void processAnswer_Authorization           (const Netpacket & netpacket);
	void processAnswer_Registration            (const Netpacket & netpacket);
	void processAnswer_Error                   (const Netpacket & netpacket);
	void processAnswer_RegistrationError       (const Netpacket & netpacket);
	void processAnswer_AuthorizationError      (const Netpacket & netpacket);
	void processAnswer_ChangedColor            (const Netpacket & netpacket);
	void processAnswer_AvailableRevisions      (const Netpacket & netpacket);
	void processAnswer_RevisionsRange	       (const Netpacket & netpacket);
	void processAnswer_RestoredPad		       (const Netpacket & netpacket);
	void processUpdate_RestoredPad		       (const Netpacket & netpacket);
	void processAnswer_FirstDocVersionRevisions(const Netpacket & netpacket);
	void processAnswer_AddingCharsError		   (const Netpacket & netpacket);
	void processAnswer_RemovingCharsError	   (const Netpacket & netpacket);
	void processUpdate_ChangedUserLevel		   (const Netpacket & netpacket);
	void processUpdate_ForeignChangeUserLevel  (const Netpacket & netpacket);

	Client::Pad takeUserPad(const id_t & padId);
	void addUserPad(const Client::Pad & pad, const Client::User::Level & level);

};

#endif // CLIENT_H
