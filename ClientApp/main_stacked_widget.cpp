#include <QMessageBox>

#include "connecting_dialog.h"
#include "main_stacked_widget.h"
#include "ui_main_stacked_widget.h"
#include "authorization_dialog.h"
#include "registration_dialog.h"
#include "user_color_setup_dialog.h"
#include "pad_main_window.h"

#include "qext.h"
#include "std_ext.h"
#include "error.h"
#include "client_user.h"
#include "client_pad.h"


MainStackedWidget::MainStackedWidget(QWidget *parent) :
	QStackedWidget(parent),
	ui(new Ui::MainStackedWidget),
	m_client(new MainClient(this))
{
	ui->setupUi(this);
	QWidget::setWindowTitle("CommonPad");
	ConnectingDialog * connectingDialog {
		new ConnectingDialog
	};
	connect(
		connectingDialog,
		SIGNAL(abortRequest()),
		this,
		SLOT(handle_connectingDialog_abortRequest())
	);
	setupClientSignals();
	m_client->openConnection(QHostAddress("10.14.8.29"));
	QStackedWidget::addWidget(connectingDialog);
}

MainStackedWidget::~MainStackedWidget(){
	delete ui;
}

void MainStackedWidget::handle_client_connectedToServer(){
	QLOG("successful connection");
	gotoAuthorization();
}

void MainStackedWidget::gotoAuthorization(){
	deleteAllStackedWidgetsLater();
	AuthorizationDialog * authDialog {
		new AuthorizationDialog
	};
	Qext::Object::connectMultipleSignalsToSlots(
		authDialog,
		this,
		{
			SIGNAL(signInRequest(QString,QString)),
			SLOT(handle_authDialog_signInRequest(QString,QString)),

			SIGNAL(newAccountRequest()),
			SLOT(handle_authDialog_newAccountRequest()),

			SIGNAL(rejected()),
			SLOT(handle_authDialog_rejected())
		}
	);
	QStackedWidget::addWidget(authDialog);
}


void MainStackedWidget::handle_authDialog_rejected(){
	Qext::Meta::scheduleRoutine(this, &QWidget::close);
}

void MainStackedWidget::handle_authDialog_signInRequest(const QString & login,
														const QString & password){
	AuthorizationDialog * authDialog { QSENDER(AuthorizationDialog *) };
	authDialog->await();
	m_client->sendAuthorizationRequest(login, password);
}

void MainStackedWidget::handle_authDialog_newAccountRequest(){
	deleteAllStackedWidgetsLater();
	RegistrationDialog * registrDialog {
		new RegistrationDialog
	};
	Qext::Object::connectMultipleSignalsToSlots(
		registrDialog,
		this,
		{
			SIGNAL(registrationRequest(QString,QString,QString)),
			SLOT(handle_registrDialog_registrationRequest(QString,QString,QString)),

			SIGNAL(rejected()),
			SLOT(handle_registrDialog_rejected())
		}
	);
	QStackedWidget::addWidget(registrDialog);
}

void MainStackedWidget::handle_registrDialog_registrationRequest(const QString & name,
																 const QString & login,
																 const QString & password){
	QSENDER(RegistrationDialog *)->await();
	m_client->sendRegistrationRequest(name, login, password);
}

void MainStackedWidget::handle_registrDialog_rejected(){
	gotoAuthorization();
}

void MainStackedWidget::handle_client_registered(const Client::User & newUser){
	deleteAllStackedWidgetsLater();
	QLOG("registered new user " << newUser);
	UserColorSetupDialog * colorSetupDialog {
		new UserColorSetupDialog
	};
	QStackedWidget::addWidget(colorSetupDialog);
	connect(
		colorSetupDialog,
		SIGNAL(accepted()),
		this,
		SLOT(handle_colorSetupDialog_accepted())
	);
}


void MainStackedWidget::handle_colorSetupDialog_accepted(){
	m_client->sendColorChangeRequest(QSENDER(UserColorSetupDialog *)->chosenColor());
	gotoPadMainWindow();
}




void MainStackedWidget::handle_connectingDialog_abortRequest(){
	m_client->disconnectedFromServer();
	QWidget::close();
}

void MainStackedWidget::handle_client_registrationError(const QString & reason){
	produceCriticalMessageBox(
		"Registration failed",
		tr("Registration failed: \"%1\"").arg(reason)
	);
	dynamic_cast<Awaitable *>(QStackedWidget::widget(0))->enableInteraction();
}

void MainStackedWidget::handle_client_authorizationError(const QString & reason){
	static_cast<AuthorizationDialog *>(QStackedWidget::widget(0))->enableInteraction();
	produceCriticalMessageBox(
		"Authorization error",
		tr("Authorization error: \"%1\"").arg(reason)
	);
}

void MainStackedWidget::handle_client_socketError(const QString & reason){
	deleteAllStackedWidgetsLater();
	QStackedWidget::hide();
	Qext::Meta::scheduleRoutine(
	[this, reason](){
		connect(
			produceCriticalMessageBox(
				tr("Connection error"),
				tr("Connection error: \"%1\"").arg(reason)
			),
			SIGNAL(destroyed(QObject*)),
			this,
			SLOT(close())
		);
	});
}

void MainStackedWidget::handle_client_receivedErrorReport(const QString & report){
	QLOG_ERROR("ERROR REPORT FROM SERVER: " << report);
}

void MainStackedWidget::handle_client_authorized(const Client::User &, const QHash<id_t, Client::Pad> &){
	gotoPadMainWindow();
}

void MainStackedWidget::gotoPadMainWindow(){
	deleteAllStackedWidgetsLater();
	PadMainWindow * padMainwindow { new PadMainWindow(m_client, this) };
	QStackedWidget::addWidget(padMainwindow);
}


QMessageBox * MainStackedWidget::produceCriticalMessageBox(const QString & title, const QString & reason){
	QMessageBox * criticalMessageBox {
		new QMessageBox(
			QMessageBox::Critical,
			title,
			reason,
			QMessageBox::Close,
			this
		)
	};
	criticalMessageBox->show();
	Qext::Widget::deleteOnClosed(criticalMessageBox);
	return criticalMessageBox;
}





void MainStackedWidget::setupClientSignals(){
	Qext::Object::connectMultipleSignalsToSlots(
		m_client,
		this,
		{
			SIGNAL(authorized(Client::User,QHash<id_t,Client::Pad>)),
			SLOT(handle_client_authorized(Client::User,QHash<id_t,Client::Pad>)),

			SIGNAL(authorizationError(QString)),
			SLOT(handle_client_authorizationError(QString)),

			SIGNAL(receivedErrorReport(QString)),
			SLOT(handle_client_receivedErrorReport(QString)),

			SIGNAL(socketError(QString)),
			SLOT(handle_client_socketError(QString)),

			SIGNAL(connectedToServer()),
			SLOT(handle_client_connectedToServer()),

			SIGNAL(disconnectedFromServer()),
			SLOT(handle_client_disconnectedFromServer()),

			SIGNAL(registered(Client::User,QHash<id_t,Client::Pad>)),
			SLOT(handle_client_registered(Client::User)),

			SIGNAL(registrationError(QString)),
			SLOT(handle_client_registrationError(QString))

		}
	);

}








void MainStackedWidget::handle_client_disconnectedFromServer(){
	QLOG("disconnected from server");
}

void MainStackedWidget::deleteAllStackedWidgetsLater(){
	while (QStackedWidget::count()){
		QWidget * widget { QStackedWidget::widget(0) };
		widget->deleteLater();
		QStackedWidget::removeWidget(widget);
	}
}


