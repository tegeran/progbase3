#ifndef MAIN_STACKED_WIDGET_H
#define MAIN_STACKED_WIDGET_H

#include <QWidget>
#include <QStackedWidget>
#include <QMessageBox>

#include "main_client.h"

namespace Ui {
	class MainStackedWidget;
}

class MainStackedWidget : public QStackedWidget{
	Q_OBJECT

public:
	explicit MainStackedWidget(QWidget *parent = 0);
	~MainStackedWidget();

private slots:
	void handle_connectingDialog_abortRequest();

	void handle_authDialog_signInRequest(const QString & login, const QString & password);
	void handle_authDialog_newAccountRequest();
	void handle_authDialog_rejected();

	void handle_client_registrationError(const QString & reason);
	void handle_client_registered(const Client::User & newUser);

	void handle_registrDialog_registrationRequest(const QString & name,
												  const QString & login,
												  const QString & password);
	void handle_registrDialog_rejected();

	void handle_client_authorizationError(const QString & reason);

	void handle_colorSetupDialog_accepted();


	void handle_client_connectedToServer();
	void handle_client_disconnectedFromServer();
	void handle_client_socketError(const QString & reason);
	void handle_client_receivedErrorReport(const QString & report);
	void handle_client_authorized(const Client::User & selfUser,
								  const QHash<id_t, Client::Pad> & padsOnline);


//	void handle_client_authorized();

	QMessageBox * produceCriticalMessageBox(const QString & title, const QString & reason);
private:
	Ui::MainStackedWidget *ui;
	MainClient * m_client;

	void setupClientSignals();
	void deleteAllStackedWidgetsLater();
	void gotoAuthorization();
	void gotoPadMainWindow();
};

#endif // MAIN_STACKED_WIDGET_H
