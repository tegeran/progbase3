#include <QGridLayout>
#include <QPlainTextEdit>
#include <QTextCursor>
#include <QTextBlock>
#include <QTextFragment>
#include <QTextDocumentFragment>
#include <QAbstractTextDocumentLayout>
#include <QFileDialog>

#include "pad_main_window.h"
#include "ui_pad_main_window.h"
#include "pads_dialog.h"
#include "creating_new_pad_dialog.h"
#include "revisions_viewer.h"
#include "user_pads_dialog.h"
#include "admin_dialog.h"

#include "qext.h"

PadMainWindow::PadMainWindow(MainClient * const & client,
							 MainStackedWidget * const & stackedWidget,
							 QWidget * const & parent)
	: QMainWindow(parent),
	  ui(new Ui::PadMainWindow),
	  m_client(client),
	  m_userlistModel(new UserlistModel(client, this)),
	  m_stackedWidget(stackedWidget),
	  m_userPadsModel(new UserPadsModel(client, this))
{
	ui->setupUi(this);
	QMainWindow::setWindowTitle("CommonPad");
	ui->chat->		   setMainClient(m_client);
	ui->mainEditor->   setMainClient(m_client);
	ui->userColorRect->setMainClient(m_client);
	ui->mainEditor->setControlls(
		ui->pb_bold,
		ui->pb_italic,
		ui->pb_underline,
		ui->pb_strikeout,
		ui->pb_increaseFont,
		ui->pb_decreaseFont,
		ui->combo_font,
		ui->pb_finalize,
		ui->lcd_fontSize
	);
	ui->lbl_username->setText(m_client->clientUser().name());
	ui->lv_usersOnline->setModel(m_userlistModel);
	ui->lv_usersOnline->setEditTriggers(QListView::NoEditTriggers);
	ui->lv_usersOnline->setSelectionMode(QAbstractItemView::NoSelection);
	setupClientSignals();
	ui->pb_timeSlider->setEnabled(m_client->isConnectedToPad());
	ui->pb_leavePad->setEnabled(m_client->isConnectedToPad());
}


PadMainWindow::~PadMainWindow(){
	delete ui;
}


void PadMainWindow::handle_client_createdNewPad(const Client::Pad & pad){
	ui->lbl_padname->setText(pad.name());
	ui->pb_timeSlider->setEnabled(true);
	ui->pb_leavePad->setEnabled(true);
	validateAdminMode();
}



void PadMainWindow::handle_client_connectedToPad(const Client::Pad & pad,
												 const QHash<id_t, Client::User> &,
												 const QString &){
	ui->lbl_padname->setText(pad.name());
	ui->pb_timeSlider->setEnabled(true);
	ui->pb_leavePad->setEnabled(true);
	validateAdminMode();
}

void PadMainWindow::handle_client_disconnectedFromPad(){
	ui->pb_timeSlider->setEnabled(false);
	ui->pb_leavePad->setEnabled(false);
	ui->lbl_padname->setText("Local pad");
	QLOG("client disconnnected from pad");
}



void PadMainWindow::setupClientSignals(){

	Qext::Object::connectMultipleSignalsToSlots(
		m_client,
		this,
		{
			SIGNAL            (currentLevelChanged(Client::User,Client::User::Level)),
			SLOT(handle_client_currentLevelChanged(Client::User,Client::User::Level)),

			SIGNAL            (connectedToPad(Client::Pad,QHash<id_t,Client::User>,QString)),
			SLOT(handle_client_connectedToPad(Client::Pad,QHash<id_t,Client::User>,QString)),

			SIGNAL            (createdNewPad(Client::Pad)),
			SLOT(handle_client_createdNewPad(Client::Pad)),

			SIGNAL            (disconnectedFromPad()),
			SLOT(handle_client_disconnectedFromPad())
		}
	);

}

void PadMainWindow::startAwaiting(){
	QMainWindow::setEnabled(false);
}

void PadMainWindow::stopAwaiting(){
	QMainWindow::setEnabled(true);
}


void PadMainWindow::on_pb_padsOnline_clicked(){
	PadsDialog * padsDialog = new PadsDialog(m_client);
	Qext::Widget::deleteOnClosed(padsDialog);
	m_stackedWidget->addWidget(padsDialog);
	m_stackedWidget->setCurrentWidget(padsDialog);
}



void PadMainWindow::on_pb_createNewPad_clicked(){
	CreatingNewPadDialog * newPadDialog = {
		new CreatingNewPadDialog(m_client, ui->mainEditor)
	};
	Qext::Widget::deleteOnClosed(newPadDialog);
	m_stackedWidget->addWidget(newPadDialog);
	m_stackedWidget->setCurrentWidget(newPadDialog);

}

void PadMainWindow::handle_saveHtmlDialog_selectedFile(const QString & fileName){
	saveDocument(fileName, ui->mainEditor->document()->toHtml());
}

void PadMainWindow::handle_savePlainTextDialog_selectedFile(const QString & fileName){
	saveDocument(fileName, ui->mainEditor->document()->toPlainText());
}

void PadMainWindow::saveDocument(const QString & filePath, const QString & document){
	if (filePath.isEmpty()){
		return;
	}
	try {
		Qext::File::write(document, filePath);
	} catch (const MessageException & exception){
		Qext::throwNewMessageBox(
			QMessageBox::Critical,
			"SIGSEGV",
			QString("SIGSEGV: failed to save document to \"%1\" (%2)")
				.arg(filePath).arg(exception.what()),
			QMessageBox::Close,
			this
		);
		return;
	}
	Qext::throwNewMessageBox(
		QMessageBox::Information,
		"Document saved",
		QString("Successfully saved document to \"%1\"").arg(filePath),
		QMessageBox::Ok,
		this
				);
}

void PadMainWindow::validateAdminMode(){
	if (m_client->isConnectedToPad()
	 && (m_client->clientUser().isOwner()
	  || m_client->clientUser().hasAdminRights())){
		ui->lv_usersOnline->setSelectionMode(QAbstractItemView::SingleSelection);
		ui->lv_usersOnline->setCursor(QCursor(Qt::PointingHandCursor));
	} else {
		ui->lv_usersOnline->setSelectionMode(QAbstractItemView::NoSelection);
		ui->lv_usersOnline->setCursor(QCursor(Qt::ArrowCursor));
	}
}




void PadMainWindow::removeWidget(QWidget * widget){
	if (widget){
		m_stackedWidget->removeWidget(widget);
	} else {
		QLOG_ERROR("failed to remove widget");
	}
}


void PadMainWindow::on_pb_menuButton_clicked(){
	QGridLayout * gridLayout {
		static_cast<QGridLayout *>(ui->centralwidget->layout())
	};
	if (ui->wget_menu->isHidden()){
		Qext::GridLayout::repositionWidget(
			*gridLayout, ui->wget_edit,
			2, 1, -1, -1
		);
		ui->wget_menu->show();
		gridLayout->addWidget(ui->wget_menu, 2, 0, 1, 1);
	} else {
		gridLayout->removeWidget(ui->wget_menu);
		ui->wget_menu->hide();
		Qext::GridLayout::repositionWidget(
			*gridLayout, ui->wget_edit,
			2, 0, -1, -1
		);
	}
}




void PadMainWindow::on_pb_timeSlider_clicked(){
	RevisionsViewer * revisionsViewer {
		new RevisionsViewer(m_client)
	};
	Qext::Widget::deleteOnClosed(revisionsViewer);
	m_stackedWidget->addWidget(revisionsViewer);
	m_stackedWidget->setCurrentWidget(revisionsViewer);
}


void PadMainWindow::on_act_saveAsHtml_triggered(){
	QFileDialog * saveFileDialog(new QFileDialog(this));
	saveFileDialog->setAcceptMode(QFileDialog::AcceptSave);
	saveFileDialog->setOption(QFileDialog::DontConfirmOverwrite);
	Qext::Widget::deleteOnClosed(saveFileDialog);
	saveFileDialog->show();
	connect(
		saveFileDialog,
		SIGNAL(fileSelected(QString)),
		this,
		SLOT(handle_saveHtmlDialog_selectedFile(QString))
	);
}

void PadMainWindow::on_act_saveAsPlainText_triggered(){
	QFileDialog * saveFileDialog(new QFileDialog(this));
	saveFileDialog->setAcceptMode(QFileDialog::AcceptSave);
	saveFileDialog->setOption(QFileDialog::DontConfirmOverwrite);
	Qext::Widget::deleteOnClosed(saveFileDialog);
	saveFileDialog->show();
	connect(
		saveFileDialog,
		SIGNAL(fileSelected(QString)),
		this,
		SLOT(handle_savePlainTextDialog_selectedFile(QString))
	);
}

void PadMainWindow::on_pb_myPads_clicked(){
	UserPadsDialog * userPadsDialog{ new UserPadsDialog(m_client, m_userPadsModel)};
	Qext::Widget::deleteOnClosed(userPadsDialog);
	m_stackedWidget->addWidget(userPadsDialog);
	m_stackedWidget->setCurrentWidget(userPadsDialog);
}

void PadMainWindow::handle_client_currentLevelChanged(const Client::User &,
													  const Client::User::Level &){
	validateAdminMode();
}



void PadMainWindow::on_lv_usersOnline_doubleClicked(const QModelIndex &index){
	if (index.isValid() && m_client->clientUser().canModifyUserRightsFor(
			m_userlistModel->userAt(index.row())
		)){
		AdminDialog * adminDialog {
			new AdminDialog(m_client, m_userlistModel->userAt(index.row()), this)
		};
		adminDialog->show();
		Qext::Widget::deleteOnClosed(adminDialog);
	}
}


void PadMainWindow::on_pb_leavePad_clicked(){
	if (m_client->isConnectedToPad()){
		m_client->disconnectFromPad();
		ui->pb_leavePad->setEnabled(false);
		ui->pb_timeSlider->setEnabled(false);
	}
}
