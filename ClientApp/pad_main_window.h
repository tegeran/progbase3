#ifndef PAD_MAIN_WINDOW_H
#define PAD_MAIN_WINDOW_H

#include <QMainWindow>
#include <QTextDocument>
#include <QTextCharFormat>

#include "main_client.h"
#include "userlist_model.h"
#include "awaitable.h"
#include "pads_dialog.h"
#include "main_stacked_widget.h"
#include "creating_new_pad_dialog.h"
#include "user_pads_model.h"

namespace Ui {
	class PadMainWindow;
}

class PadMainWindow : public QMainWindow, public Awaitable{
	Q_OBJECT

public:
	explicit PadMainWindow(MainClient * const &client,
						   MainStackedWidget * const & stackedWidget,
						   QWidget * const &parent = nullptr);
	~PadMainWindow();

private slots:

	void handle_client_connectedToPad(const Client::Pad & pad,
									  const QHash<id_t, Client::User> & connectedUsers,
									  const QString & newCurrentDocumentHtml);

	void handle_client_disconnectedFromPad();

	void handle_client_createdNewPad(const Client::Pad & pad);

	void handle_saveHtmlDialog_selectedFile(const QString & fileName);
	void handle_savePlainTextDialog_selectedFile(const QString & fileName);

	void on_pb_padsOnline_clicked();
	void on_pb_createNewPad_clicked();
	void on_pb_menuButton_clicked();

	void on_pb_timeSlider_clicked();

	void on_act_saveAsHtml_triggered();

	void on_act_saveAsPlainText_triggered();

	void on_pb_myPads_clicked();

	void handle_client_currentLevelChanged(const Client::User & initiator,
										   const Client::User::Level & newLevel);

	void on_lv_usersOnline_doubleClicked(const QModelIndex &index);

	void on_pb_leavePad_clicked();

private:
	Ui::PadMainWindow * ui;
	MainClient * m_client;
	UserlistModel * m_userlistModel;
	MainStackedWidget * m_stackedWidget;
	UserPadsModel * m_userPadsModel;

	void correctBugs(const int & index, int & charsRemoved, int & charsAdded);
	void setupClientSignals();
	void removeWidget(QWidget * widget);
	void saveDocument(const QString & filePath, const QString & document);

	void validateAdminMode();

	// Awaitable interface
protected:
	void startAwaiting() override;
	void stopAwaiting() override;
};

#endif // PAD_MAIN_WINDOW_H
