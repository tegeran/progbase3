#include "pads_dialog.h"
#include "ui_pads_dialog.h"
#include "qext.h"

PadsDialog::PadsDialog(MainClient * const & client, QWidget * const & parent) :
	QDialog(parent),
	ui(new Ui::PadsDialog),
	m_padsModel(client),
	m_client(client)
{
	QTRACE_CALL();
	ui->setupUi(this);
	ui->lv_padsOnline->setModel(&m_padsModel);
	ui->lv_padsOnline->setEditTriggers(QListView::NoEditTriggers);
	ui->lv_padsOnline->setSelectionMode(QAbstractItemView::SingleSelection);
	Qext::Object::connectMultipleSignals(
		ui->lv_padsOnline->selectionModel(),
		{
			SIGNAL(currentChanged(QModelIndex,QModelIndex)),
			SIGNAL(currentRowChanged(QModelIndex,QModelIndex)),
			SIGNAL(selectionChanged(QItemSelection,QItemSelection))
		},
		this,
		SLOT(validateConnectButton())
	);
	Qext::Dialog::closeOnFinished(this);
	Qext::Object::connectMultipleSignals(
		m_client,
		{
			SIGNAL(connectedToPad(Client::Pad,QHash<id_t,Client::User>,QString)),
			SIGNAL(createdNewPad(Client::Pad)),
			SIGNAL(disconnectedFromServer())
		},
		this,
		SLOT(reject())
	);
	validateConnectButton();
}

PadsDialog::~PadsDialog(){
	QTRACE_CALL();
	delete ui;
}

void PadsDialog::on_pb_cancel_clicked(){
	QDialog::reject();
}

void PadsDialog::validateConnectButton(){
	QModelIndex currentIndex { ui->lv_padsOnline->currentIndex() };
	ui->pb_connectToPad->setEnabled(
		!Awaitable::isAwaiting()
		&& currentIndex.isValid()
		&& m_padsModel.padsVector().at(currentIndex.row()).id()
			!= m_client->currentPadId()
	);
}

void PadsDialog::on_pb_connectToPad_clicked(){
	m_client->connectToPad(
		m_padsModel.padsVector().at(ui->lv_padsOnline->currentIndex().row()).id()
	);
	Awaitable::await();
}

void PadsDialog::startAwaiting(){
	Qext::Widget::disableWidgets(ui->pb_connectToPad, ui->pb_cancel);
}

void PadsDialog::stopAwaiting(){
	validateConnectButton();
	ui->pb_cancel->setEnabled(true);
}

void PadsDialog::on_lv_padsOnline_doubleClicked(const QModelIndex &index){
	if (index.isValid() && !Awaitable::isAwaiting()
			&& m_padsModel.padsVector().at(index.row()).id()
				!= m_client->currentPadId()){
		m_client->connectToPad(m_padsModel.padsVector().at(index.row()).id());
		Awaitable::await();
	}
}
