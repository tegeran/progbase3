#ifndef PADS_DIALOG_H
#define PADS_DIALOG_H

#include <QDialog>
#include "pads_model.h"
#include "awaitable.h"
#include "main_client.h"

namespace Ui {
	class PadsDialog;
}

class PadsDialog : public QDialog, public Awaitable{
	Q_OBJECT

public:
	explicit PadsDialog(MainClient * const & client,
						QWidget * const & parent = nullptr);
	~PadsDialog();

private slots:
	void on_pb_cancel_clicked();

	void validateConnectButton();

	void on_pb_connectToPad_clicked();

	void on_lv_padsOnline_doubleClicked(const QModelIndex &index);

private:
	Ui::PadsDialog *ui;
	PadsModel m_padsModel;
	MainClient * m_client;


	// Awaitable interface
protected:
	void startAwaiting() override;
	void stopAwaiting() override;
};

#endif // PADS_DIALOG_H
