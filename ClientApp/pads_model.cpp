#include "pads_model.h"
#include "qext.h"

PadsModel::PadsModel(MainClient * const & client, QObject *parent)
	: QAbstractListModel(parent), m_client(client) {
	Qext::Object::connectMultipleSignalsToSlots(
		client,
		this,
		{
			SIGNAL(updateAddedPadOnline(Client::Pad)),
			SLOT(handle_client_updateAddedPadOnline(Client::Pad)),

			SIGNAL(updateRemovedPadOnline(Client::Pad)),
			SLOT(handle_client_updateRemovedPadOnline(Client::Pad)),

			SIGNAL(createdNewPad(Client::Pad)),
			SLOT(handle_client_createdNewPad(Client::Pad)),

			SIGNAL(connectedToPad(Client::Pad,QHash<id_t,Client::User>,QString)),
			SLOT(handle_client_connectedToPad(Client::Pad,QHash<id_t,Client::User>,QString))
		}
	);
	for (const Client::Pad & pad : client->padsOnline()){
		m_padsVector << pad;
	}
}

int PadsModel::rowCount(const QModelIndex &parent) const{
	return parent.isValid()
			? 0
			: m_padsVector.size();
}

QVariant PadsModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();


	switch (role){
		case Qt::DisplayRole:{
			return m_padsVector.at(index.row()).name();
		}
		case Qt::BackgroundRole:{
			return m_padsVector.at(index.row()).id() == m_client->currentPadId()
				? QVariant(QBrush(QColor::fromRgb(153, 248, 218)))
				: QVariant();
		}
		case Qt::ForegroundRole:{
			return m_padsVector.at(index.row()).id() == m_client->currentPadId()
				? QVariant(QBrush(QColor(Qt::black)))
				: QVariant();
		}
		default: return QVariant();
	}
}

const QVector<Client::Pad> & PadsModel::padsVector() const{
	return m_padsVector;
}

void PadsModel::handle_client_updateAddedPadOnline(const Client::Pad & pad){
	beginInsertRows(QModelIndex(), m_padsVector.size(), m_padsVector.size());
	m_padsVector << pad;
	endInsertRows();
}

void PadsModel::handle_client_updateRemovedPadOnline(const Client::Pad & pad){
	auto resultIterator {
		std::find_if(
			m_padsVector.begin(),
			m_padsVector.end(),
			[&pad](const Client::Pad & suspect){
				return suspect.id() == pad.id();
			}
		)
	};
	if (resultIterator == m_padsVector.end()){
		QLOG_ERROR("an attempt to remove online pad with unknown id was made");
		return;
	}
	long removingIndex { resultIterator - m_padsVector.begin() };

	beginRemoveRows(QModelIndex(), removingIndex, removingIndex	);
	m_padsVector.remove(removingIndex);
	endRemoveRows();
}

void PadsModel::handle_client_createdNewPad(const Client::Pad & pad){
	beginResetModel();
	m_padsVector << pad;
	endResetModel();

}

void PadsModel::handle_client_connectedToPad(const Client::Pad &,
											 const QHash<id_t, Client::User>,
											 const QString &){
	beginResetModel();
	endResetModel();
}

Qt::ItemFlags PadsModel::flags(const QModelIndex & index) const{
	if (index.isValid()){
		return m_padsVector.at(index.row()).id() == m_client->currentPadId()
					? Qt::ItemIsEnabled
					: (Qt::ItemIsEnabled | Qt::ItemIsSelectable);
	} else {
		return Qt::NoItemFlags;
	}
}

