#ifndef PADSMODEL_H
#define PADSMODEL_H

#include <QAbstractListModel>
#include <QHash>

#include "client_pad.h"
#include "main_client.h"

class PadsModel : public QAbstractListModel
{
	Q_OBJECT

public:
	explicit PadsModel(MainClient * const & client,
					   QObject *parent = nullptr);

	// Basic functionality:
	int rowCount(const QModelIndex &parent = QModelIndex()) const override;

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

	const QVector<Client::Pad> &padsVector() const;

private slots:
	void handle_client_updateAddedPadOnline(const Client::Pad & pad);
	void handle_client_updateRemovedPadOnline(const Client::Pad & pad);

	void handle_client_createdNewPad(const Client::Pad & pad);

	void handle_client_connectedToPad(const Client::Pad & pad,
									  const QHash<id_t,Client::User>,
									  const QString&);

private:
	QVector<Client::Pad> m_padsVector;
	MainClient * m_client = nullptr;


	// QAbstractItemModel interface
public:
	Qt::ItemFlags flags(const QModelIndex & index) const override;
};

#endif // PADSMODEL_H
