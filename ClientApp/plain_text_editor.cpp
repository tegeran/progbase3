#include "plain_text_editor.h"
#include <QKeyEvent>
#include <QChar>
#include <QTextDocumentFragment>
#include <QTextCharFormat>

#include "text_editor.h"
#include "error.h"
#include "qext.h"
#include "editor_commands.h"


PlainTextEditor::PlainTextEditor(QWidget * const & parent)
	: QPlainTextEdit(parent),
	  m_client(nullptr),
	  m_document(QPlainTextEdit::document()){
	QPlainTextEdit::setUndoRedoEnabled(false);
}

PlainTextEditor::~PlainTextEditor(){}

void PlainTextEditor::setMainClient(MainClient * const & client){
	m_client = client;
	Qext::Object::connectMultipleSignalsToSlots(
		m_client,
		this,
		{
			SIGNAL(updateCharactersAdded(int,QTextDocumentFragment)),
			SLOT(handle_client_updateCharactersAdded(int,QTextDocumentFragment)),

			SIGNAL(updateCharactersRemoved(int,int)),
			SLOT(handle_client_updateCharactersRemoved(int,int)),

			SIGNAL(connectedToPad(Client::Pad,QHash<id_t,Client::User>,QString)),
			SLOT(handle_client_connectedToPad(Client::Pad,QHash<id_t,Client::User>,QString)),

			SIGNAL(createdNewPad(Client::Pad)),
			SLOT(handle_client_createdNewPad(Client::Pad)),

			SIGNAL(changedColor(QColor)),
			SLOT(handle_client_changedColor(QColor)),

			SIGNAL(disconnectedFromPad()),
			SLOT(stopDataTransmitting()),

			SIGNAL(disconnectedFromServer()),
			SLOT(stopDataTransmitting())
		}
	);
}
void PlainTextEditor::startDataTransmitting(){
	connect(
		m_document,
		SIGNAL(contentsChange(int,int,int)),
		this,
		SLOT(handle_document_contentsChange(int,int,int)),
		Qt::UniqueConnection
	);
}

void PlainTextEditor::stopDataTransmitting(){
	disconnect(
		m_document,
		SIGNAL(contentsChange(int,int,int)),
		this,
		SLOT(handle_document_contentsChange(int,int,int))
	);
}


void PlainTextEditor::handle_client_updateCharactersAdded(const int & position,
													 const QTextDocumentFragment & fragment){
	m_externalEdit = true;
	QTextCharFormat prevFormat { QPlainTextEdit::currentCharFormat() };
	Qext::TextDocument::insertFragment(m_document, fragment, position);
	m_externalEdit = false;
	QPlainTextEdit::setCurrentCharFormat(prevFormat);
}

void PlainTextEditor::handle_client_updateCharactersRemoved(const int & position,
														  const int & amount){
	m_externalEdit = true;
	QTextCharFormat prevFormat { QPlainTextEdit::currentCharFormat() };
	Qext::TextDocument::removeText(m_document, position, amount);
	m_externalEdit = false;
	QPlainTextEdit::setCurrentCharFormat(prevFormat);
}


void PlainTextEditor::handle_document_contentsChange(int index,
												int charsRemoved,
												int charsAdded){
	correctBugs(index, charsRemoved, charsAdded);
	if (m_externalEdit) {
		return;
	}
	if (charsRemoved){
		m_client->sendCharactersRemoved(index, charsRemoved);
	}
	if (charsAdded){
		QTextCursor cursor { m_document };
		cursor.setPosition(index);
		cursor.anchor();
		cursor.setPosition(index + charsAdded, QTextCursor::KeepAnchor);
		m_client->sendCharactersAdded(index, cursor.selection());
	}

}


void PlainTextEditor::handle_client_createdNewPad(const Client::Pad &){
	QPlainTextEdit::clear();
	startDataTransmitting();
}



void PlainTextEditor::handle_client_connectedToPad(const Client::Pad &,
											  const QHash<id_t, Client::User> &,
											  const QString & newCurrentDocumentHtml){
	m_document->setHtml(newCurrentDocumentHtml);
	QTextCharFormat format { QPlainTextEdit::currentCharFormat() };
	format.setBackground(QBrush(m_client->clientUser().color()));
	QPlainTextEdit::setCurrentCharFormat(format);
	startDataTransmitting();
}



void PlainTextEditor::handle_client_changedColor(const QColor & color){
	QTextCursor cursor { QPlainTextEdit::textCursor() };
	cursor.clearSelection();
	QPlainTextEdit::setTextCursor(cursor);
	QTextCharFormat format { QPlainTextEdit::currentCharFormat() };
	format.setBackground(QBrush(color));
	QPlainTextEdit::setCurrentCharFormat(format);
}
















void PlainTextEditor::handle_bold_clicked(const bool & checked){
	QTextCharFormat format { QPlainTextEdit::currentCharFormat() };
	format.setFontWeight(checked ? QFont::Bold : QFont::Normal);
	QPlainTextEdit::setCurrentCharFormat(format);
}

void PlainTextEditor::handle_italic_clicked(const bool & checked){
	QTextCharFormat format { QPlainTextEdit::currentCharFormat() };
	format.setFontItalic(checked);
	QPlainTextEdit::setCurrentCharFormat(format);
}

void PlainTextEditor::handle_underline_clicked(const bool & checked){
	QTextCharFormat format { QPlainTextEdit::currentCharFormat() };
	format.setFontUnderline(checked);
	QPlainTextEdit::setCurrentCharFormat(format);
}

void PlainTextEditor::handle_strikeout_clicked(const bool & checked){
	QTextCharFormat format { QPlainTextEdit::currentCharFormat() };
	format.setFontStrikeOut(checked);
	QPlainTextEdit::setCurrentCharFormat(format);
}

void PlainTextEditor::handle_increaseFont_clicked(){
	qreal currentSize { m_fontSize->value() };
	if (currentSize >= MAX_FONT_POINTSIZE){ return; }
	m_fontSize->display(currentSize += 4);
	QTextCharFormat format { QPlainTextEdit::currentCharFormat() };
	format.setFontPointSize(currentSize);
	QPlainTextEdit::setCurrentCharFormat(format);
}


void PlainTextEditor::handle_decreaseFont_clicked(){
	qreal currentSize { m_fontSize->value() };
	if (currentSize <= MIN_FONT_POINTSIZE){ return; }
	m_fontSize->display(currentSize -= 4);
	QTextCharFormat format { QPlainTextEdit::currentCharFormat() };
	format.setFontPointSize(currentSize);
	QPlainTextEdit::setCurrentCharFormat(format);
}

void PlainTextEditor::handle_finalize_clicked(const bool & checked){
	QTextCharFormat format { QPlainTextEdit::currentCharFormat() };
	format.setBackground(
		QBrush(
			checked
				? QColor(Qt::white)
				: m_client->clientUser().color()
		)
	);
	QPlainTextEdit::setCurrentCharFormat(format);
}


void PlainTextEditor::handle_font_currentFontChanged(const QFont & font){
	QTextCharFormat format { QPlainTextEdit::currentCharFormat() };
	format.setFontFamily(font.family());
	QPlainTextEdit::setCurrentCharFormat(format);
}


void PlainTextEditor::setControlls(QPushButton * const & bold,
							  QPushButton * const & italic,
							  QPushButton * const & underline,
							  QPushButton * const & strikeout,
							  QPushButton * const & increaseFont,
							  QPushButton * const & decreaseFont,
							  QFontComboBox * const & font,
							  QPushButton * const & finalize,
							  QLCDNumber  * const & lcdFontSize){
	m_bold = bold;
	m_italic = italic;
	m_underline = underline;
	m_strikeout = strikeout;
	m_increaseFont = increaseFont;
	m_decreaseFont = decreaseFont;
	m_font = font;
	m_finalize = finalize;
	m_fontSize = lcdFontSize;
	m_fontSize->display(DEFAULT_FONT_POINTSIZE);
	connect(bold,   SIGNAL(clicked(bool)), this, SLOT(handle_bold_clicked(bool)));
	connect(italic, SIGNAL(clicked(bool)), this, SLOT(handle_italic_clicked(bool)));
	connect(underline, SIGNAL(clicked(bool)), this, SLOT(handle_underline_clicked(bool)));
	connect(strikeout, SIGNAL(clicked(bool)), this, SLOT(handle_strikeout_clicked(bool)));
	connect(increaseFont, SIGNAL(clicked(bool)), this, SLOT(handle_increaseFont_clicked()));
	connect(decreaseFont, SIGNAL(clicked(bool)), this, SLOT(handle_decreaseFont_clicked()));
	connect(finalize, SIGNAL(clicked(bool)), this, SLOT(handle_finalize_clicked(bool)));
	connect(font, SIGNAL(currentFontChanged(QFont)), this, SLOT(handle_font_currentFontChanged(QFont)));
	updateCurrentFormat();
}


void PlainTextEditor::updateCurrentFormat(){
	QTextCharFormat format;
	format.setFontWeight(m_bold->isChecked() ? QFont::Bold : QFont::Normal);
	format.setFontItalic(m_italic->		 isChecked());
	format.setFontUnderline(m_underline->isChecked());
	format.setFontStrikeOut(m_strikeout->isChecked());
	format.setFontPointSize(m_fontSize->value());
	format.setFontFamily(m_font->currentFont().family());
	if (m_finalize->isChecked()){
		format.setBackground(QBrush(QColor(Qt::white)));
	} else {
		format.setBackground(QBrush(m_client->clientUser().color()));
	}
	format.setForeground(QBrush(QColor(Qt::black)));
	QPlainTextEdit::setCurrentCharFormat(format);
}



















void PlainTextEditor::correctBugs(const int & index,
								int & charsRemoved,
								int & charsAdded){
	if (index > 0) {
		return;
	}
	if (charsAdded > 1){
		if (charsAdded > charsRemoved){
			charsAdded -= charsRemoved;
			charsRemoved = 0;
		} else {
			charsRemoved -= charsAdded;
			charsAdded = 0;
		}
	} else if (charsAdded == 1 && m_document->isEmpty()){
		--charsRemoved;
		charsAdded = 0;
	}
}



void PlainTextEditor::inputMethodEvent(QInputMethodEvent * event){
	event->ignore();
}

void PlainTextEditor::insertFromMimeData(const QMimeData * source){
	QTextCursor cursor { QPlainTextEdit::textCursor() };
	QLOG("INSERT_MIME, selection start -> "
		 << cursor.selectionStart()
		 << " end -> " << cursor.selectionEnd());
	if (cursor.hasSelection()) {
		cursor.removeSelectedText();
		QLOG("REMOVED_SELECTED_TEXT");
	}
	QPlainTextEdit::insertFromMimeData(source);
}

bool PlainTextEditor::event(QEvent * event){
//	QLOG(event->type());
	return QPlainTextEdit::event(event);
}

void PlainTextEditor::keyPressEvent(QKeyEvent * event){
	if (!m_client->clientUser().canModifyFinalText()
		&& Qext::TextDocument::isSurroundedByFinalText(
			QPlainTextEdit::document(), QPlainTextEdit::textCursor().position()
		)
	){
		event->ignore();
		QLOG_ERROR("cant modify this text");
		return;
	} else {
		QPlainTextEdit::keyPressEvent(event);
	}
}

void PlainTextEditor::dragEnterEvent(QDragEnterEvent * event){
	QLOG("DRAG_EVENT");
	QPlainTextEdit::textCursor().removeSelectedText();
	QPlainTextEdit::dragEnterEvent(event);
}

QMimeData * PlainTextEditor::createMimeDataFromSelection() const{
	QLOG("CREATE_MIME_DATA");
	return QPlainTextEdit::createMimeDataFromSelection();
}












