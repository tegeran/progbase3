#ifndef PLAIN_TEXT_EDITOR_H
#define PLAIN_TEXT_EDITOR_H
#include <QPlainTextEdit>
#include <QTextDocumentFragment>
#include <QPushButton>
#include <QFontComboBox>
#include <QFont>
#include <QLCDNumber>
#include <QUndoStack>

#include "main_client.h"
#include "client_pad.h"
#include "client_user.h"


class PlainTextEditor : public QPlainTextEdit {
	Q_OBJECT

public:
	static constexpr const qreal DEFAULT_FONT_POINTSIZE = 16;
	static constexpr const qreal MIN_FONT_POINTSIZE = 4;
	static constexpr const qreal MAX_FONT_POINTSIZE = 64;
	explicit PlainTextEditor(QWidget * const & parent = nullptr);

	~PlainTextEditor();

	void setMainClient(MainClient * const & client);
	void setControlls(QPushButton *   const & bold,
					  QPushButton *   const & italic,
					  QPushButton *   const & underline,
					  QPushButton *   const & strikeout,
					  QPushButton *   const & increaseFont,
					  QPushButton *   const & decreaseFont,
					  QFontComboBox * const & font,
					  QPushButton *   const & finalize,
					  QLCDNumber  *   const & lcdFontSize);

private slots:

	void handle_client_updateCharactersAdded(const int & position,
											  const QTextDocumentFragment & fragment);


	void handle_client_updateCharactersRemoved(const int & position,
											   const int & amount);

	void handle_client_connectedToPad(const Client::Pad &,
									  const QHash<id_t, Client::User> &,
									  const QString & newCurrentDocumentHtml);
	void handle_document_contentsChange(int index,
									 int charsRemoved,
									 int charsAdded);


	void handle_client_createdNewPad(const Client::Pad & pad);

	void handle_client_changedColor(const QColor & color);


	void startDataTransmitting();
	void stopDataTransmitting();

	void handle_bold_clicked(const bool & checked);
	void handle_italic_clicked(const bool & checked);
	void handle_underline_clicked(const bool & checked);
	void handle_strikeout_clicked(const bool & checked);
	void handle_increaseFont_clicked();
	void handle_decreaseFont_clicked();
	void handle_finalize_clicked(const bool & checked);
	void handle_font_currentFontChanged(const QFont & font);
protected:
	void inputMethodEvent(QInputMethodEvent * event) override;

	// QPlainTextEdit interface
	void insertFromMimeData(const QMimeData * source) override;

	// QWidget interface

	// QObject interface
public:
	bool event(QEvent * event) override;
	void keyPressEvent(QKeyEvent * event) override;

protected:
	// QWidget interface
	void dragEnterEvent(QDragEnterEvent * event) override;
	// QPlainTextEdit interface
	QMimeData *createMimeDataFromSelection() const override;

private:
	MainClient * m_client;
	QTextDocument * m_document;
	QPushButton * m_bold         = nullptr;
	QPushButton * m_italic       = nullptr;
	QPushButton * m_underline    = nullptr;
	QPushButton * m_strikeout    = nullptr;
	QPushButton * m_increaseFont = nullptr;
	QPushButton * m_decreaseFont = nullptr;
	QFontComboBox * m_font       = nullptr;
	QPushButton * m_finalize     = nullptr;
	QLCDNumber * m_fontSize      = nullptr;
	bool m_externalEdit = false;


	void correctBugs(const int & index,
					 int & charsRemoved,
					 int & charsAdded);

	void updateCurrentFormat();

	// QWidget interface
protected:
};

#endif // PLAIN_TEXT_EDITOR_H
