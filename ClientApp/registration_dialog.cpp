#include "registration_dialog.h"
#include "ui_registration_dialog.h"
#include "qext.h"
#include "client_user.h"

RegistrationDialog::RegistrationDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::RegistrationDialog)
{
	ui->setupUi(this);
	Qext::LineEdit::setValidator(ui->ledit_username, Client::User::NAME_REG_EXP);
	Qext::LineEdit::setValidator(ui->ledit_login,    Client::User::LOGIN_REG_EXP);
	Qext::LineEdit::setValidator(ui->ledit_password, Client::User::PASSWORD_REG_EXP);
	Qext::Object::connectMultipleSenders(
		{
			ui->ledit_username,
			ui->ledit_login,
			ui->ledit_password,
			ui->ledit_repeatPassword
		},
		SIGNAL(textChanged(QString)),
		this,
		SLOT(tryEnableRegister())
	);
}

RegistrationDialog::~RegistrationDialog(){
	delete ui;
}

QString RegistrationDialog::chosenUsername() const{
	return ui->ledit_username->text();
}

QString RegistrationDialog::chosenLogin() const{
	return ui->ledit_login->text();
}

QString RegistrationDialog::chosenPassword() const{
	return ui->ledit_password->text();
}


void RegistrationDialog::on_pb_register_clicked(){
	emit registrationRequest(
		chosenUsername(),
		chosenLogin(),
		chosenPassword()
	);
}

void RegistrationDialog::tryEnableRegister(){
	ui->pb_register->setEnabled(
		!Awaitable::isAwaiting()
		&& ui->ledit_username->hasAcceptableInput()
		&& ui->ledit_login->   hasAcceptableInput()
		&& ui->ledit_password->hasAcceptableInput()
		&& ui->ledit_password->text() == ui->ledit_repeatPassword->text()
	);
}

void RegistrationDialog::startAwaiting(){
	ui->pb_cancel->setEnabled(false);
	ui->pb_register->setEnabled(false);
}

void RegistrationDialog::stopAwaiting(){
	ui->pb_cancel->setEnabled(true);
	ui->pb_register->setEnabled(true);
	tryEnableRegister();
}

void RegistrationDialog::on_pb_cancel_clicked(){
	QDialog::reject();
}
