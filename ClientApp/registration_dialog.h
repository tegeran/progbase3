#ifndef REGISTRATION_DIALOG_H
#define REGISTRATION_DIALOG_H

#include <QDialog>
#include "awaitable.h"

namespace Ui {
	class RegistrationDialog;
}

class RegistrationDialog : public QDialog, public Awaitable{
	Q_OBJECT


public:
	explicit RegistrationDialog(QWidget *parent = 0);
	~RegistrationDialog();

	QString chosenUsername() const;
	QString chosenLogin() const;
	QString chosenPassword() const;

signals:
	void registrationRequest(const QString & username,
							 const QString & login,
							 const QString & password);

private slots:
	void on_pb_register_clicked();

	void tryEnableRegister();

	void on_pb_cancel_clicked();

private:
	Ui::RegistrationDialog *ui;



	// Awaitable interface
protected:
	void startAwaiting() override;
	void stopAwaiting() override;
};

#endif // REGISTRATION_DIALOG_H
