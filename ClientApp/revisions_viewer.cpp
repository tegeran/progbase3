#include <QMessageBox>

#include "revisions_viewer.h"
#include "ui_revisions_viewer.h"
#include "qext.h"
#include "revision.h"
#include "std_ext.h"
#include "data.h"


RevisionsViewer::RevisionsViewer(MainClient * const & client, QWidget * const & parent) :
	QDialog(parent),
	ui(new Ui::RevisionsViewer),
	m_client(client)
{
	ui->setupUi(this);
	ui->mainBrowser->setUndoRedoEnabled(false);
	Qext::Object::connectMultipleSignalsToSlots(
		m_client,
		this,
		{
			SIGNAL(disconnectedFromPad()),
			SLOT(close()),

			SIGNAL(disconnectedFromServer()),
			SLOT(close()),

			SIGNAL			  (currentLevelChanged(Client::User,Client::User::Level)),
			SLOT(handle_client_currentLevelChanged()),

			SIGNAL(restoredPad(QString)),
			SLOT(reject()),

			SIGNAL(receivedAvailableRevisions(timeid_t)),
			SLOT(handle_client_receivedAvailableRevisions(timeid_t)),

			SIGNAL(receivedFirstDocVersionRevisions(QString,timeid_t,timeid_t,QVector<Netpacket>)),
			SLOT(handle_client_receivedFirstDocVersionRevisions(QString,timeid_t,timeid_t,QVector<Netpacket>)),

			SIGNAL(receivedRevisionsRange(timeid_t,timeid_t,QVector<Netpacket>)),
			SLOT(handle_client_receivedRevisionsRange(timeid_t,timeid_t,QVector<Netpacket>))
		}
	);
	ui->pb_restore->setEnabled(
		m_client->clientUser().canRestorePad()
	);
	Qext::Dialog::closeOnFinished(this);
	Awaitable::await();
	m_client->sendAvailableRevisionsRequest();
	QLOG("created revisions viewer");
}

RevisionsViewer::~RevisionsViewer(){
	delete ui;
	QLOG("destroyed revisions viewer");
}

void RevisionsViewer::handle_client_receivedAvailableRevisions(const timeid_t & amount){
	if (!amount){
		QMessageBox * infoBox(
			new QMessageBox(
				QMessageBox::Information,
				"No revisions available",
				"SIGSEGV: no revisions are currently available for this pad",
				QMessageBox::Close,
				this
			)
		);
		Qext::Widget::deleteOnClosed(infoBox);
		connect(
			infoBox,
			SIGNAL(finished(int)),
			this,
			SLOT(reject())
		);
		infoBox->show();
	} else {
		ui->timeSlider->setMaximum(amount);
		m_client->sendFirstDocVersionRevisionsRequest(1, std_ext::mapToMaxbound(amount, DEFAULT_RANGE));
	}
}

void RevisionsViewer::handle_client_receivedFirstDocVersionRevisions(
		const QString & documentHtml,
		const timeid_t & firstTimeId,
		const timeid_t & lastTimeId,
		const QVector<Netpacket> & revisions)
{
	ui->mainBrowser->setHtml(documentHtml);
	QSUPPOSE(firstTimeId <= lastTimeId);
	setNewRevisionsRange(firstTimeId, lastTimeId, revisions);
	Awaitable::enableInteraction();
}

void RevisionsViewer::handle_client_currentLevelChanged(){
	ui->pb_restore->setEnabled(m_client->clientUser().canRestorePad());
}

void RevisionsViewer::handle_client_receivedRevisionsRange(
		const timeid_t & firstTimeId,
		const timeid_t & lastTimeId,
		const QVector<Netpacket> & revisions)
{
	QSUPPOSE(firstTimeId <= lastTimeId);
	setNewRevisionsRange(firstTimeId, lastTimeId, revisions);
	Awaitable::enableInteraction();
}


void RevisionsViewer::setNewRevisionsRange(
		const timeid_t &,
		const timeid_t & last,
		const QVector<Netpacket> & revisions)
{
	suspend_updates(ui->mainBrowser){

	for (const Netpacket & revision : revisions){
		switch(revision.type()){
			case Netpacket::Revision_AddChars:{
				Data::IndexAndTextFragment data{
					revision.dataPacket<Data::IndexAndTextFragment>()
				};
				m_undoStack.push(
					new Revision::AddChars(
						ui->mainBrowser->document(),
						data.index,
						Qext::TextDocument::fragmentSize(data.fragment),
						std::move(data.fragment)
					)
				);
				break;
			}
			case Netpacket::Revision_RemoveChars:{
				Data::IndexAndAmount data{
					revision.dataPacket<Data::IndexAndAmount>()
				};
				m_undoStack.push(
					new Revision::RemoveChars(
						ui->mainBrowser->document(),
						data.index,
						data.amount
					)
				);
				break;
			}
			default:{ QSHUTDOWN("undefined behaviour"); }
		}
	}
	while (m_lastRevision < last){
		QSUPPOSE(m_undoStack.canUndo());
		m_undoStack.undo();
		++m_lastRevision;
	}
	QSUPPOSE(std_ext::isEqual(m_lastRevision, m_undoStack.count()));

	} // suspend_updates(ui->mainBrowser)
}

void RevisionsViewer::setCurrentRevision(const timeid_t & revision){
	if (m_currentRevision < revision){
		do {
			QSUPPOSE(m_undoStack.canRedo());
			m_undoStack.redo();
		} while (++m_currentRevision != revision);
	} else while (m_currentRevision != revision){
		QSUPPOSE(m_undoStack.canUndo());
		m_undoStack.undo();
		--m_currentRevision;
	}
	ui->revisionCounter->display(static_cast<int>(m_currentRevision));
}



void RevisionsViewer::on_timeSlider_valueChanged(int value){
	if (std_ext::isLess(m_lastRevision, value)){
		m_client->sendRevisionsRangeRequest(
			m_lastRevision + 1,
			std_ext::mapToMaxbound(
				m_lastRevision + DEFAULT_RANGE,
				ui->timeSlider->maximum()
			)
		);
		mute_signals(ui->timeSlider){
			ui->timeSlider->setValue(m_currentRevision);
		}
		setCurrentRevision(m_lastRevision);
		Awaitable::await();
	} else {
		setCurrentRevision(static_cast<timeid_t>(value));
	}
}


void RevisionsViewer::startAwaiting(){
	Qext::Widget::disableWidgets(
		ui->timeSlider, ui->pb_cancel, ui->pb_restore
	);
}

void RevisionsViewer::stopAwaiting(){
	Qext::Widget::enableWidgets(
		ui->timeSlider, ui->pb_cancel
	);
	ui->pb_restore->setEnabled(m_client->clientUser().canRestorePad());
}


void RevisionsViewer::on_pb_cancel_clicked(){
	reject();
}

void RevisionsViewer::on_pb_restore_clicked(){
	m_client->requireRestorePad(m_currentRevision);
}
