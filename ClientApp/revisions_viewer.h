﻿#ifndef REVISIONS_VIEWER_H
#define REVISIONS_VIEWER_H

#include <QDialog>
#include <QUndoStack>
#include <QStack>

#include <deque>

#include "awaitable.h"
#include "main_client.h"
#include "sharedlib_global.h"

namespace Ui {
	class RevisionsViewer;
}

class RevisionsViewer : public QDialog, public Awaitable
{
	Q_OBJECT

public:
	explicit RevisionsViewer(MainClient * const & client,
							 QWidget * const & parent = nullptr);
	~RevisionsViewer();

private slots:
	void handle_client_receivedAvailableRevisions(const timeid_t & amount);
	void handle_client_receivedFirstDocVersionRevisions(
		const QString & documentHtml,
		const timeid_t & firstTimeId,
		const timeid_t &  lastTimeId,
		const QVector<Netpacket> & revisions
	);

	void handle_client_currentLevelChanged();

	void handle_client_receivedRevisionsRange(
		const timeid_t & firstTimeId,
		const timeid_t &  lastTimeId,
		const QVector<Netpacket> & revisions
	);

	void on_timeSlider_valueChanged(int value);

	void on_pb_cancel_clicked();

	void on_pb_restore_clicked();

private:
	Ui::RevisionsViewer * ui;
	MainClient * const & m_client;
	QUndoStack m_undoStack;
	timeid_t m_lastRevision = 0;
	timeid_t m_currentRevision = 0;
	QVector<Netpacket> m_pendingRevisions;

	static constexpr const timeid_t DEFAULT_RANGE = 50ULL;

	void setNewRevisionsRange(
		const timeid_t & first,
		const timeid_t & last,
		const QVector<Netpacket> & revisions
	);



	void setCurrentRevision(const timeid_t & revision);
	// Awaitable interface
protected:
	void startAwaiting() override;
	void stopAwaiting() override;
};

#endif // REVISIONS_VIEWER_H
