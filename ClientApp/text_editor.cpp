#include <QKeyEvent>
#include <QChar>
#include <QTextDocumentFragment>
#include <QTextCharFormat>

#include "text_editor.h"
#include "error.h"
#include "qext.h"
#include "editor_commands.h"


TextEditor::TextEditor(QWidget * const & parent)
	: QTextEdit(parent),
	  m_client(nullptr),
	  m_document(QTextEdit::document()),
	  m_oldDocument(new QTextDocument(this))
{
	QTextEdit::setUndoRedoEnabled(false);
	startOfflineMode();
}

TextEditor::~TextEditor(){}

void TextEditor::setMainClient(MainClient * const & client){
	m_client = client;
	Qext::Object::connectMultipleSignalsToSlots(
		m_client,
		this,
		{
			SIGNAL            (currentLevelChanged(Client::User,Client::User::Level)),
			SLOT(handle_client_currentLevelChanged()),

			SIGNAL            (restoredPad(QString)),
			SLOT(handle_client_restoredPad(QString)),

			SIGNAL            (updateSomeUserRestoredPad(Client::User,QString)),
			SLOT(handle_client_updateSomeUserRestoredPad(Client::User,QString)),

			SIGNAL            (updateCharactersAdded(int,QTextDocumentFragment)),
			SLOT(handle_client_updateCharactersAdded(int,QTextDocumentFragment)),

			SIGNAL            (updateCharactersRemoved(int,int)),
			SLOT(handle_client_updateCharactersRemoved(int,int)),

			SIGNAL            (connectedToPad(Client::Pad,QHash<id_t,Client::User>,QString)),
			SLOT(handle_client_connectedToPad(Client::Pad,QHash<id_t,Client::User>,QString)),

			SIGNAL            (createdNewPad(Client::Pad)),
			SLOT(handle_client_createdNewPad(Client::Pad)),

			SIGNAL			  (changedColor(QColor)),
			SLOT(handle_client_changedColor(QColor)),

			SIGNAL            (disconnectedFromPad()),
			SLOT(handle_client_disconnectedFromPad()),

			SIGNAL			  (disconnectedFromServer()),
			SLOT(handle_client_disconnectedFromServer())
		}
	);
}



void TextEditor::handle_client_updateCharactersAdded(const int & position,
													 const QTextDocumentFragment & fragment){
	m_externalEdit = true;
	QTextCursor prevCursor = QTextEdit::textCursor();
	Qext::TextDocument::insertFragment(m_document, fragment, position);
	Qext::TextDocument::insertFragment(m_oldDocument, fragment, position);
	m_externalEdit = false;
	QTextEdit::setTextCursor(prevCursor);
}

void TextEditor::handle_client_updateCharactersRemoved(const int & position,
														  const int & amount){
	m_externalEdit = true;
	QTextCursor prevCursor = QTextEdit::textCursor();
	Qext::TextDocument::removeText(m_document, position, amount);
	Qext::TextDocument::removeText(m_oldDocument, position, amount);
	m_externalEdit = false;
	QTextEdit::setTextCursor(prevCursor);
}


void TextEditor::handle_document_contentsChange(int index,
												int charsRemoved,
												int charsAdded){
	if (m_externalEdit) {
		return;
	}
	suspend_updates(this){

		correctBugs(index, charsRemoved, charsAdded);
		QTextDocumentFragment addedFragment;
		QTextCursor cursorForAdded;
		if (charsAdded){
			cursorForAdded = Qext::TextDocument::cursorSelect(
				m_document,
				index,
				index + charsAdded
			);
			cursorForAdded.setCharFormat(realCharFormat());
			addedFragment = cursorForAdded.selection();
		}
		if (charsRemoved){
			QTextCursor cursor {
				Qext::TextDocument::cursorSelect(m_oldDocument, index, index + charsRemoved)
			};
			mute_signals(m_document){
				Qext::TextDocument::insertFragment(
					m_document,
					cursor.selection(),
					index
				);
			}
			m_client->sendCharactersRemoved(index, charsRemoved);
		}
		if (charsAdded){
			m_client->sendCharactersAdded(index, addedFragment);
			mute_signals(m_document){
				cursorForAdded.removeSelectedText();
			}
		}

	}
}

void TextEditor::handle_client_createdNewPad(const Client::Pad &){
	stopOfflineMode();
	updateFinalizeButton();
	startOnlineMode();
}

void TextEditor::handle_client_connectedToPad(const Client::Pad &,
											  const QHash<id_t, Client::User> &,
											  const QString & newCurrentDocumentHtml){
	stopOfflineMode();
	stopOnlineMode();
	updateFinalizeButton();
	QTextEdit::setHtml(newCurrentDocumentHtml);
	m_oldDocument->setHtml(newCurrentDocumentHtml);
	startOnlineMode();
}


void TextEditor::handle_client_changedColor(const QColor & color){
	QTextCursor cursor { QTextEdit::textCursor() };
	cursor.clearSelection();
	QTextEdit::setTextCursor(cursor);
	QTextEdit::setTextBackgroundColor(color);
}

void TextEditor::handle_client_updateSomeUserRestoredPad(
		const Client::User &,
		const QString & newDocumentHtml){
	m_externalEdit = true;
	m_document->setHtml(newDocumentHtml);
	m_oldDocument->setHtml(newDocumentHtml);
	m_externalEdit = false;
}


void TextEditor::handle_client_restoredPad(const QString & newDocumentHtml){
	handle_client_updateSomeUserRestoredPad(m_client->clientUser(), newDocumentHtml);
}














void TextEditor::handle_bold_clicked(const bool & checked){
	if (!canModifySelectedFragment()){
		return;
	}
	QTextEdit::setFontWeight(checked ? QFont::Bold : QFont::Normal);
}

void TextEditor::handle_italic_clicked(const bool & checked){
	if (!canModifySelectedFragment()){
		return;
	}
	QTextEdit::setFontItalic(checked);
}

void TextEditor::handle_underline_clicked(const bool & checked){
	if (!canModifySelectedFragment()){
		return;
	}
	QTextEdit::setFontUnderline(checked);
}

void TextEditor::handle_strikeout_clicked(const bool & checked){
	if (!canModifySelectedFragment()){
		return;
	}
	QTextCharFormat format { QTextEdit::currentCharFormat() };
	format.setFontStrikeOut(checked);
	QTextEdit::setCurrentCharFormat(format);
}

void TextEditor::handle_increaseFont_clicked(){
	if (!canModifySelectedFragment()){
		return;
	}
	qreal currentSize { m_fontSize->value() };
	if (currentSize >= MAX_FONT_POINTSIZE){ return; }
	m_fontSize->display(currentSize += 4);
	QTextEdit::setFontPointSize(currentSize);
}


void TextEditor::handle_decreaseFont_clicked(){
	if (!canModifySelectedFragment()){
		return;
	}
	qreal currentSize { m_fontSize->value() };
	if (currentSize <= MIN_FONT_POINTSIZE){ return; }
	m_fontSize->display(currentSize -= 4);
	QTextEdit::setFontPointSize(currentSize);
}

void TextEditor::handle_finalize_clicked(const bool & checked){
	QTextEdit::setTextBackgroundColor(
		checked
			? QColor(Client::User::finalColor())
			: m_client->clientUser().color()
	);
}


void TextEditor::handle_font_currentFontChanged(const QFont & font){
	QTextCharFormat format { QTextEdit::currentCharFormat() };
	format.setFontFamily(font.family());
	QTextEdit::setCurrentCharFormat(format);
}



void TextEditor::setControlls(QPushButton * const & bold,
							  QPushButton * const & italic,
							  QPushButton * const & underline,
							  QPushButton * const & strikeout,
							  QPushButton * const & increaseFont,
							  QPushButton * const & decreaseFont,
							  QFontComboBox * const & font,
							  QPushButton * const & finalize,
							  QLCDNumber  * const & lcdFontSize){
	m_bold = bold;
	m_italic = italic;
	m_underline = underline;
	m_strikeout = strikeout;
	m_increaseFont = increaseFont;
	m_decreaseFont = decreaseFont;
	m_font = font;
	m_finalize = finalize;
	m_fontSize = lcdFontSize;
	m_fontSize->display(DEFAULT_FONT_POINTSIZE);
	connect(bold,   SIGNAL(clicked(bool)), this, SLOT(handle_bold_clicked(bool)));
	connect(italic, SIGNAL(clicked(bool)), this, SLOT(handle_italic_clicked(bool)));
	connect(underline, SIGNAL(clicked(bool)), this, SLOT(handle_underline_clicked(bool)));
	connect(strikeout, SIGNAL(clicked(bool)), this, SLOT(handle_strikeout_clicked(bool)));
	connect(increaseFont, SIGNAL(clicked(bool)), this, SLOT(handle_increaseFont_clicked()));
	connect(decreaseFont, SIGNAL(clicked(bool)), this, SLOT(handle_decreaseFont_clicked()));
	connect(finalize, SIGNAL(clicked(bool)), this, SLOT(handle_finalize_clicked(bool)));
	connect(font, SIGNAL(currentFontChanged(QFont)), this, SLOT(handle_font_currentFontChanged(QFont)));
}



QTextCharFormat TextEditor::realCharFormat() const{
	QTextCharFormat format;
	format.setFontWeight(m_bold->isChecked() ? QFont::Bold : QFont::Normal);
	format.setFontItalic(m_italic->		 isChecked());
	format.setFontUnderline(m_underline->isChecked());
	format.setFontStrikeOut(m_strikeout->isChecked());
	format.setFontPointSize(m_fontSize->value());
	format.setFontFamily(m_font->currentFont().family());
	if (m_finalize->isChecked()){
		format.setBackground(QBrush(QColor(Client::User::finalColor())));
	} else {
		format.setBackground(QBrush(m_client->clientUser().color()));
	}
	format.setForeground(QBrush(QColor(Qt::black)));
	return format;
}

bool TextEditor::hasSelectedWhiteText(){
	QTextCursor currentCursor { QTextEdit::textCursor() };
	return currentCursor.hasSelection()
		&&	Qext::TextDocument::containsFinalText(
		m_document,
		currentCursor.selectionStart(),
		currentCursor.selectionEnd() - currentCursor.selectionStart()
	);
}

bool TextEditor::hasWhiteTextToTheLeft(){
	return Qext::TextDocument::containsFinalText(
			m_document,
			QTextEdit::textCursor().position() - 1,
			1
	);
}

bool TextEditor::hasWhiteTextToTheRight(){
	return Qext::TextDocument::containsFinalText(
			m_document,
			QTextEdit::textCursor().position(),
			1
	);
}

bool TextEditor::canModifySelectedFragment(){
	return m_client->clientUser().canModifyFinalText()
			|| !hasSelectedWhiteText();
}

QString TextEditor::prepareDocumentForNewPad(){
	stopOfflineMode();
	stopOnlineMode();
	QTextCursor cursor { Qext::TextDocument::cursorSelectAll(m_document) };
	cursor.setCharFormat(realCharFormat());
	QString newHtml { cursor.selection().toHtml() };
	m_oldDocument->setHtml(newHtml);
	return newHtml;
}

void TextEditor::updateFinalizeButton(){
	if (m_client->isConnectedToPad()
			&& !m_client->clientUser().canModifyFinalText()){
		m_finalize->setChecked(false);
		m_finalize->setEnabled(false);
	} else {
		m_finalize->setEnabled(true);
	}
}


void TextEditor::handle_client_currentLevelChanged(){
	updateFinalizeButton();
}



void TextEditor::handle_offlineSuper_contentsChange(int index, int charsRemoved, int charsAdded){
	QSUPPOSE(!m_client || !m_client->isConnectedToPad());
	correctBugs(index, charsRemoved, charsAdded);
	if (charsAdded){
		mute_signals(m_document){
			Qext::TextDocument::cursorSelect(m_document, index, index + charsAdded)
				.setCharFormat(realCharFormat());
		}
	}
}

void TextEditor::handle_client_disconnectedFromPad(){
	stopOnlineMode();
	startOfflineMode();
}

void TextEditor::handle_client_disconnectedFromServer(){
	stopOnlineMode();
	startOfflineMode();
}









void TextEditor::startOnlineMode(){
	connect(
		m_document,
		SIGNAL(contentsChange(int,int,int)),
		this,
		SLOT(handle_document_contentsChange(int,int,int)),
		Qt::UniqueConnection
	);
	QLOG("editor switched to online mode");
}

void TextEditor::stopOnlineMode(){
	disconnect(
		m_document,
		SIGNAL(contentsChange(int,int,int)),
		this,
		SLOT(handle_document_contentsChange(int,int,int))
	);
	QLOG("editor switched off from online mode");
}
void TextEditor::startOfflineMode(){
	connect(
		m_document,
		SIGNAL(contentsChange(int,int,int)),
		this,
		SLOT(handle_offlineSuper_contentsChange(int,int,int)),
		Qt::UniqueConnection
	);
	QLOG("editor switched to offline mode");
}


void TextEditor::stopOfflineMode(){
	disconnect(
		m_document,
		SIGNAL(contentsChange(int,int,int)),
		this,
		SLOT(handle_offlineSuper_contentsChange(int,int,int))
	);
	QLOG("editor switched off from offline mode");
}


void TextEditor::correctBugs(const int & index,
								int & charsRemoved,
								int & charsAdded){
	QLOG("BEFORE: i -> " << index
		 << " removed -> " << charsRemoved
		 << " added -> " << charsAdded);
	if (index > 0 || (charsAdded == charsRemoved
			&& QTextEdit::textCursor().hasSelection())) {
		return;
	}


	if (charsAdded > 1){
		if (charsAdded > charsRemoved){
			charsAdded -= charsRemoved;
			charsRemoved = 0;
		} else {
			charsRemoved -= charsAdded;
			charsAdded = 0;
		}
	} else if (charsAdded == 1 && m_document->isEmpty()){
		--charsRemoved;
		charsAdded = 0;
	}
	QLOG("AFTER: i -> " << index
		 << " removed -> " << charsRemoved
		 << " added -> " << charsAdded);
}



void TextEditor::inputMethodEvent(QInputMethodEvent * event){
	event->ignore();
}

void TextEditor::insertFromMimeData(const QMimeData * source){
	QTextCursor cursor { QTextEdit::textCursor() };
	int cursorSelectionStart { cursor.selectionStart() };
	QLOG("INSERT_MIME, selection start -> "
		 << cursorSelectionStart
		 << " end -> " << cursor.selectionEnd());
	if (cursor.hasSelection()) {
		cursor.removeSelectedText();
		QLOG("REMOVED_SELECTED_TEXT");
		cursor.setPosition(cursorSelectionStart);
		QTextEdit::setTextCursor(cursor);
	};
	QTextEdit::insertFromMimeData(source);
}


void TextEditor::keyPressEvent(QKeyEvent * event){
	if (!m_client->clientUser().canModifyFinalText()){

		if ((hasSelectedWhiteText() || Qext::TextDocument::isSurroundedByFinalText(
				QTextEdit::document(), QTextEdit::textCursor().position()))
				&& QChar(event->key()).isPrint()){
			event->ignore();
			QLOG_ERROR("can't selected text or inside white text");
			return;
		}

		switch (event->key()){
			case Qt::Key_Backspace:{
				if (hasWhiteTextToTheLeft()){
					event->ignore();
					QLOG_ERROR("can't modify white text to the left");
					return;
				}
				break;
			}
			case Qt::Key_Delete:{
				if (hasWhiteTextToTheRight()){
					event->ignore();
					QLOG_ERROR("cant modify white text to the right");
					return;
				}
				break;
			}
		}

	}
	QTextEdit::keyPressEvent(event);
}

void TextEditor::dragEnterEvent(QDragEnterEvent * event){
	QLOG("DRAG_EVENT");
	QTextEdit::textCursor().removeSelectedText();
	QTextEdit::dragEnterEvent(event);
}












