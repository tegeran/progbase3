#ifndef TEXT_EDITOR_H
#define TEXT_EDITOR_H
#include <QTextEdit>
#include <QTextDocumentFragment>
#include <QPushButton>
#include <QFontComboBox>
#include <QFont>
#include <QLCDNumber>
#include <QUndoStack>

#include "main_client.h"
#include "client_pad.h"
#include "client_user.h"

namespace EditorCommand{
	class AddCharacters;
	class RemoveCharacters;
}

class TextEditor : public QTextEdit {
	Q_OBJECT
	friend class EditorCommand::AddCharacters;
	friend class EditorCommand::RemoveCharacters;

public:
	static constexpr const qreal DEFAULT_FONT_POINTSIZE = 16;
	static constexpr const qreal MIN_FONT_POINTSIZE = 4;
	static constexpr const qreal MAX_FONT_POINTSIZE = 64;
	explicit TextEditor(QWidget * const & parent = nullptr);

	~TextEditor();

	void setMainClient(MainClient * const & client);
	void setControlls(QPushButton *   const & bold,
					  QPushButton *   const & italic,
					  QPushButton *   const & underline,
					  QPushButton *   const & strikeout,
					  QPushButton *   const & increaseFont,
					  QPushButton *   const & decreaseFont,
					  QFontComboBox * const & font,
					  QPushButton *   const & finalize,
					  QLCDNumber  *   const & lcdFontSize);

	QString prepareDocumentForNewPad();

private slots:
	void handle_client_currentLevelChanged();

	void handle_client_restoredPad(const QString & newDocumentHtml);

	void handle_client_updateSomeUserRestoredPad(
		const Client::User & user,
		const QString & newDocumentHtml
	);

	void handle_offlineSuper_contentsChange(int index, int charsRemoved, int charsAdded);
	void handle_client_disconnectedFromPad();
	void handle_client_disconnectedFromServer();

	void handle_client_updateCharactersAdded(const int & position,
											  const QTextDocumentFragment & fragment);


	void handle_client_updateCharactersRemoved(const int & position,
											   const int & amount);

	void handle_client_connectedToPad(const Client::Pad &,
									  const QHash<id_t, Client::User> &,
									  const QString & newCurrentDocumentHtml);
	void handle_document_contentsChange(int index,
									 int charsRemoved,
									 int charsAdded);


	void handle_client_createdNewPad(const Client::Pad & pad);

	void handle_client_changedColor(const QColor & color);


	void startOnlineMode();
	void stopOnlineMode();

	void handle_bold_clicked(const bool & checked);
	void handle_italic_clicked(const bool & checked);
	void handle_underline_clicked(const bool & checked);
	void handle_strikeout_clicked(const bool & checked);
	void handle_increaseFont_clicked();
	void handle_decreaseFont_clicked();
	void handle_finalize_clicked(const bool & checked);
	void handle_font_currentFontChanged(const QFont & font);

	void updateFinalizeButton();
protected:
	void inputMethodEvent(QInputMethodEvent * event) override;
	void insertFromMimeData(const QMimeData * source) override;

public:
	void keyPressEvent(QKeyEvent * event) override;

protected:
	// QWidget interface
	void dragEnterEvent(QDragEnterEvent * event) override;

private:
	MainClient * m_client;
	QTextDocument * m_document;
	QPushButton * m_bold         = nullptr;
	QPushButton * m_italic       = nullptr;
	QPushButton * m_underline    = nullptr;
	QPushButton * m_strikeout    = nullptr;
	QPushButton * m_increaseFont = nullptr;
	QPushButton * m_decreaseFont = nullptr;
	QFontComboBox * m_font       = nullptr;
	QPushButton * m_finalize     = nullptr;
	QLCDNumber * m_fontSize      = nullptr;
	bool m_externalEdit = false;
	QTextDocument * m_oldDocument;

	void correctBugs(const int & index,
					 int & charsRemoved,
					 int & charsAdded);

	bool hasSelectedWhiteText();
	bool hasWhiteTextToTheLeft();
	bool hasWhiteTextToTheRight();
	bool canModifySelectedFragment();
	QTextCharFormat realCharFormat() const;

	void startOfflineMode();
	void stopOfflineMode();

	// QWidget interface
protected:
};

#endif // TEXT_EDITOR_H
