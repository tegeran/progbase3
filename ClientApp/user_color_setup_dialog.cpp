#include <QPalette>
#include <QPixmap>
#include <QColorDialog>

#include "user_color_setup_dialog.h"
#include "ui_user_color_setup_dialog.h"
#include "client_user.h"
#include "clickable_rect.h"
#include "qext.h"

UserColorSetupDialog::UserColorSetupDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::UserColorSetupDialog)
{
	ui->setupUi(this);
}

UserColorSetupDialog::~UserColorSetupDialog(){
	delete ui;
}

QColor UserColorSetupDialog::chosenColor() const{
	return ui->colorRect->fillColor();
}

void UserColorSetupDialog::on_pb_apply_clicked(){
	QDialog::accept();
}
