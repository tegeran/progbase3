#ifndef USER_COLOR_SETUP_DIALOG_H
#define USER_COLOR_SETUP_DIALOG_H

#include <QDialog>

namespace Ui {
	class UserColorSetupDialog;
}

class UserColorSetupDialog : public QDialog
{
	Q_OBJECT

public:
	explicit UserColorSetupDialog(QWidget *parent = 0);
	~UserColorSetupDialog();

	QColor chosenColor() const;

private slots:
	void on_pb_apply_clicked();

private:
	Ui::UserColorSetupDialog *ui;
};

#endif // USER_COLOR_SETUP_DIALOG_H
