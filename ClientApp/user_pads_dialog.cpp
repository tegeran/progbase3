#include "user_pads_dialog.h"
#include "ui_user_pads_dialog.h"
#include "qext.h"

UserPadsDialog::UserPadsDialog(
		MainClient * const & client,
		UserPadsModel * const & model,
		QWidget * const & parent
)
	: QDialog(parent),
	  ui(new Ui::UserPadsDialog),
	  m_client(client)
{
	ui->setupUi(this);
	ui->tv_pads->setModel(model);
	ui->tv_pads->setSelectionMode(QAbstractItemView::SingleSelection);
	ui->tv_pads->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui->tv_pads->setEditTriggers(QTableView::NoEditTriggers);

	ui->tv_pads->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	Qext::Object::connectMultipleSignals(
		ui->tv_pads->selectionModel(),
		{
			SIGNAL(currentChanged(QModelIndex,QModelIndex)),
			SIGNAL(currentRowChanged(QModelIndex,QModelIndex)),
			SIGNAL(currentColumnChanged(QModelIndex,QModelIndex))
		},
		this,
		SLOT(handle_currentIndexChanged())
	);
	Qext::Dialog::closeOnFinished(this);
	Qext::Object::connectMultipleSignals(
		m_client,
		{
			SIGNAL(connectedToPad(Client::Pad,QHash<id_t,Client::User>,QString)),
			SIGNAL(createdNewPad(Client::Pad)),
			SIGNAL(disconnectedFromServer())
		},
		this,
		SLOT(reject())
	);
	validateConnectButton();
	QLOG("created user pads dialog");
}

UserPadsDialog::~UserPadsDialog(){
	delete ui;
	QLOG("deleted user pads dialog");
}

void UserPadsDialog::handle_currentIndexChanged(){
	validateConnectButton();
}

void UserPadsDialog::validateConnectButton(){
	ui->pb_connectToPad->setEnabled(
		ui->tv_pads->currentIndex().isValid()
		&& static_cast<UserPadsModel *>(ui->tv_pads->model())
			->padFromRow(ui->tv_pads->currentIndex().row()).id()
			!= m_client->currentPadId()
	);
}

void UserPadsDialog::startAwaiting(){
	Qext::Widget::disableWidgets(ui->pb_connectToPad, ui->pb_cancel);
}

void UserPadsDialog::stopAwaiting(){
	Qext::Widget::enableWidgets(ui->pb_connectToPad, ui->pb_cancel);
}


void UserPadsDialog::on_pb_cancel_clicked(){
	reject();
}

void UserPadsDialog::on_tv_pads_doubleClicked(const QModelIndex &index){
	if (index.isValid()
			&& !Awaitable::isAwaiting()
			&& static_cast<UserPadsModel *>(ui->tv_pads->model())
			->padFromRow(index.row()).id() != m_client->currentPadId()){
		m_client->connectToPad(
			static_cast<UserPadsModel *>(ui->tv_pads->model())
					->padFromRow(index.row()).id()
		);
		Awaitable::await();
	}
}

void UserPadsDialog::on_pb_connectToPad_clicked(){
	QModelIndex index { ui->tv_pads->currentIndex() };
	if (index.isValid()
			&& !Awaitable::isAwaiting()
			&& static_cast<UserPadsModel *>(ui->tv_pads->model())
			->padFromRow(index.row()).id() != m_client->currentPadId()){

		m_client->connectToPad(
			static_cast<UserPadsModel *>(ui->tv_pads->model())
				->padFromRow(ui->tv_pads->currentIndex().row()).id()
		);
		Awaitable::await();
	}
}
