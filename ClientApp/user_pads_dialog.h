#ifndef USER_PADS_DIALOG_H
#define USER_PADS_DIALOG_H

#include <QDialog>
#include "user_pads_model.h"
#include "main_client.h"
#include "awaitable.h"

namespace Ui {
	class UserPadsDialog;
}

class UserPadsDialog : public QDialog, public Awaitable
{
	Q_OBJECT

public:
	explicit UserPadsDialog(
		MainClient * const & client,
		UserPadsModel * const & model,
		QWidget * const & parent = nullptr
	);
	~UserPadsDialog();

private slots:
	void handle_currentIndexChanged();

	void on_pb_cancel_clicked();

	void on_tv_pads_doubleClicked(const QModelIndex &index);

	void on_pb_connectToPad_clicked();

private:
	Ui::UserPadsDialog *ui;
	MainClient * const & m_client;

	void validateConnectButton();


	// Awaitable interface
protected:
	void startAwaiting() override;
	void stopAwaiting() override;
};

#endif // USER_PADS_DIALOG_H
