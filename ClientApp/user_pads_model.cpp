#include "user_pads_model.h"
#include "qext.h"

enum Column { Level, Name, IsPublic };

UserPadsModel::UserPadsModel(MainClient * const & client, QObject * const & parent)
	: QAbstractTableModel(parent), m_client(client)
{
	Qext::Object::connectMultipleSignalsToSlots(
		client,
		this,
		{
			SIGNAL(createdNewPad(Client::Pad)),
			SLOT(handle_client_createdNewPad()),

			SIGNAL(authorized(Client::User,QHash<id_t,Client::Pad>)),
			SLOT(handle_client_authorized())
		}
	);
}

QVariant UserPadsModel::headerData(int section, Qt::Orientation orientation, int role) const{
	if (orientation == Qt::Vertical){
		return QVariant();
	}
	switch (section){
		case Level:{
			switch (role){
				case Qt::DisplayRole:{
					return QString("Level");
				}
			}
			return QVariant();
		}
		case Name:{
			switch (role){
				case Qt::DisplayRole:{
					return QString("Pad name");
				}
			}
			return QVariant();
		}
		case IsPublic:{
			switch (role){
				case Qt::DisplayRole:{
					return QString("Privacy");
				}
			}
			return QVariant();
		}
	}
	return QVariant();
}

int UserPadsModel::rowCount(const QModelIndex &parent) const{
	return parent.isValid()
		? 0
		: m_client->adminPads().size()
		  + m_client->ownerPads().size()
		  + m_client->authorPads().size();
}

int UserPadsModel::columnCount(const QModelIndex &parent) const
{
	return parent.isValid()
			? 0
			: 3;
}

QVariant UserPadsModel::data(const QModelIndex &index, int role) const {
	if (!index.isValid())
		return QVariant();
	if (padFromRow(index.row()).id() == m_client->currentPadId()){
		switch (role){
			case Qt::BackgroundColorRole:{
				return QBrush(QColor::fromRgb(153, 248, 218));
			}
			case Qt::ForegroundRole:{
				return QBrush(Qt::black);
			}
		}

	}
	switch (index.column()){
		case Level:{
			switch (role){
				case Qt::DisplayRole: {
					if (isRowInOwnerSection(index.row())){
						return QString("owner");
					} else if (isRowInAdminSection(index.row())){
						return QString("admin");
					} else {
						return QString("author");
					}
				}
			}
			return QVariant();
		}
		case Name:{
			switch (role){
				case Qt::DisplayRole: {
					return padFromRow(index.row()).name();
				}
			}
			return QVariant();
		}
		case IsPublic:{
			switch (role){
				case Qt::DisplayRole: {
					return padFromRow(index.row()).isPublic()
						? QString("public")
						: QString("private");
				}
				case Qt::BackgroundColorRole: {
					return padFromRow(index.row()).isPublic()
							? QBrush(QColor(76, 102, 5))
							: QBrush(QColor(91, 37, 19));
				}
				case Qt::ForegroundRole:{
					return QBrush(Qt::white);
				}
			}
			return QVariant();
		}
	}
	return QVariant();
}

void UserPadsModel::handle_client_createdNewPad(){
	beginInsertRows(QModelIndex(), 0, 0);
	endInsertRows();
}

void UserPadsModel::handle_client_authorized(){
	beginResetModel();
	endResetModel();
}

bool UserPadsModel::isRowInOwnerSection(const int & row) const{
	return row < m_client->ownerPads().size();
}

bool UserPadsModel::isRowInAdminSection(const int & row) const{
	return !isRowInOwnerSection(row)
			&& row < m_client->ownerPads().size() + m_client->adminPads().size();
}

bool UserPadsModel::isRowInAuthorSection(const int & row) const{
	return !isRowInAdminSection(row);
}

const Client::Pad &UserPadsModel::padFromRow(const int & row) const{
	return isRowInOwnerSection(row)
			? m_client->ownerPads().at(mapRowToOwnerIndex(row))
			: (isRowInAdminSection(row)
			   ? m_client->adminPads().at(mapRowToAdminIndex(row))
			   : m_client->authorPads().at(mapRowToAuthorIndex(row)));
}

int UserPadsModel::mapRowToOwnerIndex(const int & row) const{
	return row;
}

int UserPadsModel::mapRowToAdminIndex(const int & row) const{
	return row - m_client->ownerPads().size();
}

int UserPadsModel::mapRowToAuthorIndex(const int & row) const{
	return row - m_client->adminPads().size() - m_client->ownerPads().size();
}

Qt::ItemFlags UserPadsModel::flags(const QModelIndex & index) const{
	if (index.isValid()){
		return padFromRow(index.row()).id() == m_client->currentPadId()
					? Qt::ItemIsEnabled
					: (Qt::ItemIsEnabled | Qt::ItemIsSelectable);
	} else {
		return Qt::NoItemFlags;
	}
}
