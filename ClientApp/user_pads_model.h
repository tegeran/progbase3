#ifndef USERPADSMODEL_H
#define USERPADSMODEL_H

#include <QAbstractTableModel>
#include "main_client.h"

class UserPadsModel : public QAbstractTableModel
{
	Q_OBJECT

public:
	explicit UserPadsModel(MainClient * const & client,
						   QObject * const & parent = nullptr);

	// Header:
	QVariant headerData(
		int section,
		Qt::Orientation orientation,
		int role = Qt::DisplayRole
	) const override;

	// Basic functionality:
	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
	const Client::Pad & padFromRow(const int & row) const;

private slots:
	void handle_client_createdNewPad();
	void handle_client_authorized();

private:
	MainClient * m_client;

	bool isRowInOwnerSection(const int & row) const;
	bool isRowInAdminSection(const int & row) const;
	bool isRowInAuthorSection(const int & row) const;


	int mapRowToOwnerIndex(const int & row) const;
	int mapRowToAdminIndex(const int & row) const;
	int mapRowToAuthorIndex(const int & row) const;

	// QAbstractItemModel interface
public:
	Qt::ItemFlags flags(const QModelIndex & index) const override;
};

#endif // USERPADSMODEL_H
