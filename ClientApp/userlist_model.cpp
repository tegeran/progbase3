#include "userlist_model.h"
#include "qext.h"

UserlistModel::UserlistModel(MainClient * const & client, QObject *parent)
	: QAbstractListModel(parent), m_client(client) {
	Qext::Object::connectMultipleSignalsToSlots(
		client,
		this,
		{
			SIGNAL			  (currentLevelChanged(Client::User,Client::User::Level)),
			SLOT(handle_client_currentLevelChanged(Client::User,Client::User::Level)),

			SIGNAL			  (someUserChangedLevel(Client::User,Client::User,Client::User::Level)),
			SLOT(handle_client_someUserChangedLevel(Client::User,Client::User,Client::User::Level)),

			SIGNAL(updateUserLeftPad(Client::User)),
			SLOT(removeUser(Client::User)),

			SIGNAL(userConnected(Client::User)),
			SLOT(addUser(Client::User)),

			SIGNAL(userDisconnected(Client::User)),
			SLOT(removeUser(Client::User)),

			SIGNAL            (connectedToPad(Client::Pad,QHash<id_t,Client::User>,QString)),
			SLOT(handle_client_connectedToPad(Client::Pad,QHash<id_t,Client::User>,QString)),

			SIGNAL            (createdNewPad(Client::Pad)),
			SLOT(handle_client_createdNewPad()),

			SIGNAL			  (disconnectedFromPad()),
			SLOT(handle_client_disconnectedFromPad()),

			SIGNAL			  (updateUserChangedColor(Client::User)),
			SLOT(handle_client_updateUserChangedColor(Client::User)),

			SIGNAL			  (changedColor(QColor)),
			SLOT(handle_client_changedColor(QColor))
		}
	);
}


int UserlistModel::rowCount(const QModelIndex &parent) const{
	return parent.isValid() ? 0 : m_users.size();
}

QVariant UserlistModel::data(const QModelIndex &index, int role) const{
	if (!index.isValid())
		return QVariant();

	switch (role){
		case Qt::DisplayRole: {
			return QString("%1 (%2)")
					.arg(m_users.at(index.row()).name())
					.arg(m_users.at(index.row()).levelName());
		}
		case Qt::BackgroundColorRole: {
			return QBrush(m_users.at(index.row()).color());
		}
		case Qt::ForegroundRole:{
			return QBrush(Qt::black);
		}
		case Qt::FontRole:{
			QFont font;
			font.setPointSize(16);
			font.setFamily("Ubuntu");
			if (m_users.at(index.row()).id() == m_client->clientUser().id()){
				font.setWeight(QFont::Bold);
			}
			return font;
		}
		default: return QVariant();
	}
}

bool UserlistModel::insertRows(int row, int count, const QModelIndex &parent){
	if (parent.isValid()){
		return false;
	}
	beginInsertRows(parent, row, row + count - 1);
	m_users.insert(row, count, Client::User());
	endInsertRows();
	return true;
}

bool UserlistModel::removeRows(int row, int count, const QModelIndex &parent){
	if (parent.isValid()){
		return false;
	}
	beginRemoveRows(parent, row, row + count - 1);
	m_users.remove(row, count);
	endRemoveRows();
	return true;
}

void UserlistModel::setUsers(const Client::User & user){
	beginResetModel();
	m_users.clear();
	m_users << user;
	endResetModel();
}

void UserlistModel::setUsers(const QHash<id_t, Client::User> & users){
	beginResetModel();
	m_users.clear();
	for (const Client::User & user : users){
		m_users << user;
	}
	endResetModel();
}

const Client::User &UserlistModel::userAt(const int & row){
	return m_users.at(row);
}

void UserlistModel::addUser(const Client::User & user){
	beginInsertRows(QModelIndex(), m_users.size(), m_users.size());
	m_users << user;
	endInsertRows();
}

void UserlistModel::removeUser(const Client::User & user){
	auto resultIterator {
		findUserById(user.id())
	};
	QSUPPOSE(resultIterator != m_users.end());
	if (resultIterator == m_users.end()){
		QLOG_ERROR("an attempt to remove user with unknown id was made");
		return;
	}
	long removingIndex { resultIterator - m_users.begin() };
	beginRemoveRows(QModelIndex(), removingIndex, removingIndex);
	m_users.remove(removingIndex);
	endRemoveRows();
}

void UserlistModel::handle_client_connectedToPad(const Client::Pad &,
												 const QHash<id_t, Client::User> & users,
												 const QString &){
	setUsers(users);
}

void UserlistModel::handle_client_currentLevelChanged(const Client::User & user,
													  const Client::User::Level & level){
	handle_client_someUserChangedLevel(user, m_client->clientUser(), level);
}

void UserlistModel::handle_client_createdNewPad(){
	setUsers(m_client->clientUser());
}

void UserlistModel::handle_client_changedColor(const QColor & color){
	if (!m_client->isConnectedToPad()){
		return;
	}
	auto resultIterator {
		findUserById(m_client->clientUser().id())
	};
	QSUPPOSE(resultIterator != m_users.end());
	resultIterator->setColor(QColor(color));
	QModelIndex userIndex{ index(resultIterator - m_users.begin()) };
	emit dataChanged(userIndex, userIndex, QVector<int>() << Qt::DisplayRole);
}

void UserlistModel::handle_client_disconnectedFromPad(){
	setUsers(QHash<id_t, Client::User>());
}

void UserlistModel::handle_client_updateUserChangedColor(const Client::User & user){
	auto resultIterator {
		findUserById(user.id())
	};
	QSUPPOSE(resultIterator != m_users.end());
	resultIterator->setColor(QColor(user.color()));
	QModelIndex userIndex{ index(resultIterator - m_users.begin()) };
	emit dataChanged(userIndex, userIndex, QVector<int>() << Qt::DisplayRole);
}

void UserlistModel::handle_client_someUserChangedLevel(const Client::User &,
													   const Client::User & targetUser,
													   const Client::User::Level & newLevel){
	auto targetIterator {
		findUserById(targetUser.id())
	};
	QSUPPOSE(targetIterator != m_users.end());
	targetIterator->setLevel(newLevel);
	QModelIndex userIndex{ index(targetIterator - m_users.begin()) };
	emit dataChanged(userIndex, userIndex, QVector<int>() << Qt::DisplayRole);
}

QVector<Client::User>::iterator UserlistModel::findUserById(const id_t & userId){
	return std::find_if(
		m_users.begin(),
		m_users.end(),
		[&userId](const Client::User & suspect){
			return suspect.id() == userId;
		}
	);
}

Qt::ItemFlags UserlistModel::flags(const QModelIndex & index) const{
	if (index.isValid()){
		return m_client->clientUser().canModifyUserRightsFor(m_users.at(index.row()))
					? (Qt::ItemIsEnabled | Qt::ItemIsSelectable)
					:  Qt::ItemIsEnabled;
	} else {
		return Qt::NoItemFlags;
	}
}












