#ifndef USERLISTMODEL_H
#define USERLISTMODEL_H

#include <QAbstractListModel>
#include <QVector>

#include "client_user.h"
#include "main_client.h"

class UserlistModel : public QAbstractListModel
{
	Q_OBJECT

public:
	explicit UserlistModel(
			MainClient * const & client,
			QObject *parent = nullptr
	);

	// Basic functionality:
	int rowCount(const QModelIndex &parent = QModelIndex()) const override;

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

	// Add data:
	bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

	// Remove data:
	bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

	void setUsers(const Client::User & user);
	void setUsers(const QHash<id_t, Client::User> & users);

	const Client::User & userAt(const int & row);

private slots:
	void addUser(const Client::User & user);
	void removeUser(const Client::User & user);

	void handle_client_connectedToPad(const Client::Pad & pad,
									  const QHash<id_t, Client::User> & users,
									  const QString & newDocHtml);



	void handle_client_currentLevelChanged(const Client::User &, const Client::User::Level &);

	void handle_client_createdNewPad();
	void handle_client_changedColor(const QColor & color);
	void handle_client_disconnectedFromPad();
	void handle_client_updateUserChangedColor(const Client::User & user);
	void handle_client_someUserChangedLevel(const Client::User &,
											const Client::User & targetUser,
											const Client::User::Level & newLevel);

	QVector<Client::User>::iterator findUserById(const id_t & userId);

private:
	QVector<Client::User> m_users;
	MainClient * m_client;

	// QAbstractItemModel interface
public:
	Qt::ItemFlags flags(const QModelIndex & index) const override;
};

#endif // USERLISTMODEL_H
