#-------------------------------------------------
#
# Project created by QtCreator 2018-05-09T20:22:37
#
#-------------------------------------------------

QT += core gui network sql xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ServerApp
TEMPLATE = app

CONFIG += c++2a
QMAKE_CXXFLAGS += -std=c++2a -Wall -Wextra -Wno-variadic-macros

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
        mainwindow.cpp \
        main_server.cpp


HEADERS += \
        mainwindow.h \
        main_server.h \
    ../SharedLib/tcpsocket.h \
    ../SharedLib/tcpserver.h \
    ../SharedLib/std_ext.h \
    ../SharedLib/range.h \
    ../SharedLib/qnet.h \
    ../SharedLib/qext.h \
    ../SharedLib/netpacket.h \
    ../SharedLib/message_exception.h \
    ../SharedLib/location.h \
    ../SharedLib/error.h \
    ../SharedLib/client_user.h \
    ../SharedLib/client_pad.h \
    ../SharedLib/data.h \
    ../SharedLib/server_user.h \
    ../SharedLib/server_pad.h  \
    ../stylesheet/stylesheet.h \
    ../SharedLib/dbmanager.h \
    ../SharedLib/revision.h


FORMS += \
        mainwindow.ui


INCLUDEPATH += ../stylesheet

unix:!macx: LIBS += -L$$PWD/../SharedLib/build/ -lSharedLib

INCLUDEPATH += $$PWD/../SharedLib
DEPENDPATH += $$PWD/../SharedLib/build
