#include <QMetaType>
#include <QSet>

#include "mainwindow.h"
#include <QApplication>
#include "main_server.h"
#include "stylesheet.h"
#include "client_pad.h"
#include "client_user.h"


Q_DECLARE_METATYPE(id_t)
Q_DECLARE_METATYPE(timeid_t)
Q_DECLARE_METATYPE(QSet<id_t>)
Q_DECLARE_METATYPE(Client::User::Level)

int main(int argc, char *argv[]){
	QApplication a(argc, argv);
	a.setApplicationDisplayName("CommonPad ServerApp");
	a.setStyleSheet(GLOBAL_STYLESHEET);
	MainWindow w;
	w.setWindowTitle("CommonPad server app");
	w.show();
	qRegisterMetaType<Client::Pad>();
	qRegisterMetaType<Client::User>();
	qRegisterMetaType<id_t>("id_t");
	qRegisterMetaType<timeid_t>("timeid_t");
	qRegisterMetaType<QTextDocumentFragment>("QTextDocumentFragment");
	qRegisterMetaType<Data::RevisionsRange>("Data::RevisionsRange");
	qRegisterMetaType<QSet<id_t>>("QSet<id_t>");
	qRegisterMetaType<Client::User::Level>("Client::User::Level");
	qRegisterMetaType<Data::UserAndAllPads>("Data::UserAndAllPads");
	qRegisterMetaType<Data::DocumentAndRevisionsRange>("Data::DocumentAndRevisionsRange");
	QLOG("Ideal thread count -> " << QThread::idealThreadCount());
	return a.exec();
}
