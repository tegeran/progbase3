﻿#include <QHostAddress>
#include <QSettings>
#include <QTcpSocket>
#include <QNetworkAccessManager>
#include <QNetworkConfiguration>
#include <QNetworkInterface>
#include <QSqlQuery>
#include <QSqlError>
#include <QCryptographicHash>

#include <algorithm>
#include <utility>

#include "main_server.h"
#include "error.h"
#include "std_ext.h"
#include "qext.h"
#include "client_pad.h"
#include "netpacket.h"
#include "qnet.h"
#include "data.h"
#include "server_user.h"

#define DB_CONNECTION_NAME "server_dbcon"
#define DB_REVISIONS_SAVE_CONNECTION_NAME "server_revisions_save_dbcon"
#define DB_REVISIONS_LOAD_CONNECTION_NAME "server_revisions_load_dbcon"
#define DB_FILE_PATH "database.sqlite3"

#define TRY_PROCESS_QUERY_FOR(USER_OR_PAD)({             \
	auto ____USER_OR_PAD((USER_OR_PAD));			     \
	____USER_OR_PAD->removeActiveQuery();		         \
	RETURN_IF(____USER_OR_PAD->isScheduledForDeletion()) \
})

#define TRY_PROCESS_QUERY_FOR_BOTH(USER, PAD)({  \
	auto ____USER((USER));                       \
	auto ____PAD((PAD));                         \
	____USER->removeActiveQuery();               \
	____PAD->removeActiveQuery();                \
	RETURN_IF(____USER->isScheduledForDeletion() \
			|| ____PAD->isScheduledForDeletion())\
})

using namespace Server;

MainServer::MainServer()
	:
	  m_generalDbThread(new QThread(this)),
	  m_revisionsSaverThread(new QThread(this)),
	  m_revisionsLoaderThread(new QThread(this))
	{
	Qext::Object::connectMultipleSignalsToSlots(
		&m_server,
		this,
		{
			SIGNAL(newConnection()),
			SLOT(handle_server_newConnection()),

			SIGNAL(acceptError(QAbstractSocket::SocketError)),
			SLOT(handle_server_acceptError(QAbstractSocket::SocketError))
		}
	);
	setupDbConnections();
}

MainServer::~MainServer() {
	close();
	m_generalDbThread->quit();
	m_generalDbThread->wait();
	m_revisionsSaverThread->quit();
	m_revisionsSaverThread->wait();
	m_revisionsLoaderThread->quit();
	m_revisionsLoaderThread->wait();
}

void MainServer::handle_dbManager_successUpdateUserLevel(
		User * const & senderUser,
		const id_t & padId,
		const id_t & targetId,
		const User::Level & newLevel)
{
	TRY_PROCESS_QUERY_FOR(senderUser);
	Server::User * targetUser { userOnlineFromId(targetId) };
	RETURN_IF(!targetUser);
	if (targetUser->isConnectedToPad()
	 && targetUser->currentPad()->id() == padId){
		targetUser->setLevel(newLevel, senderUser);
	} else {
		targetUser->sendNetpacket<Data::SenderIdAnd<Data::IdAndLevel>>(
			Netpacket::Update_ForeignChangeUserLevel,
			senderUser->id(),
			padId,
			newLevel
		);
	}
}

void MainServer::handle_dbManager_failureUpdateUserLevel(
	User * const & senderUser,
	const QString & reason)
{
	TRY_PROCESS_QUERY_FOR(senderUser);
	senderUser->sendErrorReport(QString("failed to update target user's level %1").arg(reason));
}

void MainServer::handle_dbManager_successOpenOldPad(
	User * const & senderUser,
	Client::Pad padData,
	const QString & documentHtml,
	const id_t & ownerId,
	QSet<id_t> admins,
	QSet<id_t> authors,
	const timeid_t & lastRevision)
{
	TRY_PROCESS_QUERY_FOR(senderUser);
	Server::Pad * const newPad{
		new Server::Pad(
			std::move(padData),
			ownerId,
			documentHtml,
			std::move(admins),
			std::move(authors)
		)
	};
	newPad->setLastRevisionTimeId(lastRevision);
	addNewPad(newPad);
	notifyAllOfNewPadExcept(senderUser, newPad);
	if (senderUser->isConnectedToPad()){
		senderUser->currentPad()->disconnectUserFromPad(senderUser);
	}
	newPad->connectUserToPad(senderUser);
}

void MainServer::handle_dbManager_failureOpenOldPad(User * const & senderUser,
													const QString & reason){
	TRY_PROCESS_QUERY_FOR(senderUser);
	senderUser->sendErrorReport(
		QString("failed to open required pad %1").arg(reason)
	);
}

void MainServer::handle_loadManager_successRestorePad(
	Pad * const & senderPad,
	User * const & senderUser,
	const QString & newDocumentHtml)
{
	TRY_PROCESS_QUERY_FOR_BOTH(senderUser, senderPad);
	if (senderUser->currentPad() == senderPad){
		senderPad->setRestoredDocument(
			senderUser,
			newDocumentHtml
		);
	}
}

void MainServer::handle_loadManager_failureRestorePad(
	Pad * const & senderPad,
	User * const & senderUser,
	const QString & reason)
{
	TRY_PROCESS_QUERY_FOR_BOTH(senderUser, senderPad);
	senderUser->sendErrorReport(
		QString("failed to restore pad %1").arg(reason)
	);
}

void MainServer::handle_loadManager_successFirstDocVersionRevisions(
		User * const & senderUser,
		const Data::DocumentAndRevisionsRange & data){
	TRY_PROCESS_QUERY_FOR(senderUser);
	senderUser->sendNetpacket<Data::DocumentAndRevisionsRange>(
		Netpacket::Answer_FirstDocRevision,
		data.documentHtml,
		data.revisions
	);
}

void MainServer::handle_loadManager_failureFirstDocVersionRevisions(
		User * const & senderUser,
		const QString & reason)
{
	TRY_PROCESS_QUERY_FOR(senderUser);
	senderUser->sendErrorReport(
		QString("failed to fetch first document version revisions %1")
			.arg(reason)
	);
}

void MainServer::handle_loadManager_successRevisionsRange(
	User * const & senderUser,
	const Data::RevisionsRange & revisions)
{
	TRY_PROCESS_QUERY_FOR(senderUser);
	senderUser->sendNetpacket<Data::RevisionsRange>(
		Netpacket::Answer_RevisionsRange,
		revisions.firstTimeId,
		revisions.lastTimeId,
		revisions.commands
				);
}

void MainServer::handle_loadManager_failureRevisionsRange(
	User * const & senderUser,
	const QString & reason)
{
	TRY_PROCESS_QUERY_FOR(senderUser);
	senderUser->sendErrorReport(
		QString("failed to query revisions %1").arg(reason)
	);
}

void MainServer::handle_loadManager_successAvailableRevisions(
		User * const & senderUser,
		const timeid_t & lastRevisionId)
{
	TRY_PROCESS_QUERY_FOR(senderUser);
	senderUser->sendNetpacket<Data::Integer<timeid_t>>(
		Netpacket::Answer_AvailableRevisions,
		lastRevisionId
	);
}

void MainServer::handle_dbManager_failureRegisterNewPad(User * const & senderUser,
														const QString & reason){
	TRY_PROCESS_QUERY_FOR(senderUser);
	senderUser->sendErrorReport(
		Netpacket::Answer_Error,
		QString("failed to register new pad %1").arg(reason)
	);
}

void MainServer::handle_dbManager_successRegisterNewPad(User * const & senderUser,
															 Client::Pad clientData,
															 const QString & documentHtml){
	TRY_PROCESS_QUERY_FOR(senderUser);
	if (senderUser->isConnectedToPad()){
		senderUser->currentPad()->disconnectUserFromPad(senderUser);
	}
	Pad * newPad {
		new Pad(
			std::move(clientData),
			senderUser,
			documentHtml
		)
	};
	newPad->setLastRevisionTimeId(0);
	addNewPad(newPad);
	notifyAllOfNewPadExcept(senderUser, newPad);
	senderUser->sendNetpacket<Data::ClientPad>(
		Netpacket::Answer_CreatedNewPad,
		*newPad
	);
}



void MainServer::open(const quint16 & port){
	if (!m_server.listen(QHostAddress::Any, port)){
		throw MessageException("failed to start listening");
	}
	QLOG("server opened");
}

void MainServer::close(){
	if (m_server.isListening()) {
		m_server.close();
		QLOG("server closed");
	}

	std_ext::deletePtrs(m_padsOnline);
	m_padsOnline.clear();
	deleteUsers(m_usersOnline);
	m_usersOnline.clear();
	deleteUsers(m_unAuthorizedUsers);
	m_unAuthorizedUsers.clear();
}

const TcpServer & MainServer::server() const{
	return m_server;
}

void MainServer::handle_user_socketError(QAbstractSocket::SocketError err){
	User * user = QSENDER(User *);
	QLOG("received error: " << err << " from user: " << *user);
}

void MainServer::handle_user_disconnected(){
	User * user { QSENDER(User *) };
	if (user->hasValidId())
		m_usersOnline.remove(user->id());
	else
		m_unAuthorizedUsers.remove(user);
	user->scheduleDeletion();
}

void MainServer::handle_server_newConnection(){
	QLOG("NEW CONNECTION");
	User * connectedUser{ m_server.nextPendingUser() };
	QSUPPOSE(
		connectedUser->isConnected(),
		"user in non-connected state was received"
	);
	connect(
		connectedUser,
		SIGNAL(receivedNetpacket(Netpacket)),
		this,
		SLOT(handle_user_receivedNetpacket(Netpacket))
	);
	connect(
		connectedUser,
		SIGNAL(error(QAbstractSocket::SocketError)),
		this,
		SLOT(handle_user_socketError(QAbstractSocket::SocketError))
	);
	connect(
		connectedUser,
		SIGNAL(disconnected()),
		this,
		SLOT(handle_user_disconnected())
	);
	m_unAuthorizedUsers.insert(connectedUser);
}


void MainServer::handle_server_acceptError(QAbstractSocket::SocketError err){
	QLOG_ERROR(
		"got server accept error: " << err
		<< " errorString(): " << m_server.errorString()
	);
}

void MainServer::handle_user_receivedNetpacket(const Netpacket & netpacket){
	User * senderUser { QSENDER(User *) };
	try {
		switch (netpacket.type()){
			case Netpacket::Request_Authorization:
			{ processRequest_Authorization(senderUser, netpacket);    return; }

			case Netpacket::Request_PadsOnline:
			{ prosessRequest_PadsOnline(senderUser, netpacket);       return; }

			case Netpacket::Request_Registration:
			{ processRequest_Registration(senderUser, netpacket);     return; }

			case Netpacket::Request_ChangeColor:
			{ processRequest_ChangeColor(senderUser, netpacket);      return; }

			case Netpacket::Request_CreateNewPad:
			{ processRequest_CreateNewPad(senderUser, netpacket);     return; }

			case Netpacket::Request_ConnectToPad:
			{ processRequest_ConnectToPad(senderUser, netpacket);     return; }

			case Netpacket::Request_ChangeUserLevel:
			{ processRequest_ChangeUserLevel(senderUser, netpacket);  return; }

			default: return;
		}
	} catch (const MessageException & exception){
		QLOG_ERROR("received corrupted packet: " << exception.what());
		senderUser->sendErrorReport(QString(exception.what()));
	}
}


User * MainServer::userOnlineFromId(const id_t & id) const {
	return std_ext::tryFindPointer(m_usersOnline, id);
}

Pad * MainServer::padOnlineFromId(const id_t & id) const {
	return std_ext::tryFindPointer(m_padsOnline, id);
}

void MainServer::prosessRequest_PadsOnline(User * const & senderUser,
										   const Netpacket &)
{
	RETURN_IF(!ensureUserIsAuthorized(senderUser));
	if (!senderUser->hasValidId()) {
		senderUser->sendErrorReport("user is not authorized");
		return;
	}
	senderUser->sendNetpacket<Data::ClientPads>(
		Netpacket::Answer_PadsOnline,
		m_padsOnline,
		senderUser
	);
}


void MainServer::processRequest_Authorization(User * const & senderUser,
											  const Netpacket & netpacket)
{
	RETURN_IF(!ensureUserIsNotAuthorized(senderUser));
	Data::Authorization authPacket {
		netpacket.dataPacket<Data::Authorization>()
	};
	senderUser->addActiveQuery();
	emit queryUserAuthData(senderUser, authPacket.login, authPacket.password);
}

void MainServer::processRequest_Registration(User * const & senderUser,
											 const Netpacket & netpacket)
{
	RETURN_IF(!ensureUserIsNotAuthorized(senderUser));
	Data::Registration registrPacket {
		netpacket.dataPacket<Data::Registration>()
	};

	if (!isAcceptableRegistrationData(
			registrPacket.auth.login,
			registrPacket.auth.password,
			registrPacket.name)
	){
		senderUser->sendErrorReport(
			Netpacket::Answer_RegistrationError,
			"unacceptable registration data"
		);
		return;
	}
	senderUser->addActiveQuery();
	emit queryRegisterNewUser(
		senderUser,
		registrPacket.auth.login,
		registrPacket.auth.password,
		registrPacket.name,
		Client::User::DEFAULT_COLOR
	);
}

void MainServer::handle_dbManager_successUserAuthData(Server::User * const & senderUser,
													  const Data::UserAndAllPads & authData)
{
	TRY_PROCESS_QUERY_FOR(senderUser);
	QSUPPOSE(authData.user.hasValidId());

	if (m_usersOnline.contains(authData.user.id())) {
		senderUser->sendErrorReport(
			Netpacket::Answer_AuthorizationError,
			"requested user is already online"
		);
		return;
	}
	senderUser->setClientData(Client::User(authData.user));
	moveUserToOnline(senderUser);
	senderUser->sendNetpacket<Data::UserAndAllPads>(
		Netpacket::Answer_Authorization,
		senderUser,
		m_padsOnline,
		authData.ownerPads,
		authData.adminPads,
		authData.authorPads
	);
}

void MainServer::handle_dbManager_failureUserAuthData(Server::User * const & senderUser,
													  const QString & reason){
	TRY_PROCESS_QUERY_FOR(senderUser);
	senderUser->sendErrorReport(
		Netpacket::Answer_AuthorizationError,
		reason
	);
}

void MainServer::handle_dbManager_failureRegisterNewUser(User * const & senderUser, const QString & reason){
	TRY_PROCESS_QUERY_FOR(senderUser);
	senderUser->sendErrorReport(
		Netpacket::Answer_RegistrationError,
		QString("failed to register new user %1").arg(reason)
	);
}

void MainServer::handle_dbManager_successRegisterNewUser(User * const & senderUser,
														 Client::User clientData){
	TRY_PROCESS_QUERY_FOR(senderUser);
	senderUser->setClientData(std::move(clientData));
	moveUserToOnline(senderUser);
	senderUser->sendNetpacket<Data::UserAndAllPads>(
		Netpacket::Answer_Registration,
		senderUser,
		m_padsOnline,
		QVector<Client::Pad>(),
		QVector<Client::Pad>(),
		QVector<Client::Pad>()
	);
}

void MainServer::handle_dbManager_successUpdateUserColor(User * const & senderUser,
															  QColor newColor){
	TRY_PROCESS_QUERY_FOR(senderUser);
	senderUser->setColor(std::move(newColor));
	senderUser->sendNetpacket<Data::Color>(
		Netpacket::Answer_ChangedColor,
		senderUser->color()
	);
}

void MainServer::handle_dbManager_failureUpdateUserColor(User * const & senderUser, const QString & reason){
	TRY_PROCESS_QUERY_FOR(senderUser);
	senderUser->sendErrorReport(
		Netpacket::Answer_Error,
		QString("failed to set new color %1").arg(reason)
	);
}


void MainServer::processRequest_ChangeColor(User * const & user,
											const Netpacket & netpacket){
	RETURN_IF(!ensureUserIsAuthorized(user));
	Data::Color colorPacket {
		netpacket.dataPacket<Data::Color>()
	};
	user->addActiveQuery();
	emit queryUpdateUserColor(user, user->id(), colorPacket.color);
}

void MainServer::processRequest_CreateNewPad(User * const & user, const Netpacket & netpacket){
	RETURN_IF(!ensureUserIsAuthorized(user));

	Data::NewPadData dataPacket {
		netpacket.dataPacket<Data::NewPadData>()
	};
	user->addActiveQuery();
	emit queryRegisterNewPad(
		user,
		user->id(),
		dataPacket.name,
		dataPacket.isPublic,
		dataPacket.textDocumentHtml
	);
}

void MainServer::processRequest_ConnectToPad(User * const & user, const Netpacket & netpacket){
	RETURN_IF(!ensureUserIsAuthorized(user));
	Data::Id dataPacket {
		netpacket.dataPacket<Data::Id>().id
	};
	Pad * pad { padOnlineFromId(dataPacket.id) };
	if (pad){
		if (user->isConnectedToPad()){
			if (pad->id() != user->currentPad()->id()){
				user->currentPad()->disconnectUserFromPad(user);
			} else {
				user->sendErrorReport("user is already connected to required pad");
				return;
			}
		}
		pad->connectUserToPad(user);
	}  else {
		user->addActiveQuery();
		emit queryOpenOldPad(user, user->id(), dataPacket.id);
	}
}

void MainServer::processRequest_ChangeUserLevel(User * const & user, const Netpacket & netpacket){
	RETURN_IF(!ensureUserIsAuthorized(user));
	if (!user->isConnectedToPad()){
		user->sendErrorReport("user must be connected to pad to modify its members levels");
		return;
	}
	Data::IdAndLevel dataPacket {
		netpacket.dataPacket<Data::IdAndLevel>()
	};
	Client::User::Level targetPrevLevel {
		user->currentPad()->userLevelFromId(dataPacket.id)
	};

	if (!user->canPromoteUserFromTo(targetPrevLevel, dataPacket.level)){
		user->sendErrorReport("failed to change user's level");
	}
	user->addActiveQuery();
	emit queryUpdateUserLevel(user, user->currentPad()->id(), dataPacket.id, dataPacket.level);
}

void MainServer::handle_pad_lastUserLeft(){
	Pad * senderPad { QSENDER(Server::Pad *) };
	bool check { m_padsOnline.remove(senderPad->id()) };
	QSUPPOSE(check);
	sendToAllUsersOnline<Data::Id>(
		Netpacket::Update_RemovedPadOnline,
		senderPad->id()
	);
	senderPad->scheduleDeletion();
}

bool MainServer::isAcceptableRegistrationData(const QString & login,
											  const QString & password,
											  const QString & name) const
{
	return User::isLogin(login) && User::isPassword(password) && User::isName(name);
}

void MainServer::addNewPad(Pad * const & pad){
	QSUPPOSE(!m_padsOnline.contains(pad->id()));
	connect(
		pad,
		SIGNAL(lastUserLeft()),
		this,
		SLOT(handle_pad_lastUserLeft())
	);
	m_padsOnline.insert(pad->id(), pad);
}

void MainServer::notifyAllOfNewPadExcept(User * const & exception, Pad * const & newPad){
	if (newPad->isPublic()){
		sendToAllUsersOnlineExcept<Data::ClientPad>(
			exception,
			Netpacket::Update_AddedPadOnline,
			*newPad
		);
	} else for (Server::User * const & user : m_usersOnline){
		if (user != exception && user->hasAccessToPad(newPad)){
			user->sendNetpacket<Data::ClientPad>(
				Netpacket::Update_AddedPadOnline,
				*newPad
			);
		}
	}
}



void MainServer::moveUserToOnline(User * const & user){
	QSUPPOSE(user->hasValidId());
	bool userIsUnAuthorized { m_unAuthorizedUsers.remove(user) };
	QSUPPOSE(userIsUnAuthorized);
	QSUPPOSE(!m_usersOnline.contains(user->id()));
	m_usersOnline.insert(user->id(), user);
}

bool MainServer::ensureUserIsAuthorized(User * user){
	QSUPPOSE(user);
	if (!user->hasValidId()){
		user->sendErrorReport("insufficient operation for unauthorized user");
		return false;
	}
	return true;
}

bool MainServer::ensureUserIsNotAuthorized(User * user){
	QSUPPOSE(user);
	if (user->hasValidId()){
		user->sendErrorReport("insufficient operation for already authorized user");
		return false;
	}
	return true;
}



void MainServer::setupDbConnections(){
	launchRevisionsLoaderDb();
	launchGeneralDb();
	launchRevisionsSaverDb();
}

void MainServer::launchGeneralDb(){
	DbManager * dbManager {
		new DbManager(
			DB_CONNECTION_NAME,
			DB_FILE_PATH,
			&m_globalMutex
		)
	};
	dbManager->moveToThread(m_generalDbThread);
	Qext::Object::connectMultipleSignalsToSlots(
		m_generalDbThread,
		dbManager,
		{
			SIGNAL(started()),
			SLOT(initialize()),

			SIGNAL(finished()),
			SLOT(deleteLater())
		}
	);
	Qext::Thread::deleteLaterOnFinished(m_generalDbThread);
	Qext::Object::connectMultipleSignalsToSlots(
		this,
		dbManager,
		{
			SIGNAL(queryUpdateUserLevel(Server::User*,id_t,id_t,Client::User::Level)),
			SLOT  (queryUpdateUserLevel(Server::User*,id_t,id_t,Client::User::Level)),

			SIGNAL(queryOpenOldPad(Server::User*,id_t,id_t)),
			SLOT  (queryOpenOldPad(Server::User*,id_t,id_t)),

			SIGNAL(queryRegisterNewPad(Server::User*,id_t,QString,bool,QString)),
			SLOT  (queryRegisterNewPad(Server::User*,id_t,QString,bool,QString)),

			SIGNAL(queryUserAuthData(Server::User*,QString,QString)),
			SLOT  (queryUserAuthData(Server::User*,QString,QString)),

			SIGNAL(queryRegisterNewUser(Server::User*,QString,QString,QString,QColor)),
			SLOT  (queryRegisterNewUser(Server::User*,QString,QString,QString,QColor)),

			SIGNAL(queryUpdateUserColor(Server::User*,id_t,QColor)),
			SLOT  (queryUpdateUserColor(Server::User*,id_t,QColor))
		}
	);

	Qext::Object::connectMultipleSignalsToSlots(
		dbManager,
		this,
		{

			SIGNAL				 (successUpdateUserLevel(Server::User*,id_t,id_t,Client::User::Level)),
			SLOT(handle_dbManager_successUpdateUserLevel(Server::User*,id_t,id_t,Client::User::Level)),

			SIGNAL               (failureUpdateUserLevel(Server::User*,QString)),
			SLOT(handle_dbManager_failureUpdateUserLevel(Server::User*,QString)),

			SIGNAL				 (successOpenOldPad(Server::User*,Client::Pad,QString,id_t,QSet<id_t>,QSet<id_t>,timeid_t)),
			SLOT(handle_dbManager_successOpenOldPad(Server::User*,Client::Pad,QString,id_t,QSet<id_t>,QSet<id_t>,timeid_t)),

			SIGNAL               (failureOpenOldPad(Server::User*,QString)),
			SLOT(handle_dbManager_failureOpenOldPad(Server::User*,QString)),

			SIGNAL               (successUserAuthData(Server::User*,Data::UserAndAllPads)),
			SLOT(handle_dbManager_successUserAuthData(Server::User*,Data::UserAndAllPads)),

			SIGNAL				 (failureUserAuthData(Server::User*,QString)),
			SLOT(handle_dbManager_failureUserAuthData(Server::User*,QString)),

			SIGNAL				 (successRegisterNewPad(Server::User*,Client::Pad,QString)),
			SLOT(handle_dbManager_successRegisterNewPad(Server::User*,Client::Pad,QString)),

			SIGNAL				 (failureRegisterNewPad(Server::User*,QString)),
			SLOT(handle_dbManager_failureRegisterNewPad(Server::User*,QString)),

			SIGNAL				 (successUpdateUserColor(Server::User*,QColor)),
			SLOT(handle_dbManager_successUpdateUserColor(Server::User*,QColor)),

			SIGNAL				 (failureUpdateUserColor(Server::User*,QString)),
			SLOT(handle_dbManager_failureUpdateUserColor(Server::User*,QString)),

			SIGNAL				 (successRegisterNewUser(Server::User*,Client::User)),
			SLOT(handle_dbManager_successRegisterNewUser(Server::User*,Client::User)),

			SIGNAL               (failureRegisterNewUser(Server::User*,QString)),
			SLOT(handle_dbManager_failureRegisterNewUser(Server::User*,QString))
		}
	);
	m_generalDbThread->start();
}

void MainServer::launchRevisionsLoaderDb(){
	DbManager * revisionsLoadManager {
		new DbManager(
			DB_REVISIONS_LOAD_CONNECTION_NAME,
			DB_FILE_PATH,
			&m_globalMutex
		)
	};
	revisionsLoadManager->moveToThread(m_revisionsLoaderThread);
	Qext::Object::connectMultipleSignalsToSlots(
		m_revisionsLoaderThread,
		revisionsLoadManager,
		{
			SIGNAL(started()),
			SLOT(initialize()),

			SIGNAL(finished()),
			SLOT(deleteLater())
		}
	);
	Qext::Thread::deleteLaterOnFinished(m_revisionsLoaderThread);
	Pad::setRevisionsLoadManager(revisionsLoadManager);
	Qext::Object::connectMultipleSignalsToSlots(
		revisionsLoadManager,
		this,
		{
			SIGNAL                 (successRestorePad(Server::Pad*,Server::User*,QString)),
			SLOT(handle_loadManager_successRestorePad(Server::Pad*,Server::User*,QString)),

			SIGNAL                 (failureRestorePad(Server::Pad*,Server::User*,QString)),
			SLOT(handle_loadManager_failureRestorePad(Server::Pad*,Server::User*,QString)),

			SIGNAL                 (successFirstDocVersionRevisions(Server::User*,Data::DocumentAndRevisionsRange)),
			SLOT(handle_loadManager_successFirstDocVersionRevisions(Server::User*,Data::DocumentAndRevisionsRange)),

			SIGNAL                 (failureFirstDocVersionRevisions(Server::User*,QString)),
			SLOT(handle_loadManager_failureFirstDocVersionRevisions(Server::User*,QString)),

			SIGNAL                 (successRevisionsRange(Server::User*,Data::RevisionsRange)),
			SLOT(handle_loadManager_successRevisionsRange(Server::User*,Data::RevisionsRange)),

			SIGNAL                 (failureRevisionsRange(Server::User*,QString)),
			SLOT(handle_loadManager_failureRevisionsRange(Server::User*,QString)),

			SIGNAL                 (successAvailableRevisions(Server::User*,timeid_t)),
			SLOT(handle_loadManager_successAvailableRevisions(Server::User*,timeid_t))
		}
	);



	m_revisionsLoaderThread->start();
}

void MainServer::launchRevisionsSaverDb(){
	DbManager * revisionsSaveManager {
		new DbManager(
			DB_REVISIONS_SAVE_CONNECTION_NAME ,
			DB_FILE_PATH,
			&m_globalMutex
		)
	};
	revisionsSaveManager->moveToThread(m_revisionsSaverThread);
	Qext::Object::connectMultipleSignalsToSlots(
		m_revisionsSaverThread,
		revisionsSaveManager,
		{
			SIGNAL(started()),
			SLOT(initialize()),

			SIGNAL(finished()),
			SLOT(deleteLater())
		}
	);
	Qext::Thread::deleteLaterOnFinished(m_revisionsSaverThread);
	Pad::setRevisionsSaveManager(revisionsSaveManager);

	m_revisionsSaverThread->start();
}

