#ifndef SERVER_H
#define SERVER_H
#include <QMap>
#include <QTcpServer>
#include <QTcpSocket>
#include <QSqlDatabase>
#include <QThread>
#include <QMutex>

#include "message_exception.h"
#include "server_user.h"
#include "std_ext.h"
#include "server_pad.h"
#include "netpacket.h"
#include "tcpserver.h"
#include "tcpsocket.h"
#include "dbmanager.h"

class MainServer : public QObject{
	Q_OBJECT
	DECL_UNCOPIABLE(MainServer)
public:

	static constexpr const char * ORGANIZATION = "VitahaDev";
	static constexpr const char * APPLICATION  = "CursachOp2";

	MainServer();

	static constexpr quint16 SERVER_PORT = 2000;

	void open(const quint16 & port = SERVER_PORT); /* throws MessageException */
	void close();

	const TcpServer & server() const;

	~MainServer();
signals:
	void queryUpdateUserLevel(Server::User * senderUser,
							  const id_t & padId,
							  const id_t & targetId,
							  const Client::User::Level & newLevel);

	void queryUserAuthData(Server::User * senderUser,
							  const QString & login,
							  const QString & password);

	void queryRegisterNewUser(Server::User * const & senderUser,
							  const QString & login,
							  const QString & password,
							  const QString & name,
							  const QColor & color);

	void queryUpdateUserColor(Server::User * const & senderUser,
							   const id_t & userId,
							   const QColor & newColor);

	void queryRegisterNewPad(Server::User * const & senderUser,
							 const id_t & onwerId,
							 const QString & name,
							 const bool & isPublic,
							 const QString & initialDocumentHtml);

	void queryOpenOldPad(Server::User * const & senderUser,
						 const id_t & userId,
						 const id_t & padId);

private slots:	

	void handle_dbManager_successUpdateUserLevel(
		Server::User * const & senderUser,
		const id_t & padId,
		const id_t & targetId,
		const Client::User::Level & newLevel
	);

	void handle_dbManager_failureUpdateUserLevel(
		Server::User * const & senderUser,
		const QString & reason
	);

	void handle_dbManager_successOpenOldPad(
		Server::User * const & senderUser,
		Client::Pad padData,
		const QString & documentHtml,
		const id_t & ownerId,
		QSet<id_t> admins,
		QSet<id_t> authors,
		const timeid_t & lastRevision);

	void handle_dbManager_failureOpenOldPad(
		Server::User * const & senderUser,
		const QString & reason
	);


	void handle_loadManager_successRestorePad(
		Server::Pad * const & senderPad,
		Server::User * const & senderUser,
		const QString & newDocumentHtml
	);

	void handle_loadManager_failureRestorePad(
		Server::Pad * const & senderPad,
		Server::User * const & senderUser,
		const QString & reason
	);


	void handle_loadManager_successFirstDocVersionRevisions(
		Server::User * const & senderUser,
		const Data::DocumentAndRevisionsRange & data
	);

	void handle_loadManager_failureFirstDocVersionRevisions(
		Server::User * const & senderUser,
		const QString & reason
	);

	void handle_loadManager_successRevisionsRange(
		Server::User * const & senderUser,
		const Data::RevisionsRange & revisions
	);

	void handle_loadManager_failureRevisionsRange(
		Server::User * const & senderUser,
		const QString & reason
	);

	void handle_loadManager_successAvailableRevisions(
		Server::User * const & senderUser,
		const timeid_t & lastRevisionId
	);


	void handle_dbManager_failureRegisterNewPad(
		Server::User * const & senderUser,
		const QString & reason
	);

	void handle_dbManager_successRegisterNewPad(
		Server::User * const & senderUser,
		Client::Pad clientData,
		const QString & documentHtml
	);


	void handle_dbManager_successUserAuthData(
		Server::User * const & senderUser,
		const Data::UserAndAllPads & authData
	);

	void handle_dbManager_failureUserAuthData(
		Server::User * const & senderUser,
		const QString & reason
	);

	void handle_dbManager_failureRegisterNewUser(
		Server::User * const & senderUser,
		const QString & reason
	);

	void handle_dbManager_successRegisterNewUser(
		Server::User * const & senderUser,
		Client::User clientData
	);

	void handle_dbManager_successUpdateUserColor(
		Server::User * const & senderUser,
		QColor newColor
	);

	void handle_dbManager_failureUpdateUserColor(
		Server::User * const & senderUser,
		const QString & reason
	);

	void handle_server_newConnection();
	void handle_server_acceptError(QAbstractSocket::SocketError err);
	void handle_user_receivedNetpacket(const Netpacket & netpacket);
	void handle_user_socketError(QAbstractSocket::SocketError err);
	void handle_user_disconnected();

	void handle_pad_lastUserLeft();
private:
	TcpServer m_server;
	QHash<id_t, Server::User *> m_usersOnline;
	QHash<id_t, Server::Pad *> m_padsOnline;
	QSet<Server::User *> m_unAuthorizedUsers;
	QThread * m_generalDbThread;
	QThread * m_revisionsSaverThread;
	QThread * m_revisionsLoaderThread;
	QMutex m_globalMutex;

	void setupDbConnections();
	void launchGeneralDb();
	void launchRevisionsLoaderDb();
	void launchRevisionsSaverDb();

	Server::User * userOnlineFromId(const id_t & id) const;
	Server::Pad  * padOnlineFromId (const id_t & id) const;

	void prosessRequest_PadsOnline(Server::User * const & user,
								   const Netpacket & netpacket);
	void processRequest_Authorization(Server::User * const & user,
									  const Netpacket & netpacket);
	void processRequest_Registration(Server::User * const & user,
									 const Netpacket & netpacket);

	void processRequest_ChangeColor(Server::User * const & user,
									const Netpacket & netpacket);

	void processRequest_CreateNewPad(Server::User * const & user,
									const Netpacket & netpacket);

	void processRequest_ConnectToPad(Server::User * const & user,
									 const Netpacket & netpacket);

	void processRequest_ChangeUserLevel(Server::User * const & user,
										const Netpacket & netpacket);


	bool isAcceptableRegistrationData(const QString & login,
								 const QString & password,
								 const QString & name) const;


	void addNewPad(Server::Pad * const & pad);
	void notifyAllOfNewPadExcept(Server::User * const & exception, Server::Pad * const & newPad);

	void moveUserToOnline(Server::User * const & user);

	bool ensureUserIsAuthorized(Server::User * user);
	bool ensureUserIsNotAuthorized(Server::User * user);


	template <typename TDataPacket, typename... TPacketArgs>
	void sendToAllUsersOnline(const Netpacket::Type & type,
							  TPacketArgs &&... args);

	template <typename TDataPacket, typename... TPacketArgs>
	void sendToAllUsersOnlineExcept(Server::User * const & exception,
									const Netpacket::Type & type,
									TPacketArgs &&... args);

	template<typename TUserPtrContainer>
	void deleteUsers(TUserPtrContainer & users);


};




template <typename TDataPacket, typename... TPacketArgs>
void MainServer::sendToAllUsersOnline(const Netpacket::Type & type,
										 TPacketArgs &&... args){
	Netpacket netpacket {
		type,
		TDataPacket::toJsonValue(std::forward<TPacketArgs>(args)...)
	};
	for(Server::User * const & user : m_usersOnline){
		user->sendNetpacket(netpacket);
	}
}

template <typename TDataPacket, typename... TPacketArgs>
void MainServer::sendToAllUsersOnlineExcept(Server::User * const & exception,
												const Netpacket::Type & type,
												TPacketArgs &&... args)
{
	Netpacket netpacket {
		type,
		TDataPacket::toJsonValue(std::forward<TPacketArgs>(args)...)
	};
	for(Server::User * const & user : m_usersOnline){
		if (user != exception)
			user->sendNetpacket(netpacket);
	}
}


template<typename TUserPtrContainer>
void MainServer::deleteUsers(TUserPtrContainer & users){
	for (Server::User * const & user : users){
		disconnect(
			user,
			SIGNAL(disconnected()),
			this,
			SLOT(handle_user_disconnected())
		);
		delete user;
	}
}
























#endif // SERVER_H
