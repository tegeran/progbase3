#include <QNetworkInterface>
#include <QHostAddress>
#include <QMessageBox>
#include <QMessageBox>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "message_exception.h"
#include "qnet.h"
#include "qext.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	if (!tryOpenServer())
		enableOpenOption();
	else
		disableOpenOption();

	Qnet::LocalAddresses localAddress;
	ui->lbl_hostIPv4->setText(localAddress.ipv4.toString());
	ui->lbl_hostIPv6->setText(localAddress.ipv6.toString());

}

MainWindow::~MainWindow(){
	delete ui;
	m_server.close();
}

void MainWindow::on_pb_open_clicked(){
	disableOpenOption();
	QSUPPOSE(!m_server.server().isListening());
	if (!tryOpenServer()){
		enableOpenOption();
	}
}

bool MainWindow::tryOpenServer(){
	if (m_server.server().isListening()){
		QLOG_ERROR(
			"tried to open server while "
			"it was already in listening state"
		);
		return true;
	}
	try { m_server.open(); }
	catch (const MessageException & exception){
		QMessageBox::critical(this, "SIGSEGV", "SIGSEGV: failed to open server");
//		Qext::Meta::scheduleRoutine(
//			this,
//			&QWidget::close
//		);
//		ui->lbl_serverPort->clear();
		return false;
	}
	ui->lbl_serverPort->setText(QString::number(m_server.server().serverPort()));
	return true;
}

void MainWindow::enableOpenOption(){
	ui->pb_open->setEnabled(true);
	ui->pb_close->setEnabled(false);
}

void MainWindow::disableOpenOption(){
	ui->pb_open->setEnabled(false);
	ui->pb_close->setEnabled(true);
}

void MainWindow::on_pb_close_clicked(){
	enableOpenOption();
	m_server.close();
}
