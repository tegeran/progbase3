#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "main_server.h"

namespace Ui {
	class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();
private slots:

	void on_pb_open_clicked();

	void on_pb_close_clicked();

private:
	Ui::MainWindow *ui;
	MainServer m_server;

	bool tryOpenServer();
	void enableOpenOption();
	void disableOpenOption();
};

#endif // MAINWINDOW_H
