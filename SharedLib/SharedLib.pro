#-------------------------------------------------
#
# Project created by QtCreator 2018-05-29T17:33:09
#
#-------------------------------------------------

QT       += widgets network sql xml

TARGET = SharedLib
TEMPLATE = lib

DEFINES += SHAREDLIB_LIBRARY
CONFIG += c++2a
QMAKE_CXXFLAGS += -std=c++2a -Wall -Wextra -Wno-variadic-macros

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    client_pad.cpp \
    client_user.cpp \
    data.cpp \
    error.cpp \
    message_exception.cpp \
    netpacket.cpp \
    qext.cpp \
    qnet.cpp \
    server_pad.cpp \
    server_user.cpp \
    std_ext.cpp \
    tcpserver.cpp \
    tcpsocket.cpp \
    timer.cpp \
    dbmanager.cpp \
    revision.cpp

HEADERS += \
        sharedlib_global.h \ 
    client_pad.h \
    client_user.h \
    data.h \
    error.h \
    location.h \
    message_exception.h \
    netpacket.h \
    qext.h \
    qnet.h \
    range.h \
    server_pad.h \
    server_user.h \
    std_ext.h \
    tcpserver.h \
    tcpsocket.h \
    timer.h \
    dbmanager.h \
    revision.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

FORMS +=
