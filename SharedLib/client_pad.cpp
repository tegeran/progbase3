#include <utility>

#include "client_pad.h"
#include "qext.h"
#include "std_ext.h"

namespace Client {

Pad::Pad(QString && name, const id_t & id, const bool & isPublic)
	: m_name(std::move(name)), m_id(id), m_isPublic(isPublic) {}


QString & Pad::name()			 { return m_name;      }
const QString &Pad::name() const { return m_name;      }
id_t Pad::id() const			 { return m_id;        }
bool Pad::isPublic() const       { return  m_isPublic; }

void Pad::setName(QString && name)        { m_name     = std::move(name); }


void Pad::setId(const id_t & id)          { m_id       = id;              }
void Pad::setPublic(const bool & boolean) { m_isPublic = boolean;		  }

bool Pad::isValid(){
	return m_id != nullid;
}

QJsonValue Pad::toJsonValue() const{
	return QJsonValue(
		QJsonObject({
			Qext::Json::keyValue(NAME_FIELDNAME, m_name),
			Qext::Json::keyValue(ID_FIELDNAME, m_id),
			Qext::Json::keyValue(ISPUBLIC_FIELDNAME, m_isPublic)
		})
	);
}

Pad Pad::nullobj(){
	return Pad("no padname", nullid);
}

Pad Pad::fromJsonValue(const QJsonValue & jsonValue){
	QJsonObject obj { Qext::Json::tryReadFromJsonValue<QJsonObject>(jsonValue) };
	return Pad(
		Qext::Json::tryReadFromJsonValue<QString>(obj[NAME_FIELDNAME]),
		Qext::Json::tryReadFromJsonValue<id_t>(obj[ID_FIELDNAME]),
		Qext::Json::tryReadFromJsonValue<bool>(obj[ISPUBLIC_FIELDNAME])
	);
}

QDebug &operator<<(QDebug & debugStream, const Pad & self){
	return debugStream << "padname -> " << self.m_name;
}


}
