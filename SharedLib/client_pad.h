#ifndef PAD_H
#define PAD_H

#include <QString>
#include <QJsonValue>
#include <QSet>
#include <QMetaClassInfo>

#include "sharedlib_global.h"
#include "message_exception.h"
#include "std_ext.h"
#include "error.h"



namespace Client SHAREDLIBSHARED_EXPORT {

class Pad {
public:
	static constexpr const char * const NAME_REG_EXP = R"~~(^(?:\w|\d){3,128}$)~~";
	DECL_DEFAULT_COPY_AND_MOVE(Pad)
	explicit Pad(QString && name = "no padname",
				 const id_t & id = nullid,
				 const bool & isPublic = true);

	const QString & name() const;
	void setName(QString && name);
	QString & name();

	id_t id() const;
	void setId(const id_t & id);

	bool isPublic() const;
	void setPublic(const bool & boolean);


	bool isValid();

	QJsonValue toJsonValue() const;

	static Pad nullobj();

	/* throws MessageException */
	static Pad fromJsonValue(const QJsonValue & jsonValue);

	friend QDebug & operator<<(QDebug & debugStream, const Pad & self);

protected:
	static constexpr const char * const NAME_FIELDNAME = "name";
	static constexpr const char * const ID_FIELDNAME = "id";
	static constexpr const char * const ISPUBLIC_FIELDNAME = "isPublic";

	QString m_name;
	id_t m_id;
	bool m_isPublic;

};

}

Q_DECLARE_METATYPE(Client::Pad)

#endif // PAD_H
