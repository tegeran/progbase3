#include <QJsonObject>

#include <utility>

#include "client_user.h"
#include "qext.h"
#include "std_ext.h"

namespace Client{

const QColor User::DEFAULT_COLOR { QColor::fromRgb(41, 87, 146) };

const Range<> User::PASSWORD_RANGE = Range<>(4, 36);
const Range<> User::LOGIN_RANGE	   = Range<>(3, 32);
const Range<> User::NAME_RANGE	   = Range<>(3, 32);


const QColor & User::finalColor(){
	static QColor color(85, 87, 83);
	return color;
}

User::User(QString && name,
		   const id_t & id,
		   QColor && color,
		   const User::Level & level)
	: m_name(std::move(name)),
	  m_color(std::move(color)),
	  m_id(id),
	  m_level(level) {}

const QString &User::name() const { return m_name;  }
const QColor &User::color() const { return m_color; }
id_t User::id() const			  { return m_id;    }
User::Level User::level() const   { return m_level; }

void User::setId(const id_t &id)		      { m_id = id;				    }
void User::setLevel(const User::Level & level){	m_level = level;            }
void User::setName(QString && name)		      { m_name = std::move(name);   }
void User::setColor(QColor && color)	      { m_color = std::move(color); }

QTextBlockFormat User::textBlockFormat() const {
	QTextBlockFormat fmt;
	fmt.setBackground(QBrush(m_color));
	fmt.setForeground(QBrush(Qt::black));
	return fmt;
}

QTextCharFormat User::textCharFormat() const{
	QTextCharFormat fmt;
	fmt.setBackground(QBrush(m_color));
	fmt.setForeground(QBrush(Qt::black));
	return fmt;
}

User User::nullobj(){ return User("", 0, Qt::red); }

bool User::isName(const QString & suspect){
	return QRegularExpression(NAME_REG_EXP).match(suspect).hasMatch();
}

bool User::isLogin(const QString & suspect){
	return QRegularExpression(LOGIN_REG_EXP).match(suspect).hasMatch();
}

bool User::isPassword(const QString & suspect){
	return QRegularExpression(PASSWORD_REG_EXP).match(suspect).hasMatch();
}

QDataStream &operator>>(QDataStream & stream, User & self){
	return stream >> self.m_name >> self.m_color >> self.m_id;
}
QDataStream &operator<<(QDataStream & stream, const User & self){
	return stream << self.m_name << self.m_color << self.m_id;
}

bool User::hasValidId() const{
	return m_id;
}

bool User::operator==(const User & other){
	return m_id == other.m_id;
}

QJsonValue User::toJsonValue() const{
	return QJsonObject({
		Qext::Json::keyValue(NAME_FIELDNAME, m_name),
		Qext::Json::keyValue(ID_FIELDNAME, m_id),
		Qext::Json::keyValue(LEVEL_FIELDNAME, static_cast<int>(m_level)),
		Qext::Json::keyValue(
			COLOR_FIELDNAME,
			Qext::Color::toJsonValue(m_color)
		)
	});
}

User User::fromJsonValue(const QJsonValue & value){
	QJsonObject obj { Qext::Json::tryReadFromJsonValue<QJsonObject>(value) };
	return User(
		Qext::Json::tryReadFromJsonValue<QString>(obj[NAME_FIELDNAME]),
		Qext::Json::tryReadFromJsonValue<id_t>(obj[ID_FIELDNAME]),
		Qext::Color::fromJsonValue(obj[COLOR_FIELDNAME]),
		Qext::Json::tryReadMinMaxEnum<Level>(obj[LEVEL_FIELDNAME])
	);
}

QDebug &operator<<(QDebug & debugStream, const User & self){
	return debugStream << "username -> " << self.m_name;
}


bool User::isOwner() const{
	return m_level == Owner;
}

bool User::isAdmin() const{
	return m_level == Admin;
}

bool User::isAuthor() const{
	return m_level == Author;
}

bool User::isGuest() const{
	return m_level == Guest;
}

void User::promoteToOwner(){
	m_level = Owner;
}

void User::promoteToAdmin(){
	m_level = Admin;
}

void User::promoteToAuthor(){
	m_level = Author;
}

void User::promoteToGuest(){
	m_level = Guest;
}


bool User::hasAdminRights() const{
	return isAdmin() || isOwner();
}

bool User::hasAuthorRights() const{
	return isAuthor() || hasAdminRights();
}

bool User::canModifyFinalText() const{
	return hasAdminRights();
}

bool User::canModifyUserRightsFor(const User & user) const{
	return user.id() != m_id
			&&
			(this->isOwner()
			 || (isAdmin() && !user.hasAdminRights()));
}

bool User::canRestorePad() const{
	return hasAdminRights();
}

bool User::canPromoteUserFromTo(const Level & prevLevel, const Level & newLevel) const{
	if (prevLevel == Owner || newLevel == Owner){
		return false;
	}
	switch (newLevel){
		case Admin:
		case Author:
		case Guest:{
			switch(m_level){
				case Owner: { return true; }
				case Admin: {
					switch (prevLevel){
						case Guest:
						case Author:{
							return newLevel != Admin;
						}
						case Admin:
						default: return false;
					}
				}
				default: { return false; }
			}
		}
		default:{
			QLOG_ERROR("undefined level recieved " << newLevel);
			return false;
		}
	}
}

QLatin1Literal User::levelName() const{
	switch (m_level){
		case Owner:  { return QLatin1Literal("owner"  ); }
		case Admin:  { return QLatin1Literal("admin"  ); }
		case Author: { return QLatin1Literal("author" ); }
		case Guest:  { return QLatin1Literal("guest"  ); }
		case Unknown:{ return QLatin1Literal("unknown");}
		default:     { return QLatin1Literal("SIGSEGV");}
	}
}

}





