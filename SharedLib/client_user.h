#ifndef USER_H
#define USER_H

#include <QString>
#include <QColor>
#include <QRegularExpression>
#include <QTextCharFormat>
#include <QTextBlockFormat>
#include <QJsonValue>
#include <QLatin1Literal>

#include "sharedlib_global.h"
#include "range.h"
#include "std_ext.h"
#include "message_exception.h"
#include "error.h"


namespace Client SHAREDLIBSHARED_EXPORT{

class User {
public:
	static const QColor & finalColor();


	DECL_DEFAULT_COPY_AND_MOVE(User)
	static const Range<> PASSWORD_RANGE;
	static const Range<> LOGIN_RANGE;
	static const Range<> NAME_RANGE;

	static constexpr const char * const PASSWORD_REG_EXP = R"~~(^(?:\w|\d){4,36}$)~~";
	static constexpr const char * const LOGIN_REG_EXP    = R"~~(^(?:\w|\d){3,32}$)~~";
	static constexpr const char * const NAME_REG_EXP     = R"~~(^(?:\w|\d){2,128}$)~~";

	static const QColor DEFAULT_COLOR;

	enum Level {
		Owner,
		Admin,
		Author,
		Guest,
		Unknown,

		Min = Owner,
		Max = Unknown
	};

	explicit User(QString && name = "no_name",
				  const id_t & id = nullid,
				  QColor && color = QColor(DEFAULT_COLOR),
				  const Level & level = Guest);


	const QString & name() const;
	const QColor & color() const;
	id_t id() const;

	void setName(QString && name);
	virtual void setColor(QColor && color);
	void setId(const id_t & id);

	Level level() const;
	void setLevel(const Level & level);

	QLatin1Literal levelName() const;

	QTextBlockFormat textBlockFormat() const;
	QTextCharFormat textCharFormat() const;

	bool hasValidId() const;

	QJsonValue toJsonValue() const;

	/* beware compares only users id */
	bool operator==(const User & other);

	/* throws MessageException */
	static User fromJsonValue(const QJsonValue & value);

	static User nullobj();
	static bool isName(const QString & suspect);
	static bool isLogin(const QString & suspect);
	static bool isPassword(const QString & suspect);

	friend QDataStream &operator<<(QDataStream & stream, const User & self);
	friend QDataStream &operator>>(QDataStream & stream, User & self);

	friend QDebug &operator<<(QDebug & debugStream, const User & self);


	bool isOwner() const;
	bool isAdmin() const;
	bool isAuthor() const;
	bool isGuest() const;

	void promoteToOwner();
	void promoteToAdmin();
	void promoteToAuthor();
	void promoteToGuest();

	bool hasAdminRights() const;
	bool hasAuthorRights() const;
	bool canModifyFinalText() const;
	bool canModifyUserRightsFor(const Client::User & user) const;
	bool canRestorePad() const;
	bool canPromoteUserFromTo(const Level & prevLevel,
							  const Level & newLevel) const;

protected:
	static constexpr const char * const NAME_FIELDNAME  = "name";
	static constexpr const char * const COLOR_FIELDNAME = "color";
	static constexpr const char * const ID_FIELDNAME    = "id";
	static constexpr const char * const LEVEL_FIELDNAME = "lvl";


	QString m_name;
	QColor m_color;
	id_t m_id;
	Level m_level;
};

}

Q_DECLARE_METATYPE(Client::User)

#endif // USER_H
