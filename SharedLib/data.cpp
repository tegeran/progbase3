#include <QJsonArray>

#include "data.h"
#include "qext.h"
#include "server_pad.h"

namespace Data {

// --------------------------------

QJsonValue String::toJsonValue(const QString & string){
	return QJsonValue(string);
}

String String::fromJsonValue(const QJsonValue & jsonValue){
	if (!jsonValue.isString())
	{
		throw MessageException("string data packet of non string type");
	}
	return String(jsonValue.toString());
}

// --------------------------------

QJsonValue TypeOnly::toJsonValue(){
	return QJsonValue(QJsonValue::Null);
}

// --------------------------------

QJsonValue ClientPads::toJsonValue(const QHash<id_t, Server::Pad *> & pads,
								   const Server::User * const & userAccessor){
	QSUPPOSE(userAccessor);
	QJsonArray jsonArray;
	for (const Server::Pad * serverPad : pads){
		if (userAccessor->hasAccessToPad(serverPad)){
			jsonArray << static_cast<const Client::Pad *>(serverPad)->toJsonValue();
		}
	}
	return QJsonValue(jsonArray);
}


ClientPads ClientPads::fromJsonValue(const QJsonValue & jsonValue){
	if (!jsonValue.isArray())
	{
		throw MessageException("pads hashtable of nonn json array type");
	}
	ClientPads padsPacket;
	for (const QJsonValue & padValue : jsonValue.toArray()){
		Client::Pad pad { Client::Pad::fromJsonValue(padValue) };
		padsPacket.pads.insert(pad.id(), std::move(pad));
	}
	return padsPacket;
}

// --------------------------------

QJsonValue Authorization::toJsonValue(const QString & login,
									 const QString & password)
{
	return QJsonValue(
		QJsonObject({
			Qext::makeQPair(QString("login"), QJsonValue(login)),
			Qext::makeQPair(QString("password"), QJsonValue(password)),
		})
	);
}

Authorization Authorization::fromJsonValue(const QJsonValue & jsonValue){
	if (!jsonValue.isObject()){
		throw MessageException("authorization data of non json object type");
	}
	QJsonObject obj { jsonValue.toObject() };
	QJsonValue loginValue    { obj["login"   ] };
	QJsonValue passwordValue { obj["password"] };
	if (!loginValue.isString()){
		throw MessageException("invalid login value");
	}
	if (!passwordValue.isString()){
		throw MessageException("invalid password value");
	}
	return Authorization(loginValue.toString(), passwordValue.toString());
}

// --------------------------------

QJsonValue ClientUser::toJsonValue(const Client::User & user){
	return user.toJsonValue();
}

ClientUser ClientUser::fromJsonValue(const QJsonValue & jsonValue){
	return ClientUser(Client::User::fromJsonValue(jsonValue));
}

// --------------------------------

QJsonValue Registration::toJsonValue(const QString & login,
									 const QString & password,
									 const QString & name){
	return QJsonValue(
		QJsonObject({
			Qext::makeQPair(QString("auth"), Authorization::toJsonValue(login, password)),
			Qext::makeQPair(QString("name"), QJsonValue(name))
		})
	);
}

Registration Registration::fromJsonValue(const QJsonValue & jsonValue){
	if (!jsonValue.isObject())
	{
		throw MessageException("registration data of non json object type");
	}
	QJsonObject obj { jsonValue.toObject() };
	QJsonValue authValue  { obj["auth" ] };
	QJsonValue nameValue  { obj["name" ] };
	if (!nameValue.isString())
	{
		throw MessageException("invalid client name type");
	}
	return Registration(
		Authorization::fromJsonValue(authValue),
		nameValue.toString()
	);
}

// --------------------------------

QJsonValue Id::toJsonValue(const id_t & id){
	return QJsonValue(static_cast<qint64>(id));
}

Id Id::fromJsonValue(const QJsonValue & jsonValue){
	if (!jsonValue.isDouble()){
		throw MessageException("id of non integral value");
	}
	int idInt { jsonValue.toInt() };
	if (!std_ext::isInRangeOf<id_t>(idInt)){
		throw MessageException("id is out of range");
	}
	return Id(static_cast<id_t>(idInt));
}

// --------------------------------


QJsonValue IdAndString::toJsonValue(const id_t & id, const QString & string){
	return QJsonValue(
		QJsonObject({
			Qext::makeQPair(QString("id"), QJsonValue(static_cast<qint64>(id))),
			Qext::makeQPair(QString("string"), QJsonValue(string))
		})
	);
}

IdAndString IdAndString::fromJsonValue(const QJsonValue & value){
	if (!value.isObject()){
		throw MessageException("id and string of non json value type");
	}
	QJsonObject obj { value.toObject() };
	QJsonValue idValue { obj["id"    ] };
	QJsonValue strValue{ obj["string"] };
	if (!strValue.isString()){
		throw MessageException("invalid string value");
	}
	return IdAndString(
		Qext::Json::tryReadFromJsonValue<id_t>(idValue),
		strValue.toString()
				);
}

// --------------------------------

QJsonValue Color::toJsonValue(const QColor & color){
	return Qext::Color::toJsonValue(color);
}

Color Color::fromJsonValue(const QJsonValue & jsonValue){
	return Color(Qext::Color::fromJsonValue(jsonValue));
}

// --------------------------------

QJsonValue IdAndColor::toJsonValue(const id_t & id, const QColor & color){
	return QJsonValue(
		QJsonObject({
			Qext::makeQPair(QString("id"), QJsonValue(static_cast<qint64>(id))),
			Qext::makeQPair(QString("color"), Qext::Color::toJsonValue(color))
		})
				);
}

IdAndColor IdAndColor::fromJsonValue(const QJsonValue & jsonValue){
	if (!jsonValue.isObject()){
		throw MessageException("id and color of non json object type");
	}
	QJsonObject obj { jsonValue.toObject() };
	return IdAndColor(
		Qext::Json::tryReadFromJsonValue<id_t>(obj["id"]),
		Qext::Color::fromJsonValue(obj["color"])
			);
}


// --------------------------------

QJsonValue IndexAndAmount::toJsonValue(const int & index, const int & amount){
	return QJsonValue(
		QJsonObject({
			Qext::makeQPair(QString("index"), QJsonValue(index)),
			Qext::makeQPair(QString("amount"), QJsonValue(amount))
		})
	);
}

IndexAndAmount IndexAndAmount::fromJsonValue(const QJsonValue & jsonValue){
	if (!jsonValue.isObject()){
		throw MessageException("index and value are of non json object type");
	}
	QJsonObject obj { jsonValue.toObject() };
	return IndexAndAmount(
		Qext::Json::tryReadFromJsonValue<int>(obj["index" ]),
		Qext::Json::tryReadFromJsonValue<int>(obj["amount"])
	);
}

// --------------------------------

QJsonValue IndexAndTextFragment::toJsonValue(const int & index,
											 const QTextDocumentFragment & fragment){
	return QJsonObject({
	   Qext::Json::keyValue("index", index),
	   Qext::Json::keyValue("fragment", fragment.toHtml())
	});
}

QJsonValue IndexAndTextFragment::toJsonValue(const int & index, const QString & fragmentHtml){
	return QJsonObject({
	   Qext::Json::keyValue("index", index),
	   Qext::Json::keyValue("fragment", fragmentHtml)
	});
}

IndexAndTextFragment IndexAndTextFragment::fromJsonValue(const QJsonValue & jsonValue){
	if (!jsonValue.isObject()){
		throw MessageException("index and text fragment of non json object type");
	}
	QJsonObject obj { jsonValue.toObject() };
	QJsonValue fragmentValue { obj["fragment"] };
	if (!fragmentValue.isString()){
		throw MessageException("text fragment of non string type");
	}

	return IndexAndTextFragment(
		Qext::Json::tryReadFromJsonValue<int>(obj["index"]),
		QTextDocumentFragment::fromHtml(fragmentValue.toString())
				);
}

// --------------------------------

QJsonValue UserAndAllPads::toJsonValue(
	const Server::User * const & user,
	const QHash<id_t, Server::Pad *> & padsOnline,
	const QVector<Client::Pad> & ownerPads,
	const QVector<Client::Pad> & adminPads,
	const QVector<Client::Pad> & authorPads)
{
	QJsonArray onlineArray;
	for (Server::Pad * const & pad : padsOnline){
		onlineArray << pad->toJsonValue();
	}
	return QJsonValue(
		QJsonObject({
			Qext::Json::keyValue("user", user->toJsonValue()),
			Qext::Json::keyValue("online", onlineArray),
			Qext::Json::keyValue("owner", Qext::Json::toJsonArray(ownerPads)),
			Qext::Json::keyValue("admin", Qext::Json::toJsonArray(adminPads)),
			Qext::Json::keyValue("author", Qext::Json::toJsonArray(authorPads))
		})
	);
}

UserAndAllPads UserAndAllPads::fromJsonValue(const QJsonValue & jsonValue){
	QJsonObject obj { Qext::Json::tryReadFromJsonValue<QJsonObject>(jsonValue) };
	UserAndAllPads result(Client::User::fromJsonValue(obj["user"]));
	QJsonArray onlinePadsArray(
		Qext::Json::tryReadFromJsonValue<QJsonArray>(obj["online"])
	);
	for (const QJsonValue & value : onlinePadsArray){
		Client::Pad pad(Client::Pad::fromJsonValue(value));
		result.padsOnline.insert(pad.id(), std::move(pad));
	}
	QJsonArray ownerPadsArray(
		Qext::Json::tryReadFromJsonValue<QJsonArray>(obj["owner"])
	);
	for (const QJsonValue & value : ownerPadsArray){
		result.ownerPads << Client::Pad::fromJsonValue(value);
	}
	QJsonArray adminPadsArray(
		Qext::Json::tryReadFromJsonValue<QJsonArray>(obj["admin"])
	);
	for (const QJsonValue & value : adminPadsArray){
		result.adminPads << Client::Pad::fromJsonValue(value);
	}
	QJsonArray authorPadsArray(
		Qext::Json::tryReadFromJsonValue<QJsonArray>(obj["author"])
	);
	for (const QJsonValue & value : authorPadsArray){
		result.authorPads << Client::Pad::fromJsonValue(value);
	}
	return result;



}


// --------------------------------

QJsonValue ClientPad::toJsonValue(const Client::Pad & pad){
	return pad.toJsonValue();
}

ClientPad ClientPad::fromJsonValue(const QJsonValue & jsonValue){
	return ClientPad(Client::Pad::fromJsonValue(jsonValue));
}

// --------------------------------

QJsonValue NewPadData::toJsonValue(const QString & name, const bool & isPublic, const QString & textDocumentHtml){
	return QJsonObject({
		Qext::Json::keyValue("name",     name),
		Qext::Json::keyValue("isPublic", isPublic),
		Qext::Json::keyValue("document", textDocumentHtml)
	});
}

NewPadData NewPadData::fromJsonValue(const QJsonValue & jsonValue){
	QJsonObject obj { Qext::Json::tryReadFromJsonValue<QJsonObject>(jsonValue) };
	return NewPadData(
		Qext::Json::tryReadFromJsonValue<QString>(obj["name"    ]),
		Qext::Json::tryReadFromJsonValue<bool>(   obj["isPublic"]),
		Qext::Json::tryReadFromJsonValue<QString>(obj["document"])
	);
}


// --------------------------------



QJsonValue PadEnterData::toJsonValue(const Client::User::Level & level,
											  const id_t & id,
											  const QHash<id_t, Server::User *> & users,
											  const QTextDocument & document){
	QJsonArray jsonArray;
	for (Server::User * const & user : users){
		jsonArray << user->toJsonValue();
	}
	return QJsonObject({
		Qext::Json::keyValue("level", static_cast<int>(level)),
		Qext::Json::keyValue("id", id),
		Qext::Json::keyValue("users", jsonArray),
		Qext::Json::keyValue("document", document.toHtml())
	});
}

PadEnterData PadEnterData::fromJsonValue(const QJsonValue & jsonValue){
	const QJsonObject obj { Qext::Json::tryReadFromJsonValue<QJsonObject>(jsonValue) };

	PadEnterData dataPacket (
		Qext::Json::tryReadMinMaxEnum<Client::User::Level>(obj["level"]),
		Qext::Json::tryReadFromJsonValue<id_t>(obj["id"]),
		Qext::Json::tryReadFromJsonValue<QString>(obj["document"])
	);

	const QJsonArray usersJsonArray(Qext::Json::tryReadFromJsonValue<QJsonArray>(obj["users"]));

	for (const QJsonValue & userValue : usersJsonArray ){
		Client::User user { Client::User::fromJsonValue(userValue) };
		dataPacket.users.insert(user.id(), std::move(user));
	}
	return dataPacket;
}

// --------------------------------

bool RevisionsRange::isValid() const{
	return firstTimeId <= lastTimeId;
}

QJsonValue RevisionsRange::toJsonValue(const timeid_t & firstTimeId,
									   const timeid_t & lastTimeId,
									   const QVector<Netpacket> & commands){
	QJsonArray commandsArray;
	for (const Netpacket & command : commands){
		commandsArray << command.toJsonValue();
	}
	return QJsonObject({
		Qext::Json::keyValue("ftime_id", firstTimeId),
		Qext::Json::keyValue("ltime_id",  lastTimeId),
		Qext::Json::keyValue("commands", commandsArray)
	});
}

RevisionsRange RevisionsRange::fromJsonValue(const QJsonValue & value){
	QJsonObject obj(
		Qext::Json::tryReadFromJsonValue<QJsonObject>(value)
	);
	RevisionsRange revisions(
		Qext::Json::tryReadFromJsonValue<int>(obj["ftime_id"]),
		Qext::Json::tryReadFromJsonValue<int>(obj["ltime_id"])
	);
	QJsonArray commandsJsonArray(
		Qext::Json::tryReadFromJsonValue<QJsonArray>(obj["commands"])
	);
	revisions.commands.reserve(commandsJsonArray.size());
	for (const QJsonValue & commandValue : commandsJsonArray){
		Netpacket commandPacket(Netpacket::fromJsonValue(commandValue));
		switch (commandPacket.type()){
			case Netpacket::Revision_RemoveChars:
			case Netpacket::Revision_AddChars:{
				revisions.commands << std::move(commandPacket);
				break;
			}
			default:{
				throw MessageException("invalid revision netpacket type");
			}
		}
	}
	return revisions;
}

// --------------------------------

QJsonValue TimeIdRange::toJsonValue(const timeid_t & firstTimeId, const timeid_t & lastTimeId){
	return QJsonObject({
		Qext::Json::keyValue("first", firstTimeId),
		Qext::Json::keyValue("last" ,  lastTimeId)
   });
}

TimeIdRange TimeIdRange::fromJsonValue(const QJsonValue & value){
	QJsonObject obj(Qext::Json::tryReadFromJsonValue<QJsonObject>(value));
	return TimeIdRange(
		Qext::Json::tryReadFromJsonValue<timeid_t>(obj["first"]),
		Qext::Json::tryReadFromJsonValue<timeid_t>(obj["last"])
			);
}

// --------------------------------

QJsonValue DocumentAndRevisionsRange::toJsonValue(
		const QString & documentHtml,
		const RevisionsRange & revisions)
{
	return QJsonObject({
		Qext::Json::keyValue("document", documentHtml),
		Qext::Json::keyValue(
			"revisions",
			RevisionsRange::toJsonValue(
				revisions.firstTimeId,
				revisions.lastTimeId,
				revisions.commands
			)
		)
	});
}

DocumentAndRevisionsRange DocumentAndRevisionsRange::fromJsonValue(const QJsonValue & value){
	QJsonObject obj {
		Qext::Json::tryReadFromJsonValue<QJsonObject>(value)
	};
	return DocumentAndRevisionsRange(
		Qext::Json::tryReadFromJsonValue<QString>(obj["document"]),
		RevisionsRange::fromJsonValue(obj["revisions"])
	);
}


QJsonValue IdAndLevel::toJsonValue(const id_t & id, const Client::User::Level & level){
	return QJsonObject({
		Qext::Json::keyValue("id", id),
		Qext::Json::keyValue("level", level),
	});
}

IdAndLevel IdAndLevel::fromJsonValue(const QJsonValue & value){
	QJsonObject obj(Qext::Json::tryReadFromJsonValue<QJsonObject>(value));
	return IdAndLevel{
		Qext::Json::tryReadFromJsonValue<id_t>(obj["id"]),
		Qext::Json::tryReadMinMaxEnum<Client::User::Level>(obj["level"])
	};
}


// --------------------------------

// --------------------------------






}








