﻿#ifndef DATA_H
#define DATA_H

#include <QJsonValue>
#include <QHash>
#include <QTextDocumentFragment>

#include <utility>

#include "sharedlib_global.h"
#include "netpacket.h"
#include "client_pad.h"
#include "server_user.h"
#include "revision.h"
#include "qext.h"

namespace Data SHAREDLIBSHARED_EXPORT{
	struct String {
		DECL_DEFAULT_MOVE_ONLY(String)

		QString string;
		explicit String(QString && string)
			: string(std::move(string)) {}

		static QJsonValue toJsonValue(const QString & string);
		static String fromJsonValue(const QJsonValue & jsonValue);
	};

	template <typename TNumeric = int>
	struct Integer {
		DECL_DEFAULT_MOVE_ONLY(Integer)

		TNumeric integer;

		explicit Integer(const TNumeric & integer)
			: integer(integer) {}

		static QJsonValue toJsonValue(const TNumeric& integer);
		static Integer<TNumeric> fromJsonValue(const QJsonValue & jsonValue);
	};

	template <typename TNumeric>
	QJsonValue Integer<TNumeric>::toJsonValue(const TNumeric & integer){
		return QJsonValue(static_cast<qint64>(integer));
	}

	template <typename TNumeric>
	Integer<TNumeric> Integer<TNumeric>::fromJsonValue(const QJsonValue & jsonValue){
		return Integer<TNumeric>(Qext::Json::tryReadFromJsonValue<TNumeric>(jsonValue));
	}



	struct IndexAndAmount{
		DECL_DEFAULT_MOVE_ONLY(IndexAndAmount)

		int index;
		int amount;

		explicit IndexAndAmount(const int & index, const int & amount)
			: index(index), amount(amount) {}

		static QJsonValue toJsonValue(const int& index, const int & amount);
		static IndexAndAmount fromJsonValue(const QJsonValue & jsonValue);
	};

	struct Color {
		DECL_DEFAULT_MOVE_ONLY(Color)
		QColor color;

		explicit Color(QColor && color)
			: color(std::move(color)) {}

		static QJsonValue toJsonValue(const QColor & color);
		static Color fromJsonValue(const QJsonValue & jsonValue);
	};

	struct Id {
		DECL_DEFAULT_MOVE_ONLY(Id)

		id_t id;
		explicit Id(const id_t & id) : id(std::move(id)) {}

		static QJsonValue toJsonValue(const id_t & id);
		static Id fromJsonValue(const QJsonValue & jsonValue);
	};

	struct IdAndColor{
		DECL_DEFAULT_MOVE_ONLY(IdAndColor)

		id_t id;
		QColor color;

		explicit IdAndColor(const id_t & id, QColor && color)
			: id(id), color(std::move(color)) {}

		static QJsonValue toJsonValue(const id_t & id, const QColor & color);
		static IdAndColor fromJsonValue(const QJsonValue & jsonValue);
	};

	struct ClientUser {
		Client::User user;

		explicit ClientUser(Client::User && user)
			: user(std::move(user)) {}

		static QJsonValue toJsonValue(const Client::User & user);
		static ClientUser fromJsonValue(const QJsonValue & jsonValue);
	};


	struct Authorization {
		DECL_DEFAULT_MOVE_ONLY(Authorization)
		QString login;
		QString password;

		explicit Authorization(QString && login, QString && password)
			: login(std::move(login)), password(std::move(password)) {}

		static QJsonValue toJsonValue(const QString & login, const QString & password);
		static Authorization fromJsonValue(const QJsonValue & jsonValue);
	};

	struct Registration {
		DECL_DEFAULT_MOVE_ONLY(Registration)
		Authorization auth;
		QString name;

		explicit Registration(Authorization && auth,
							  QString && name)
			: auth(std::move(auth)),
			  name(std::move(name)) {}

		static QJsonValue toJsonValue(const QString & login,
									  const QString & password,
									  const QString & name);

		static Registration fromJsonValue(const QJsonValue & jsonValue);
	};

	struct IndexAndTextFragment {
		DECL_DEFAULT_MOVE_ONLY(IndexAndTextFragment)
		int index;
		QTextDocumentFragment fragment;

		explicit IndexAndTextFragment(const int & index,
									  QTextDocumentFragment && fragment)
			: index(index), fragment(std::move(fragment)) {}

		static QJsonValue toJsonValue(const int & index,
									  const QTextDocumentFragment & fragment);
		static QJsonValue toJsonValue(const int & index,
									  const QString & fragmentHtml);

		static IndexAndTextFragment fromJsonValue(const QJsonValue & jsonValue);

	};


	struct IdAndString{
		DECL_DEFAULT_MOVE_ONLY(IdAndString)
		id_t id;
		QString string;

		explicit IdAndString(const id_t & id, QString && string)
			: id(id), string(std::move(string)) {}

		static QJsonValue toJsonValue(const id_t & id, const QString & string);
		static IdAndString fromJsonValue(const QJsonValue & value);
	};

	struct TypeOnly {
		DECL_DEFAULT_MOVE_ONLY(TypeOnly)
		TypeOnly() = default;
		static QJsonValue toJsonValue();
	};

	struct ClientPads {
		DECL_DEFAULT_MOVE_ONLY(ClientPads)
		QHash<id_t, Client::Pad> pads;
		explicit ClientPads(QHash<id_t, Client::Pad> && pads = QHash<id_t, Client::Pad>())
			: pads(std::move(pads)) {}

		static QJsonValue toJsonValue(
			const QHash<id_t, Server::Pad *> & pads,
			const Server::User * const & userAccessor
		);
		static ClientPads fromJsonValue(const QJsonValue & jsonValue);

	};

	struct UserAndAllPads{
		DECL_DEFAULT_COPY_AND_MOVE(UserAndAllPads)
		Client::User user;
		QHash<id_t, Client::Pad> padsOnline;
		QVector<Client::Pad> ownerPads;
		QVector<Client::Pad> adminPads;
		QVector<Client::Pad> authorPads;

		explicit UserAndAllPads(Client::User && user = Client::User::nullobj())
							   : user(std::move(user)) {}

		static QJsonValue toJsonValue(const Server::User  * const & user,
									  const QHash<id_t, Server::Pad *> & padsOnline,
									  const QVector<Client::Pad> & ownerPads,
									  const QVector<Client::Pad> & adminPads,
									  const QVector<Client::Pad> & authorPads);
		static UserAndAllPads fromJsonValue(const QJsonValue & jsonValue);
	};

	struct ClientPad{
		DECL_DEFAULT_MOVE_ONLY(ClientPad)

		Client::Pad pad;

		explicit ClientPad(Client::Pad && pad)
			: pad(std::move(pad)) {}

		static QJsonValue toJsonValue(const Client::Pad & pad);
		static ClientPad fromJsonValue(const QJsonValue & jsonValue);
	};

	struct NewPadData {
		DECL_DEFAULT_MOVE_ONLY(NewPadData)

		QString name;
		bool isPublic;
		QString textDocumentHtml;

		explicit NewPadData(QString && name, const bool & isPublic, QString && textDocumentHtml)
			: name(std::move(name)), isPublic(isPublic), textDocumentHtml(textDocumentHtml) {}

		static QJsonValue toJsonValue(const QString & name,
									  const bool & isPublic,
									  const QString & textDocumentHtml);
		static NewPadData fromJsonValue(const QJsonValue & jsonValue);

	};

	struct PadEnterData {
		DECL_DEFAULT_MOVE_ONLY(PadEnterData)

		Client::User::Level level;
		id_t id;
		QString documentHtml;
		QHash<id_t , Client::User> users;

		explicit PadEnterData(
								  const Client::User::Level & level,
								  const id_t & id,
								  QString documentHtml = QString(),
								  QHash<id_t, Client::User> && users = QHash<id_t, Client::User>()
			)
			: level(level),
			  id(id),
			  documentHtml(std::move(documentHtml)),
			  users(std::move(users)) {}

		static QJsonValue toJsonValue(const Client::User::Level & level,
									  const id_t & id,
									  const QHash<id_t, Server::User *> & users,
									  const QTextDocument & document);
		static PadEnterData fromJsonValue(const QJsonValue & jsonValue);
	};


	struct TimeIdRange{
		DECL_DEFAULT_MOVE_ONLY(TimeIdRange)
		timeid_t first;
		timeid_t last;

		explicit TimeIdRange(const timeid_t & first, const timeid_t & last)
			: first(first), last(last) {}

		static QJsonValue toJsonValue(const timeid_t & firstTimeId,
									  const timeid_t & lastTimeId);
		static TimeIdRange fromJsonValue(const QJsonValue & value);
	};

	struct RevisionsRange {
		DECL_DEFAULT_COPY_AND_MOVE(RevisionsRange)

		timeid_t firstTimeId;
		timeid_t lastTimeId;
		QVector<Netpacket> commands;

		RevisionsRange() : firstTimeId(1), lastTimeId(0) {}
		explicit RevisionsRange(const timeid_t & firstTimeId,
								const timeid_t & lastTimeId)
			: firstTimeId(firstTimeId),
			  lastTimeId(lastTimeId) {}

		bool isValid() const;

		static QJsonValue toJsonValue(const timeid_t & firstTimeId,
									  const timeid_t & lastTimeId,
									  const QVector<Netpacket> & commands);
		static RevisionsRange fromJsonValue(const QJsonValue & value);
	};

	struct IdAndLevel{
		DECL_DEFAULT_MOVE_ONLY(IdAndLevel)

		id_t id;
		Client::User::Level level;

		explicit IdAndLevel(const id_t & id, const Client::User::Level & level)
			: id(id), level(level) {}


		static QJsonValue toJsonValue(const id_t & id, const Client::User::Level& level);

		static IdAndLevel fromJsonValue(const QJsonValue & value);
	};

	template <typename TData>
	struct SenderIdAnd{
		DECL_DEFAULT_MOVE_ONLY(SenderIdAnd)
		id_t id;
		TData data;

		explicit SenderIdAnd(const id_t & id, TData && data)
			: id(id), data(std::move(data)) {}

		template <typename... TArgs>
		static QJsonValue toJsonValue(const id_t & id, TArgs &&... args);
		static SenderIdAnd fromJsonValue(const QJsonValue & value);

	};

	struct DocumentAndRevisionsRange {
		DECL_DEFAULT_COPY_AND_MOVE(DocumentAndRevisionsRange)

		DocumentAndRevisionsRange() = default;
		QString documentHtml;
		RevisionsRange revisions;

		explicit DocumentAndRevisionsRange(QString && documentHtml,
										   RevisionsRange && revisions)
			: documentHtml(std::move(documentHtml)), revisions(std::move(revisions)) {}

		static QJsonValue toJsonValue(const QString & documentHtml,
									  const RevisionsRange & revisions);

		static DocumentAndRevisionsRange fromJsonValue(const QJsonValue & value);
	};

	// ------------------------------------------------------------------
	// ------------------------------------------------------------------
	// ------------------------------------------------------------------
	// ------------------------------------------------------------------

	template<typename TData>
	template <typename... TArgs>
	QJsonValue SenderIdAnd<TData>::toJsonValue(const id_t & id, TArgs &&... args){
		return QJsonObject{
			Qext::Json::keyValue("id", id),
			Qext::Json::keyValue("data", TData::toJsonValue(std::forward<TArgs>(args)...))
		};
	}

	template<typename TData>
	SenderIdAnd<TData> SenderIdAnd<TData>::fromJsonValue(const QJsonValue & value){
		QJsonObject obj(Qext::Json::tryReadFromJsonValue<QJsonObject>(value));
		return SenderIdAnd<TData>(
			Qext::Json::tryReadFromJsonValue<id_t>(obj["id"]),
			TData::fromJsonValue(obj["data"])
		);
	}
}

Q_DECLARE_METATYPE(Data::RevisionsRange)
Q_DECLARE_METATYPE(Data::DocumentAndRevisionsRange)
Q_DECLARE_METATYPE(Data::UserAndAllPads)

#endif // DATA_H
