#include <QSqlQuery>
#include <QCryptographicHash>
#include <QVariant>
#include <QMutexLocker>
#include <QTextDocument>

#include "dbmanager.h"
#include "error.h"
#include "qext.h"
#include "std_ext.h"
#include "server_pad.h"

#define DB_DRIVER "QSQLITE"

bool DbManager::s_isPragmaSet = false;

DbManager::DbManager(const char * const & connectionName,
					 const char * dbPath,
					 QMutex * const & mutex,
					 QObject * const & parent)
	: QObject(parent),
	  m_connectionName(connectionName),
	  m_dbPath(dbPath),
	  m_mutex(mutex)
	{
	QLOG("created database");
}

DbManager::~DbManager(){
	QSqlDatabase::removeDatabase(m_connectionName);
	QLOG("removed database " << m_connectionName);
}

void DbManager::initialize(){
	QSqlDatabase db (
		QSqlDatabase::addDatabase(
			DB_DRIVER,
			m_connectionName
		)
	);
	synchronize(m_mutex){

		db.setDatabaseName(m_dbPath);
		if (!s_isPragmaSet){
			db.open();
			s_isPragmaSet = true;
			QSqlQuery query(db);
			if (   !query.exec("PRAGMA journal_mode = WAL;")
				|| !query.exec("PRAGMA synchronous = FULL;")
				|| !query.exec("PRAGMA temp_store = MEMORY;"))
			{
				QSHUTDOWN("pragma failure -> " << query.lastError().text());
			}
		}
	}

}

void DbManager::queryUpdateUserLevel(
	Server::User * const & senderUser,
	const id_t & padId,
	const id_t & userId,
	const Client::User::Level & newLevel)
{
	synchronize(m_mutex){

		auto db(database());

		if (newLevel == Client::User::Guest){
			try {
				Qext::Sqlite::tryDeleteFrom(
					db,
					"Users_to_Pads",
					"WHERE pad_id = ? AND user_id = ?",
					{             padId,          userId}
				);
				emit successUpdateUserLevel(senderUser, padId, userId, newLevel);
			} catch (const MessageException & exception){
				emit failureUpdateUserLevel(senderUser, exception.what());
			}
		} else try{
			QSqlQuery query {
				Qext::Sqlite::trySelectFrom(
					db, "Users_to_Pads", {"1"},
					"WHERE pad_id = ? AND user_id = ?",
					{			  padId,		  userId})
			};
			if (query.next()){
				Qext::Sqlite::tryUpdate(
					db,
					"Users_to_Pads",
					{"level"},
					{static_cast<int>(newLevel)},
					"WHERE user_id = ? AND pad_id = ?",
					{userId, padId}
				);
			} else {
				Qext::Sqlite::tryInsertInto(
					db,
					"Users_to_Pads",
					{"user_id", "pad_id", "level"},
					{ userId, padId, static_cast<int>(newLevel)}
				);
			}
			emit successUpdateUserLevel(senderUser, padId, userId, newLevel);
		} catch (const MessageException & exception){
			emit failureUpdateUserLevel(senderUser, exception.what());
		}
	}
}

void DbManager::queryRestorePad(
	Server::Pad * const & senderPad,
	Server::User * const & senderUser,
	const id_t & padId,
	const timeid_t & restorationPoint)
{
	Data::RevisionsRange revisions;
	QTextDocument document;
	try {
		if (restorationPoint){
			revisions =	tryGetRevisionsRange(padId, 1, restorationPoint);
		}
		document.setHtml(
			tryGetPadDocument("first_doc_version", padId)
		);
	} catch (const MessageException & exception){
		QLOG_ERROR(exception.what());
		emit failureRestorePad(
			senderPad,
			senderUser,
			exception.what()
		);
		return;
	}
	for (const Netpacket & revision : revisions.commands){
		switch (revision.type()){
			case Netpacket::Revision_AddChars:{
				Data::IndexAndTextFragment data {
					revision.dataPacket<Data::IndexAndTextFragment>()
				};
				Qext::TextDocument::insertFragment(
					&document,
					data.fragment,
					data.index
				);
				break;
			}
			case Netpacket::Revision_RemoveChars:{
				Data::IndexAndAmount data{
					revision.dataPacket<Data::IndexAndAmount>()
				};
				Qext::TextDocument::removeText(
					&document,
					data.index,
					data.amount
				);
				break;
			}
			default:{ QSHUTDOWN("undefined behaviour"); }
		}
	}
	emit successRestorePad(senderPad, senderUser, document.toHtml());
}

void DbManager::queryOpenOldPad(
	Server::User * const & senderUser,
	const id_t & userId,
	const id_t & padId){

	auto db(database());
	try{
		QString padDocument;
		Client::Pad padData {
			tryGetPadDataFromId(padId, padDocument)
		};

		QSet<id_t> admins;
		QSet<id_t> authors;
		id_t ownerId(tryGetPadMembersData(padId, admins, authors));

		if (userId != ownerId
			&& !admins.contains(userId)
			&& !authors.contains(userId)){
			throw MessageException("user doesn't have enough rights to open required pad");
		}
		emit successOpenOldPad(
			senderUser,
			padData,
			padDocument,
			ownerId,
			admins,
			authors,
			tryGetLastPadRevisionId(padId)
		);
	} catch (const MessageException & exception){
		emit failureOpenOldPad(senderUser, exception.what());
		return;
	}
}

void DbManager::queryFirstDocVersionRevisions(Server::User * const & senderUser,
											  const id_t & padId,
											  const timeid_t & first,
											  const timeid_t & last){
	try {
		emit successFirstDocVersionRevisions(
			senderUser,
			Data::DocumentAndRevisionsRange(
				tryGetPadDocument("first_doc_version", padId),
				tryGetRevisionsRange(padId, first, last)
			)
		);
	} catch (const MessageException & exception){
		emit failureFirstDocVersionRevisions(
			senderUser,
			exception.what()
		);
	}
}

Data::RevisionsRange DbManager::tryGetRevisionsRange(const id_t & padId,
												  const timeid_t & first,
												  const timeid_t & last){
	QSUPPOSE(last >= first);
	QMutexLocker locker(m_mutex);
	QLOG("started reisions range query");
	QTime time;
	time.start();
	auto db(database());
	QSqlQuery query(db);
	enum ColumnIndex { Index, Fragment, Amount, time_id };
	if (!query.prepare(
		"SELECT indx,fragment, -1 AS amount, time_id FROM Revisions_added_chars, Revisions "
		"WHERE id IN (SELECT revision_id FROM Revisions  "
		"WHERE pad_id = :pad_id AND (time_id BETWEEN :first AND :last) AND type = 0) "
		"AND Revisions.revision_id = Revisions_added_chars.id AND type = 0 "
		"UNION "
		"SELECT indx, '' AS fragment, amount, time_id FROM Revisions_removed_chars, Revisions "
		"WHERE id IN (SELECT revision_id FROM Revisions "
		"WHERE pad_id = :pad_id AND (time_id BETWEEN :first AND :last) AND type = 1) "
		"AND Revisions.revision_id = Revisions_removed_chars.id AND type = 1 "
		"ORDER BY time_id;"
	)){
		locker.unlock();
		QSHUTDOWN(query.lastError().text());
		throw MessageException(
			query.lastError().text().toStdString()
		);
	}
	query.bindValue(":pad_id", padId);
	query.bindValue(":first",  first);
	query.bindValue(":last",    last);
	if (!query.exec()){
		locker.unlock();
		QSHUTDOWN(query.lastError().text());
		throw MessageException(
			query.lastError().text().toStdString()
		);
	}
	Data::RevisionsRange revisions(first, last);
	timeid_t debugCheck = 0;
	while (query.next()){
		int amount(query.value("amount").toInt());
		revisions.commands.push_back(
			amount < 0
			? Netpacket(
				Netpacket::Revision_AddChars,
				Data::IndexAndTextFragment::toJsonValue(
					query.value("indx").toInt(),
					query.value("fragment").toString()
				)
			)
			: Netpacket(
				Netpacket::Revision_RemoveChars,
				Data::IndexAndAmount::toJsonValue(
					query.value("indx").toInt(),
					amount
				)
			)
		);
		++debugCheck;
	}
	int elapsed(time.elapsed());
	QSUPPOSE(debugCheck == last - first + 1);
	QLOG("load revisions query elapsed -> " << elapsed << "ms");
	return revisions;
}

QString DbManager::tryGetPadDocument(const char * const & columnName, const id_t & padId){
	synchronize(m_mutex) {

		auto db(database());
		enum ColumnIndex { Document };
		QSqlQuery query(
			Qext::Sqlite::trySelectFrom(
				db,
				"Pads",
				{ columnName },
				"WHERE id = ?",
				{ padId }
			)
		);
		bool check(query.next());
		QSUPPOSE(check);
		return query.value(Document).toString();
	}
}

void DbManager::tryGetAllUserPads(
	const id_t & userId,
	Data::UserAndAllPads & toFill)
{
synchronize (m_mutex){

	auto db(database());
	enum ColumnIndex { Name, PadId, IsPublic, Level };
	QSqlQuery query(db);
	if (!query.prepare(
		"SELECT name, pad_id, is_public, Users_to_Pads.level AS level "
		"FROM Pads, Users_to_Pads "
		"WHERE user_id = ? AND pad_id = Pads.id;"))
	{
		QSHUTDOWN("query prepare error " << query.lastError().text());
	}
	query.bindValue(0, userId);
	if (!query.exec()){
		QSHUTDOWN("query exec error " << query.lastError().text());
	}
	while (query.next()){
		switch(static_cast<Client::User::Level>(query.value(Level).toInt())){
			case Client::User::Owner:{
				toFill.ownerPads.push_back(
					Client::Pad(
						query.value(Name).toString(),
						query.value(PadId).toUInt(),
						query.value(IsPublic).toBool()
					)
				);
				break;
			}
			case Client::User::Admin:{
				toFill.adminPads.push_back(
					Client::Pad(
						query.value(Name).toString(),
						query.value(PadId).toUInt(),
						query.value(IsPublic).toBool()
					)
				);
				break;
			}
			case Client::User::Author:{
				toFill.authorPads.push_back(
					Client::Pad(
						query.value(Name).toString(),
						query.value(PadId).toUInt(),
						query.value(IsPublic).toBool()
					)
				);
				break;
			}
			default:{
				QSHUTDOWN("undefined behaviour");
			}
		}
	}
}}

timeid_t DbManager::tryGetLastPadRevisionId(const id_t & padId){
	synchronize(m_mutex){

		auto db(database());
		QSqlQuery query{
			Qext::Sqlite::tryComputeValueFrom(
				db,
				"Revisions",
				"time_id",
				"MAX",
				"WHERE pad_id = ?",
				{ padId }
			)
		};
		return query.next()
				? query.value(0).toULongLong()
				: nulltimeid;
	}
}

Client::Pad DbManager::tryGetPadDataFromId(const id_t & id, QString & documentOutParam){
	synchronize(m_mutex){

		auto db(database());
		enum {Name, IsPublic, Document};
		QSqlQuery query{
			Qext::Sqlite::trySelectFrom(
				db,
				"Pads",
				{"name", "is_public", "document"},
				"WHERE id = ?",
				{ id }
			)
		};
		if (!query.next()){
			throw MessageException("no such pad was found");
		}
		documentOutParam = query.value(Document).toString();
		return Client::Pad(
			query.value(Name).toString(),
			id,
			query.value(IsPublic).toBool()
		);
	}
}



void DbManager::queryRevisionsRange(Server::User * const & senderUser,
									const id_t & padId,
									const timeid_t & first,
									const timeid_t & last){
	try {
		emit successRevisionsRange(
			senderUser,
			tryGetRevisionsRange(padId, first, last)
		);
	} catch (const MessageException & exception){
		emit failureRevisionsRange(senderUser, exception.what());
	}
}

void DbManager::queryAvailableRevisions(Server::User * const & senderUser, const id_t & padId){
	synchronize(m_mutex) {

		enum ColumnIndex { TimeId };
		auto db { database() };
		try {
			QSqlQuery query {
				Qext::Sqlite::tryComputeValueFrom(
					db,
					"Revisions",
					"time_id",
					"MAX",
					"WHERE pad_id = ?",
					{ padId }
				)
			};
			emit successAvailableRevisions(
				senderUser,
				query.next()
					? query.value(TimeId).toULongLong()
					: 0ULL
			);
		} catch (const MessageException & exception){
			QSHUTDOWN(exception.what());
		}
	}
}

void DbManager::querySaveCurrentDocument(const id_t & padId,
										 const QString & documentHtml){
	synchronize(m_mutex) {

		auto db { database() };
		try {
			Qext::Sqlite::tryUpdate(
				db,
				"Pads",
				{"document"},
				{ documentHtml },
				"WHERE id = ?",
				{ padId }
			);
		} catch (const MessageException & exception){
			QSHUTDOWN(exception.what());
		}
	}
}



void DbManager::queryUserAuthData(Server::User * const & senderUser,
									 const QString & login,
									 const QString & password){
	Data::UserAndAllPads authData;
	try {
		authData.user = tryGetUserFromLogPass(login, password);
	} catch (const MessageException & exception){
		emit failureUserAuthData(
			senderUser,
			exception.what()
		);
		return;
	}
	try {
		tryGetAllUserPads(authData.user.id(), authData);
	} catch (const MessageException & exception){
		emit failureUserAuthData(
			senderUser,
			exception.what()
		);
		return;
	}
	emit successUserAuthData(senderUser, authData);
}


Client::User DbManager::tryGetUserFromLogPass(const QString & login, const QString & password){
	synchronize(m_mutex){

		auto db { database() };
		enum FieldsIndex { Id, Color, Name };
		QSqlQuery query {
			Qext::Sqlite::trySelectFrom(
				db,
				"Users",
				{ "id", "color", "name" },
				"WHERE login = ? AND password = ?",
				{
					login,
					QCryptographicHash::hash(
						password.toUtf8(),
						QCryptographicHash::Md5
					).toHex()
				}
			)
		};
		if (!query.next()){
			throw MessageException("invalid login or password");
		}
		QColor userColor;
		userColor.setNamedColor(query.value(Color).toString());
		return Client::User(
			query.value(Name).toString(),
			static_cast<id_t>(query.value(Id).toInt()),
			std::move(userColor)
		);
	}
}

id_t DbManager::tryGetPadMembersData(const id_t & padId, QSet<id_t> & adminsOutParam, QSet<id_t> & authorsOutParam){
	synchronize(m_mutex){

		auto db(database());
		enum { UserId, Level };
		QSqlQuery query (
			Qext::Sqlite::trySelectFrom(
				db,
				"Users_to_Pads",
				{ "user_id", "level" },
				"WHERE pad_id = ?",
				{ padId }
			)
		);
		id_t ownerId = nullid;
		while (query.next()){
			switch (static_cast<Client::User::Level>(query.value(Level).toInt())){
				case Client::User::Admin:{
					adminsOutParam.insert(query.value(UserId).toUInt());
					break;
				}
				case Client::User::Author:{
					authorsOutParam.insert(query.value(UserId).toUInt() );
					break;
				}
				case Client::User::Owner:{
					QSUPPOSE(ownerId == nullid);
					ownerId = query.value(UserId).toUInt();
					break;
				}
				default:{ QSHUTDOWN("undefined behaviour");}
			}
		}
		return ownerId;
	}
}

void DbManager::queryRegisterNewUser(Server::User * const & senderUser,
									 const QString & login,
									 const QString & password,
									 const QString & name,
									 const QColor & color){
	if (isLoginAlreadyRegistered(login)){ /* <- locking mutex before this  */
		emit failureRegisterNewUser(      /*    function causes deadlock   */
			senderUser,
			"login is already taken"
		);
		return;
	}
	synchronize(m_mutex){

		auto db { database() };
		try {
			Qext::Sqlite::tryInsertInto(
				db,
				"Users",
				{"login", "password", "name", "color"},
				{
					login,
					QCryptographicHash::hash(
						password.toUtf8(),
						QCryptographicHash::Md5
					).toHex(),
					name,
					color
				}
			);
			QSqlQuery query {
				Qext::Sqlite::tryComputeValueFrom(db, "Users", "id", "MAX")
			};
			bool check(query.next());
			QSUPPOSE(check);
			emit successRegisterNewUser(
				senderUser,
				Client::User(
					QString(name),
					query.value(0).toUInt(),
					QColor(color)
				)
			);
		} catch (const MessageException & exception){
			emit failureRegisterNewUser(
				senderUser,
				exception.what()
			);
		}
	}
}

void DbManager::queryUpdateUserColor(Server::User * const & user,
									 const id_t & userId,
									 const QColor & color){
	synchronize(m_mutex){

		try {
			auto db { database() };
			Qext::Sqlite::tryUpdate(
				db,"Users",{"color"},{ color },
				"WHERE id = ?", { userId }
			);
			emit successUpdateUserColor(user, color);
		} catch (const MessageException & exception){
			emit failureUpdateUserColor(user, exception.what());
		}
	}
}

void DbManager::queryRegisterNewPad(Server::User * const & senderUser,
									const id_t & ownerId,
									const QString & name,
									const bool & isPublic,
									const QString & initialDocumentHtml){
	synchronize(m_mutex) {

		auto db { database() };
		try {
			Qext::Sqlite::tryInsertInto(
				db,
				"Pads",
				{"name", "is_public", "document", "first_doc_version"},
				{ name,   isPublic, initialDocumentHtml, initialDocumentHtml }
			);
			QSqlQuery query {
				Qext::Sqlite::tryComputeValueFrom(db, "Pads", "id", "MAX")
			};
			bool check(query.next());
			QSUPPOSE(check);
			id_t newPadId(query.value(0).toUInt());
			Qext::Sqlite::tryInsertInto(
				db,
				"Users_to_Pads",
				{"user_id", "pad_id", "level"},
				{
					ownerId,
					newPadId,
					static_cast<int>(Client::User::Owner)
				}
			);
			emit successRegisterNewPad(
				senderUser,
				Client::Pad(QString(name), newPadId, isPublic),
				initialDocumentHtml
			);
		} catch (const MessageException & exception){
			emit failureRegisterNewPad(
				senderUser,
				exception.what()
			);
		}
	}
}


QSqlDatabase DbManager::database(){
	QSqlDatabase db { QSqlDatabase::database(m_connectionName) };
	if (!db.open()){
		throw MessageException("database open() failure");
	} else {
		return db;
	}
}


bool DbManager::isLoginAlreadyRegistered(const QString & login){
	synchronize(m_mutex){
		auto db { database() };
		QSqlQuery query(db);
		query.prepare("SELECT 1 FROM Users WHERE login = :login");
		query.bindValue(0, login);
		bool res(query.exec());
		QSUPPOSE(res);
		return query.next();
	}
}




void DbManager::querySaveAddCharsRevision(
	const id_t & padId,
	const timeid_t & revisionTimeId,
	const int & index,
	const QString & fragmentHtml)
{
	synchronize(m_mutex){

		QLOG("started add chars query");
		QTime time;
		time.start();
		auto db(database());
		QSqlQuery query(db);
		enum AddedCharsColumnIndex { Index, Fragment } ;
		if (!query.prepare("INSERT INTO Revisions_added_chars (indx, fragment) "
						   "VALUES (?,?); "))
		{
			QSHUTDOWN("QUERY PREPARE ERRROR -> " << query.lastError().text());
		}
		query.bindValue(Index, index);
		query.bindValue(Fragment, fragmentHtml);
		if (!query.exec()){
			QSHUTDOWN("QUERY EXEC ERRROR -> " << query.lastError().text());
		}
		if (!query.prepare("INSERT INTO Revisions (time_id, pad_id, type, revision_id) "
						   "VALUES (?,?,?,(SELECT MAX(id) FROM Revisions_added_chars)); ")){
			QSHUTDOWN("QUERY EXEC ERRROR -> " << query.lastError().text());
		}
		enum RevisionsColumnIndex { TimeId, PadId, Type};
		query.bindValue(TimeId, revisionTimeId);
		query.bindValue(PadId, padId);
		query.bindValue(Type, static_cast<int>(Revision::Type::AddChars));
		if (!query.exec()){
			QSHUTDOWN("QUERY EXEC ERRROR -> " << query.lastError().text());
		}

		int elapsed { time.elapsed() };
		QLOG("revision add chars saved in " << elapsed << "ms"
			 << " used timeid -> " << revisionTimeId);
	}
}

void DbManager::querySaveAddCharsRevision(const id_t & padId,
										  const timeid_t & revisionTimeId,
										  const int & index,
										  const QTextDocumentFragment & fragment){
	querySaveAddCharsRevision(
		padId,
		revisionTimeId,
		index,
		fragment.toHtml()
	);
}

void DbManager::querySaveRemoveCharsRevision(const id_t & padId,
											 const timeid_t & revisionTimeId,
											 const int & index,
											 const int & amount){
	synchronize(m_mutex){

		QLOG("started remove chars query");
		QTime time;
		time.start();
		auto db(database());
		QSqlQuery query(db);
		enum RemovedCharsColumnIndex { Index, Amount} ;
		if (!query.prepare("INSERT INTO Revisions_removed_chars (indx, amount) "
						   "VALUES (?,?); "))
		{
			QSHUTDOWN("QUERY PREPARE ERRROR -> " << query.lastError().text());
		}
		query.bindValue(Index, index);
		query.bindValue(Amount, amount);
		if (!query.exec()){
			QSHUTDOWN("QUERY EXEC ERRROR -> " << query.lastError().text());
		}
		if (!query.prepare("INSERT INTO Revisions (time_id, pad_id, type, revision_id) "
						   "VALUES (?,?,?,(SELECT MAX(id) FROM Revisions_removed_chars)); ")){
			QSHUTDOWN("QUERY EXEC ERRROR -> " << query.lastError().text());
		}
		enum RevisionsColumnIndex { TimeId, PadId, Type};
		query.bindValue(TimeId, revisionTimeId);
		query.bindValue(PadId, padId);
		query.bindValue(Type, static_cast<int>(Revision::Type::RemoveChars));
		if (!query.exec()){
			QSHUTDOWN("QUERY EXEC ERRROR -> " << query.lastError().text());
		}

		int elapsed { time.elapsed() };
		QLOG("revision remove chars saved in " << elapsed
			 << "ms" << " used timeid -> " << revisionTimeId);

	}
}
