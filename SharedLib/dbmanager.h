#ifndef DBMANAGER_H
#define DBMANAGER_H

#include <QObject>
#include <QSqlDatabase>
#include <QVector>
#include <QMutex>

#include "sharedlib_global.h"
#include "server_user.h"
#include "revision.h"
#include "data.h"


namespace Client SHAREDLIBSHARED_EXPORT{
	class Pad;
}

class SHAREDLIBSHARED_EXPORT DbManager : public QObject {
	Q_OBJECT

public:
	DbManager(const char * const & connectionName,
			  const char * dbPath,
			  QMutex * const & mutex,
			  QObject * const & parent = nullptr);

	~DbManager();

public slots:
	void initialize();

	void queryUpdateUserLevel(
		Server::User * const & senderUser,
		const id_t & padId,
		const id_t & userId,
		const Client::User::Level & newLevel
	);

	void queryRestorePad(
		Server::Pad * const & senderPad,
		Server::User * const & senderUser,
		const id_t & padId,
		const timeid_t & restorationPoint
	);

	void queryOpenOldPad(
		Server::User * const & senderUser,
		const id_t & userId,
		const id_t & padId
	);


	void queryFirstDocVersionRevisions(Server::User * const & senderUser,
									   const id_t & padId,
									   const timeid_t & first,
									   const timeid_t & last);


	void queryRevisionsRange(Server::User * const & senderUser,
							 const id_t & padId,
							 const timeid_t & first,
							 const timeid_t & last);

	void queryAvailableRevisions(Server::User * const & senderUser,
								 const id_t & padId);

	void querySaveCurrentDocument(const id_t & padId,
								  const QString & documentHtml);

	void queryUserAuthData(Server::User * const & senderUser,
						   const QString & login,
						   const QString & password);

	void queryRegisterNewUser(Server::User * const & senderUser,
							  const QString & login,
							  const QString & password,
							  const QString & name,
							  const QColor & color);

	void queryUpdateUserColor(Server::User * const &  user,
							  const id_t & userId,
							  const QColor & color);

	void queryRegisterNewPad(Server::User * const & senderUser,
							 const id_t & onwerId,
							 const QString & name,
							 const bool & isPublic,
							 const QString & initialDocumentHtml);


	void querySaveAddCharsRevision(const id_t & padId,
								   const timeid_t & revisionTimeId,
								   const int & index,
								   const QString & fragmentHtml);

	void querySaveAddCharsRevision(const id_t & padId,
								   const timeid_t & revisionTimeId,
								   const int & index,
								   const QTextDocumentFragment & fragment);

	void querySaveRemoveCharsRevision(const id_t & padId,
									  const timeid_t & revisionTimeId,
									  const int & index,
									  const int & amount);

signals:

	void successUpdateUserLevel(
		Server::User * const & senderUser,
		const id_t & padId,
		const id_t & userId,
		const Client::User::Level & newLevel
	);


	void failureUpdateUserLevel(
		Server::User * const & senderUser,
		const QString & reason
	);

	void successOpenOldPad(
		Server::User * const & senderUser,
		const Client::Pad & padData,
		const QString & documentHtml,
		const id_t & ownerId,
		const QSet<id_t> admins,
		const QSet<id_t> authors,
		const timeid_t & lastRevision
	);


	void failureOpenOldPad(
		Server::User * const & senderUser,
		const QString & reason
	);

	void successRestorePad(
		Server::Pad * const & senderPad,
		Server::User * const & senderUser,
		const QString & newDocument
	);


	void failureRestorePad(
		Server::Pad * const & serverPad,
		Server::User * const & senderUser,
		const QString & reason
	);

	void successRevisionsRange(
		Server::User * const & senderUser,
		const Data::RevisionsRange & revisionsRange
	);

	void failureRevisionsRange(
		Server::User * const & senderUser,
		const QString & reason
	);

	void successAvailableRevisions(
		Server::User * const & senderUser,
		const timeid_t & lastRevisionId
	);

	void successRegisterNewPad(
		Server::User * const & senderUser,
		const Client::Pad & clientData,
		const QString & initialDocumentHtml
	);

	void failureRegisterNewPad(
		Server::User * const & senderUser,
		const QString & reason
	);


	void successUpdateUserColor(Server::User * const & user,
									  const QColor & color);

	void failureUpdateUserColor(Server::User * const & user,
									  const QString & reason);

	void successUserAuthData(Server::User * const & senderUser,
								const Data::UserAndAllPads & clientData);

	void failureUserAuthData(Server::User * const & senderUser,
								  const QString & reason);

	void failureRegisterNewUser(Server::User * const & senderUser,
									 const QString & reason);

	void successRegisterNewUser(Server::User * const & senderUser,
									 const Client::User & newClientData);

	void failureFirstDocVersionRevisions(Server::User * const & senderUser,
											  const QString & reason);

	void successFirstDocVersionRevisions(Server::User * const & senderUser,
											  const Data::DocumentAndRevisionsRange & data);
private:
	const char * m_connectionName;
	const char * m_dbPath;
	QMutex * m_mutex;

	static bool s_isPragmaSet;
	QSqlDatabase database();
	bool isLoginAlreadyRegistered(const QString & login);


	Data::RevisionsRange tryGetRevisionsRange(const id_t & padId,
										   const timeid_t & first,
										   const timeid_t & last);

	QString tryGetPadDocument(const char * const & columnName, const id_t & padId);

	void tryGetAllUserPads(
		const id_t & userId,
		Data::UserAndAllPads & toFill
	);

	timeid_t tryGetLastPadRevisionId(const id_t & padId);

	Client::Pad tryGetPadDataFromId(const id_t & id, QString & documentOutParam);

	Client::User tryGetUserFromLogPass(const QString & login, const QString & password);
	// returns ownerId
	id_t tryGetPadMembersData(const id_t & padId,
							  QSet<id_t> & adminsOutParam,
							  QSet<id_t> & authorsOutParam);

};

#endif // DBMANAGER_H
