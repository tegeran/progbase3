#pragma once
#include <QDebug>
#include <QMutex>

#include "sharedlib_global.h"
#include <cassert>
#include <clocale>
#include <exception>

namespace Debug SHAREDLIBSHARED_EXPORT{

	extern QMutex logMutex;

#define imply(presupposition, conclusion) (!(presupposition) || (conclusion))

#ifndef NO_DEBUG
    #define Debug_exists(expression)  assert((expression) && "An attempt to dereference NULL pointer was made");
    #define Debug_UTF8() assert(strcmp(setlocale(0, NULL), "C") && "An attempt to print wide character without UTF-8 format was made")
	#define Debug_inbound(suspect, leftBound, rightBound) assert((leftBound) <= (suspect) && (suspect) <= (rightBound) && "Value out of bounds exception");
	#define Debug_maxbound(suspect, rightBound) assert((suspect) <= (rightBound) && "Value out of bounds exception");
	#define Debug_minbound(suspect, leftBound) assert((suspect) >= (leftBound) && "Value out of bounds exception");
    #define Debug_suppose(expression, errorMessage) assert((expression) && (errorMessage))
    #define Debug_shutdown(reason) assert(0 && (reason))
	#define Debug_maxindex(suspect, maximum) assert(std_ext::inbound((suspect), 0, (maximum)) && "Index out of bounds exception")
    #ifndef NO_DEBUG_TIPS
        #define Debug_tip(expression, tipMessage) assert((expression) && (tipMessage))
    #else
        #define Debug_tip(expression, tipMessage)
    #endif

	#define ____QLOG(macroname, message) ({                                          \
		::Debug::logMutex.lock();					                                 \
		::qDebug().nospace() << (macroname) << " at " << __func__ << ": " << message;\
		::Debug::logMutex.unlock();												     \
	})

	#ifndef NO_QT_DEBUG
		#define QLOG_ERROR(message) \
		(____QLOG("QLOG_ERROR !*********!", message))

		#define QLOG(message) (____QLOG("QLOG", message))

		#define QSHUTDOWN(message) ({____QLOG("QSHUTDOWN", message); Q_ASSERT(false);});

		#define QSUPPOSE_Expansion(condition, message, ...)       \
			({(condition)                                         \
				? static_cast<void>(0)							  \
				: static_cast<void>(____QLOG("QSUPPOSE", message),\
					Q_ASSERT(#condition && false));				  \
			})

		#define QSUPPOSE(supposition, message...) \
			(QSUPPOSE_Expansion(supposition, ##message, "supposition failure"))

		#define QTRACE_CALL() ({                \
			::Debug::logMutex.lock();           \
			qDebug() << "Fncall to" << __func__;\
			::Debug::logMutex.unlock();         \
		})
	#else
		#define QLOG(message)
		#define QSHUTDOWN(message)
		#define QSUPPOSE(supposition, message...)
		#define QTRACE_CALL()
		#define QLOG_ERROR(message)
	#endif



    #ifndef NO_DEBUG_TRACE

    unsigned short _____getLevel(); // return current function stack height
    void _____stackPushFunc(const char *func);

    void _____stackPopFunc(const char **func);

        #define LEVEL_TOKEN '.'
        #define ESCAPING_STR " <<< "

        #define TRACE_CALL()                        \
                _____stackPushFunc(__func__);          \
                const char * _____escapedFunction __attribute__((cleanup(_____stackPopFunc))) = __func__

        #define TRACE_CALL_LINE() TRACE_CALL(); putchar('\n');
    #else
        #define TRACE_CALL()
        #define TRACE_CALL_LINE()
    #endif

#else
    #define Debug_exists(expression)
    #define Debug_UTF8()
    #define Debug_inbound(leftBound, suspect, rightBound)
    #define Debug_maxbound(suspect, rightBound)
    #define Debug_minbound(suspect, leftBound)
    #define Debug_suppose(expression, errorMessage)
    #define Debug_shutdown(reason)
	#define QLOG(message)
	#define tip(expression, tipMessage)

    #define TRACE_CALL()
    #define TRACE_CALL_LINE()
#endif
}
