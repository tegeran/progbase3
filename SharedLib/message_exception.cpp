#include "message_exception.h"
#include <string>
#include "error.h"


MessageException::MessageException(std::string &&error)
	: _error(std::move(error))
{
	QLOG_ERROR("MessageException instance was thrown -> " << _error.c_str());
}

MessageException::MessageException(const std::string &error)
	: _error(error) {
	QLOG_ERROR("MessageException instance was thrown -> " << _error.c_str());
}

const char *MessageException::what() const noexcept{
    return _error.c_str();
}
