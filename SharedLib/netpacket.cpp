#include <QJsonDocument>
#include <QJsonValue>
#include <QDataStream>

#include "netpacket.h"
#include "qnet.h"
#include "std_ext.h"
#include "message_exception.h"
#include "qext.h"
#include "error.h"
#include "client_user.h"
#include "tcpsocket.h"

Netpacket::Netpacket()
	: m_type(Null),
	  m_dataJsonValue(QJsonValue::Undefined){}

Netpacket::Netpacket(const QByteArray & dataArray)
	: m_dataJsonValue(QJsonValue::Undefined) {
	QLOG("\n>>>>>>>>>\n>>>>>>>>>" << dataArray << "\n>>>>>>>>>");

	try {
		QJsonDocument jsonDocument {Qext::Json::read(dataArray)}; /* throws MessageException */

		if (!jsonDocument.isObject())				  { goto error_label; }

		QJsonObject packetObj = jsonDocument.object();

		if (!Qext::Json::objectContains(packetObj,
				{TYPE_FIELDNAME, DATA_FIELDNAME}))    { goto error_label; }

		QJsonValue typeValue{ packetObj[TYPE_FIELDNAME] };
		m_dataJsonValue =     packetObj[DATA_FIELDNAME];

		if (!typeValue.isDouble())                    { goto error_label; }

		int packetType { typeValue.toInt() };

		if (!std_ext::enumContains<Type>(packetType)) { goto error_label; }

		m_type = static_cast<enum Type>(packetType);

		return;

	} catch (const MessageException &){
		// ... bad json syntax
	}

	error_label:
	m_type = Null;
	QLOG_ERROR("invalid request received: " << dataArray);
}

Netpacket::Netpacket(Netpacket && rvalue)
	: m_type(rvalue.m_type), m_dataJsonValue(std::move(rvalue.m_dataJsonValue))
{
	rvalue.m_type = Null;
}


Netpacket::Netpacket(const Netpacket & lvalue)
	: m_type(lvalue.m_type), m_dataJsonValue(lvalue.m_dataJsonValue) {}

Netpacket & Netpacket::operator=(Netpacket && rvalue){
	m_type = rvalue.m_type;
	m_dataJsonValue = std::move(rvalue.m_dataJsonValue);
	rvalue.m_type = Null;
	return *this;
}

Netpacket & Netpacket::operator=(const Netpacket & lvalue){
	m_type = lvalue.m_type;
	m_dataJsonValue = lvalue.m_dataJsonValue;
	return *this;
}


bool Netpacket::isValid() const{
	return m_type != Null;
}

void Netpacket::send(TcpSocket & receiverSocket) const	{
	if (!Qnet::canWriteTo(receiverSocket)){
		QLOG_ERROR(
			"socket is not ready to receive messages at -> "
			<< receiverSocket.peerAddress().toString()
		);
		return;
	}
	QByteArray dataArray {
		QJsonDocument(toJsonObject()).toJson(QJsonDocument::Compact)
	};
	receiverSocket.write(dataArray);
	QLOG("\n<<<<<<<<<\n<<<<<<<<<" << dataArray << "\n<<<<<<<<<");
	return;
}

const Netpacket::Type & Netpacket::type() const { return m_type; }

void Netpacket::setType(const Netpacket::Type & type){
	m_type = type;
}

QJsonValue Netpacket::toJsonValue() const{
	return toJsonObject();
}

QJsonObject Netpacket::toJsonObject() const{
	return QJsonObject({
		Qext::Json::keyValue(TYPE_FIELDNAME, m_type),
		Qext::Json::keyValue(DATA_FIELDNAME, m_dataJsonValue)
	});
}

Netpacket Netpacket::fromJsonValue(const QJsonValue & value){
	QJsonObject obj {
		Qext::Json::tryReadFromJsonValue<QJsonObject>(value)
	};
	if (!obj.contains(DATA_FIELDNAME)){
		throw MessageException("netpacket without data field");
	}
	return Netpacket(
		Qext::Json::tryReadMinMaxEnum<Type>(obj[TYPE_FIELDNAME]),
		obj[DATA_FIELDNAME]
	);
}


Netpacket::Netpacket(const Netpacket::Type & type, QJsonValue && dataJsonValue)
	: m_type(type), m_dataJsonValue(std::move(dataJsonValue)) {}








