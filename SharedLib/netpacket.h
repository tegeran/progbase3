#pragma once
#ifndef NETPACKET_H
#define NETPACKET_H

#include <QTcpSocket>
#include <QJsonObject>
#include <QJsonValue>

#include <utility>

#include "sharedlib_global.h"
#include "std_ext.h"
#include "error.h"
#include "client_user.h"

class SHAREDLIBSHARED_EXPORT TcpSocket; /* forward declaration to avoid cycic inclusion */

class SHAREDLIBSHARED_EXPORT Netpacket {
public:
	enum Type {
		Null = 0,
		Update_UserConnectedToPad,  // Data::ClientUser
		Update_UserLeftPad,         // Data::Id
		Update_UserDisconnected,    // Data::Id


		Request_RemoveCharacters,   // Data::IndexAndAmount
		Answer_RemovedCharacters,	// Data::TypeOnly
		Update_RemovedCharacters,   // Data::IndexAndAmount

		Request_AddCharacters,      // Data::IndexAndTextFragment
		Answer_AddedCharacters,		// Data::TypeOnly
		Update_AddedCharacters,		// Data::IndexAndTextFragment

		Request_PadsOnline,         // Data::TypeOnly
		Answer_PadsOnline,          // Data::ClientPads

		Request_CreateNewPad,	    // Data::PadnameAndPublicity
		Answer_CreatedNewPad,		// Data::ClientPad
		Update_AddedPadOnline,      // Data::ClientPad
		Update_RemovedPadOnline,    // Data::Id

		Request_DisconnectFromPad,  // Data::TypeOnly
		Answer_DisconnectedFromPad, // Data::TypeOnly

		Request_Authorization,      // Data::Authorization
		Answer_Authorization,       // Data::UserAndAllPads

		Request_Registration,       // Data::Registration
		Answer_Registration,        // Data::UserAndAllPads

		Request_ConnectToPad,       // Data::Id
		Answer_ConnectedToPad,      // Data::PadEnterData

		Request_ChangeColor,	    // Data::Color
		Answer_ChangedColor,		// Data::Color
		Update_UserChangedColor,    // Data::IdAndColor

		Request_SendChatMessage,    // Data::String
		Answer_DeliveredChatMessage,// Data::String
		Update_NewChatMessage,      // Data::IdAndString

		Request_AvailableRevisions, // Data::TypeOnly
		Answer_AvailableRevisions,  // Data::Integer<timeid_t>

		Request_RevisionsRange,		// Data::TimeIdRange
		Answer_RevisionsRange,		// Data::RevisionsRange

		Request_FirstDocRevision,	// Data::TimeIdRange
		Answer_FirstDocRevision,	// Data::DocumentAndRevisionsRange

		Request_RestorePad,			// Data::Integer<timeid_t>
		Answer_RestoredPad,			// Data::String
		Update_RestoredPad,			// Data::IdAndString


		Request_ChangeUserLevel,	// Data::IdAndLevel

		Update_ChangedUserLevel,	// Data::SenderIdAnd<Data::IdAndLevel>
															// ^~~ id is target user id
		Update_ForeignChangeUserLevel, // Data::SenderIdAnd<Data::IdAndLevel>
																// ^~~ id represents pad id
		//--------------------------------------------
		// ERROR CODES
		Answer_AuthorizationError,  // Data::String
		Answer_RegistrationError,   // Data::String
		Answer_RemovingCharsError,
		Answer_AddingCharsError,
		Answer_Error, /* general */ // Data::String

		//--------------------------------------------

		Revision_AddChars,
		Revision_RemoveChars,

		Min = Null,                 // private
		Max = Revision_RemoveChars  // private
	};

	Netpacket();
	Netpacket(const QByteArray & dataArray);
	Netpacket(Netpacket && rvalue);
	Netpacket(const Netpacket & lvalue);
	Netpacket & operator=(Netpacket && rvalue);
	Netpacket & operator=(const Netpacket & rvalue);
	Netpacket(const Type & type, QJsonValue && dataJsonValue);

	bool isValid() const;
	void send(TcpSocket & receiverSocket) const;

	template <typename TPacket>
	TPacket dataPacket() const; /* throws MessageException */

	const Type & type() const;
	void setType(const Type & type);

	QJsonValue toJsonValue() const;
	QJsonObject toJsonObject() const;

	/* throw MessageException */
	static Netpacket fromJsonValue(const QJsonValue & value);

private:
	static constexpr const char * const TYPE_FIELDNAME = "type";
	static constexpr const char * const DATA_FIELDNAME = "data";

	Type m_type;
	QJsonValue m_dataJsonValue;
};

template <typename TPacket>
TPacket Netpacket::dataPacket() const {
	QSUPPOSE(m_type != Null);
	return TPacket::fromJsonValue(static_cast<const QJsonValue>(m_dataJsonValue));
}


#endif // NETPACKET_H
