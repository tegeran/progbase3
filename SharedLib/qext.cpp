#include <QFile>
#include <QSqlQuery>
#include <QTextStream>
#include <QByteArray>
#include <QObject>
#include <QRegularExpression>
#include <QTextDocumentFragment>
#include <QDir>
#include <QTextDocument>
#include <QSqlError>

#include <limits>

#include "client_user.h"
#include "qext.h"
#include "message_exception.h"


namespace Qext {

    namespace File {
        QString read(const QString &path){
            QFile file(path);
            if (!file.open(QIODevice::ReadOnly)){
				throw MessageException(TR("failed to read file \"%1\"")
									   .arg(path).toStdString()
				);
            }
            QTextStream stream(&file);
            QString content = stream.readAll();
            file.close();
            return content;
        }

        void write(const QString &src, const QString &path){
            QFile file(path);
            if (!file.open(QIODevice::WriteOnly)){
				throw MessageException(TR("failed to open file \"%1\" for writing")
									   .arg(path).toStdString()
				);
            }
			QTextStream(&file) << src;
            file.close();
		}

		QString dirName(const QString & fullpath){
			int lastSlash = fullpath.lastIndexOf('/');
			if (lastSlash == -1){
				return fullpath;
			} else {
				return fullpath.right(fullpath.length() - lastSlash - 1);
			}
		}

	}

	namespace Xml {
		int XmlException::y(){
			return m_loc.y;
		}

		int XmlException::x(){
			return m_loc.x;
		}

		Location<int> XmlException::location(){
			return m_loc;
		}

		XmlException::XmlException(const char * const &msg, const int &y, const int &x)
			: MessageException(msg), m_loc(y, x) {}

		XmlException::XmlException(const QString &msg, const int &y, const int &x)
			: MessageException(msg.toStdString()), m_loc(y, x) {}

		XmlException::XmlException(std::string msg, const int &y, const int &x)
			: MessageException(std::move(msg)), m_loc(y, x) {}


		QDomDocument readFromFile(const QString &path){
			QFile file(path);
			QString err;
			Location<int> eloc;
			QDomDocument document;
			if (!document.setContent(&file, &err, &eloc.y, &eloc.x)){
				file.close();
				throw MessageException(TR("failed while parsing xml file \"%1\" (%2)").arg(err)
									   .arg(TR("error occured at %1").arg(eloc.toStdString().c_str()))
									   .toStdString()
				);
			}
			file.close();
			return document;
		}

		void writeToFile(const QDomDocument &document, const QString & path){
			QFile file(path);
			if (!file.open(QIODevice::WriteOnly)){
				throw MessageException(TR("failed to write xml document to \"%1\"")
									   .arg(path).toStdString()
				);
			}
			QTextStream(&file) << document.toString();
			file.close();
		}
	}


	namespace Json {
		QJsonDocument readFromFile(const QString &path){
			QByteArray dataBytes;
			{
				QString content = ::Qext::File::read(path);
				dataBytes.append(content);
			}
			QJsonParseError err;
			QJsonDocument document = { QJsonDocument::fromJson(dataBytes, &err) };
			if (err.error != QJsonParseError::NoError){
				throw JsonException(
							TR("failed while parsing json file \"%1\" (%2)")
								.arg(path).arg(err.errorString()),
							err.offset
				);
			}
			return document;
		}

		inline int JsonException::offset(){
			return m_offset;
		}

		JsonException::JsonException(const char * const &msg, const int &offset)
			: MessageException(msg), m_offset(offset) {}

		JsonException::JsonException(std::string msg, const int &offset)
			: MessageException(std::move(msg)), m_offset(offset) {}

		JsonException::JsonException(const QString &msg, const int &offset)
			: MessageException(msg.toStdString()), m_offset(offset) {}

		void writeToFile(const QJsonDocument &document, const QString &path){
			QFile file(path);
			if (!file.open(QIODevice::WriteOnly)){
				throw MessageException(TR("failed to write json to file \"%1\"")
									   .arg(path).toStdString()
				);
			}
			writeToFile(document, file);
			file.close();
		}

		void writeToFile(const QJsonDocument & document, QFile & file){
			QTextStream(&file) << document.toJson();
		}


		bool objectContains(const QJsonObject & obj, const std::initializer_list<const char *> & fields){
			for (const char * const & field : fields){
				if (!obj.contains(field)){
					return false;
				}
			}
			return true;
		}

		QJsonDocument read(const QByteArray & byteArray){
			QJsonParseError err;
			QJsonDocument document = { QJsonDocument::fromJson(byteArray, &err) };
			if (err.error != QJsonParseError::NoError){
				throw MessageException(
							TR("failed while parsing json data (%1)")
							.arg(err.errorString()).toStdString()
				);
			}
			return document;
		}

		QJsonDocument read(const QString & string){
			return read(string.toUtf8());
		}

	}

	namespace Sbox{

		void setMaxUnlimited(QSpinBox & sbox){
			sbox.setMaximum(std::numeric_limits<int>::max());
		}

		void setMaxUnlimited(QDoubleSpinBox & sbox){
			sbox.setMaximum(std::numeric_limits<double>::max());
		}

		void setMinUnlimited(QSpinBox & sbox){
			sbox.setMinimum(std::numeric_limits<int>::min());
		}

		void setMinUnlimited(QDoubleSpinBox & sbox){
			sbox.setMinimum(std::numeric_limits<double>::min());
		}

	}

	namespace Sbar {
		void showUndone(const QUndoCommand & command, QStatusBar & bar){
			bar.showMessage(TR("Undone: %1").arg(command.text()));
		}

		void showRedone(const QUndoCommand & command, QStatusBar & bar){
			bar.showMessage(command.text());
		}
	}

	namespace Object{
		void connectMultipleSignals(QObject * const & sender,
									const std::initializer_list<const char *> & sigs,
									QObject * const & receiver,
									const char * const & slot)
		{
			for (const char * const & signal : sigs)
				QObject::connect(sender, signal, receiver, slot);
		}

		void connectMultipleSenders(const std::initializer_list<QObject *> & senders,
									const char * const & signal,
									QObject * const & receiver,
									const char * const & slot)
		{
			for (QObject * const & sender : senders){
				QObject::connect(sender, signal, receiver, slot);
			}
		}

		void connectMultipleSignalsToSlots(
					  QObject * const & sender,
					  QObject * const & receiver,
					  const std::initializer_list<const char *> & signalsAndSlots){
			for (auto iterator = signalsAndSlots.begin();
					  iterator < signalsAndSlots.end();
					  iterator += 2)
			{
				QObject::connect(
					sender,
					*iterator,
					receiver,
					*(iterator + 1)
				);
			}
		}

		void disconnectMultipleSignalsFromSlots(
						QObject * const & sender,
						QObject * const & receiver,
						const std::initializer_list<const char *> & signalsAndSlots){
			for (auto iterator = signalsAndSlots.begin();
					  iterator < signalsAndSlots.end();
					  iterator += 2)
			{
				QObject::disconnect(
					sender,
					*iterator,
					receiver,
					*(iterator + 1)
				);
			}
		}

		LocalSigBlock::LocalSigBlock(QObject * const & obj)
			: m_obj(obj){
			m_obj->blockSignals(true);
		}

		LocalSigBlock::~LocalSigBlock(){
			m_obj->blockSignals(false);
		}

		bool LocalSigBlock::oneCycle(){
			return m_cycleVariable--;
		}

	}

	bool isSpacedIdentifier(const QString & suspect){
		return QRegularExpression(std_ext::SPACED_ID_REGEX).match(suspect).hasMatch();
	}

	namespace Label{
		void clearLabels(std::initializer_list<QLabel *> labels){
			for (QLabel * const & label : labels){
				label->clear();
			}
		}
	}

	void StringListModel::prependString(QStringListModel & model,
										const QString & string,
										const int & role)
	{
		model.insertRow(0);
		model.setData(model.index(0), QVariant(string), role);
	}


	bool StringListModel::removeString(QStringListModel & model, const QString & string){
		const QStringList & strings { model.stringList() };
		for (auto iterator { strings.begin() }; iterator != strings.end(); ++iterator){
			if (*iterator == string){
				model.removeRow(iterator - strings.begin());
				return true;
			}
		}
		return false;
	}

	bool StringListModel::addUniqueString(QStringListModel & model, const QString & string){
		if (std_ext::contains(model.stringList(), string)){
			return false;
		}
		model.insertRow(0);
		model.setData(model.index(0), QVariant(string), Qt::DisplayRole);
		return true;
	}

	QWidget * StackedWidget::removeLowestWidget(QStackedWidget & stackedWidget){
		if (stackedWidget.count() == 0){
			return nullptr;
		}
		QWidget * removedWidget { stackedWidget.widget(0) };
		stackedWidget.removeWidget(removedWidget);
		return removedWidget;
	}

	namespace Color {

		QJsonValue toJsonValue(const QColor & color){
			return QJsonValue(color.name());
		}

		QColor fromJsonValue(const QJsonValue & value){
			if (!value.isString()){
				throw MessageException("invalid color json value");
			}
			QColor ultimateColor;
			ultimateColor.setNamedColor(value.toString());
			return ultimateColor;
		}

	}

	namespace Dialog{
		void closeOnFinished(QDialog * const & dialog){
			QObject::connect(dialog, SIGNAL(finished(int)), dialog, SLOT(close()));
		}
	}

	namespace Widget {
		void deleteOnClosed(QWidget * widget){
			widget->setAttribute(Qt::WA_DeleteOnClose);
		}

		LocalUpdatesBlock::LocalUpdatesBlock(QWidget * const & widget)
			: m_widget(widget) {
			widget->setUpdatesEnabled(false);
		}

		LocalUpdatesBlock::~LocalUpdatesBlock(){
			m_widget->setUpdatesEnabled(true);
		}

	}
	namespace GridLayout {
		void repositionWidget(QGridLayout & gridLayout,
							  QWidget * const & widget,
							  const int & row,
							  const int & column,
							  const int & rowSpan,
							  const int & columnSpan,
							  const Qt::Alignment & alignment){
			gridLayout.removeWidget(widget);
			gridLayout.addWidget(widget, row, column, rowSpan, columnSpan, alignment);
		}
	}

	namespace TextDocument {
		int lastIndex(QTextDocument * const & document){
			QTextCursor cursor { document };
			cursor.movePosition(QTextCursor::End);
			return cursor.position();
		}


		bool hasIndex(QTextDocument * const & document, const int & index){
			return index <= lastIndex(document);
		}

		bool canRemove(QTextDocument * const & document,
								   const int & index,
								   const int & amount){
			return TextDocument::hasIndex(document, index + amount);
		}

		void removeText(QTextDocument * const & document,
									const int & index,
									const int & amount){
			QTextCursor cursor { document };
			cursor.setPosition(index);
			cursor.anchor();
			cursor.setPosition(index + amount, QTextCursor::KeepAnchor);
			cursor.removeSelectedText();
		}

		bool isSurroundedByFinalText(QTextDocument * const & document,
												   const int & index){
			QSUPPOSE(index <= TextDocument::lastIndex(document));
			if (!index){
				return false;
			}
			QTextCursor cursor { document };

			cursor.movePosition(QTextCursor::End);
			if (cursor.position() == index){
				return false;
			}

			cursor.setPosition(index);

			if (cursor.charFormat().background().color() != Client::User::finalColor()){
				return false;
			}
			cursor.movePosition(QTextCursor::NextCharacter);
			return cursor.charFormat().background().color() == Client::User::finalColor();
		}

		void insertFragment(QTextDocument * const & document,
										 const QTextDocumentFragment & fragment,
										 const int & index){
			QTextCursor cursor { document };
			cursor.setPosition(index);
			cursor.insertFragment(fragment);
		}

		bool containsFinalText(QTextDocument * const & document,
							   const int & index,
							   const int & amount){
			int lastIndex = index + amount - 1;
			for (QTextBlock block { document->findBlock(index)}; block.isValid(); block = block.next()){
				for (auto iterator { block.begin() }; !iterator.atEnd(); ++iterator){
					QTextFragment fragment { iterator.fragment() };
					if (fragment.position() + fragment.length() - 1 < index){
						continue;
					}
					if (fragment.position() > lastIndex){
						return false;
					}

					if (fragment.charFormat().background().color() == Client::User::finalColor()){
						return true;
					}
				}
			}
			return false;
		}

		QTextCursor cursorSelect(QTextDocument * const & document,
								 const int & firstPosition,
								 const int & lastPosition){
			if (lastPosition > lastIndex(document)){
				QSHUTDOWN("queried -> " << lastPosition
						   << "actual last index -> "<< lastIndex(document)
				);
				return QTextCursor();
			}
			QTextCursor cursor { document };
			cursor.setPosition(firstPosition);
			cursor.setPosition(lastPosition, QTextCursor::KeepAnchor);
			return cursor;
		}

		QTextCursor cursorSelectAll(QTextDocument * const & document)		{
			QTextCursor cursor { document };
			cursor.movePosition(QTextCursor::Start);
			cursor.movePosition(QTextCursor::End, QTextCursor::KeepAnchor);
			return cursor;
		}

		int fragmentSize(const QTextDocumentFragment & fragment){
			return fragment.isEmpty()
					? 0
					: fragment.toPlainText().size();
		}

	}

	bool Key::isArrow(const int & key){
		switch (key){
			case Qt::Key_Up:
			case Qt::Key_Down:
			case Qt::Key_Left:
			case Qt::Key_Right:
				return true;
			default:
				return false;
		}
	}

	namespace Sqlite {
		static void addBindValues(QSqlQuery & query,
							 const std::initializer_list<QVariant> & values){
			for (const QVariant & value : values){
				query.addBindValue(value);
			}
		}

		QSqlQuery tryInsertInto(const QSqlDatabase & database,
						   const char * const & tableName,
						   const std::initializer_list<const char *> & columns,
						   const std::initializer_list<QVariant> & values){
			QSUPPOSE(database.isOpen());
			QSUPPOSE(columns.size() > 0);
			QSUPPOSE(columns.size() == values.size());
			QSqlQuery query(database);
			QString queryString { QString("INSERT INTO %1 (").arg(tableName) };
			Qext::String::appendCommaSeparatedList(queryString, columns);
			queryString += ") VALUES (";
			Qext::String::appendCommaSeparatedValue(queryString, values.size(), '?');
			queryString += ");";
			if (!query.prepare(queryString)){
				throw MessageException(
					QString("failed to prepare sql query (%1)")
						.arg(query.lastError().text()).toStdString()
				);
			}
			addBindValues(query, values);
			if (!query.exec()){
				throw MessageException(
					QString("failed to execute sql query (%1)")
						.arg(query.lastError().text()).toStdString()
				);
			}
			return query;
		}

		QSqlQuery trySelectFrom(const QSqlDatabase & database,
								const char * const & tableName,
								const std::initializer_list<const char *> & columns,
								const char * const & condition,
								const std::initializer_list<QVariant> & boundValues,
								const char * const & ordering){
			QSUPPOSE(database.isOpen());
			QSUPPOSE(columns.size());
			QSqlQuery query(database);
			QString queryString { "SELECT "};
			Qext::String::appendCommaSeparatedList(queryString, columns);
			queryString += QString(" FROM %1 ").arg(tableName);
			queryString += condition;
			if (ordering){
				queryString += ' ';
				queryString += ordering;
			}
			queryString += ";";
			if (!query.prepare(queryString)){
				throw MessageException(
					QString("failed to prepare select-from sql query (%1)")
						.arg(query.lastError().text()).toStdString()
				);
			}
			addBindValues(query, boundValues);
			if (!query.exec()){
				throw MessageException(
					QString("failed to execute select-from sql query (%1)")
						.arg(query.lastError().text()).toStdString()
				);
			}
			return query;
		}

		QueryInsertInto::QueryInsertInto(const char * const & tableName,
										 const std::initializer_list<const char *> & columns,
										 const size_t & totalValues)
			: m_queryString(QString("INSERT INTO %1 (").arg(tableName)){
			Qext::String::appendCommaSeparatedList(m_queryString, columns);
			m_queryString += ") VALUES ";
			QString valueTemplate = "(";
			Qext::String::appendCommaSeparatedValue(
				valueTemplate, columns.size(), "?"
			);
			valueTemplate += ')';
			Qext::String::appendCommaSeparatedValue(
				m_queryString, totalValues, valueTemplate
			);
			m_queryString += ';';
		}

		QSqlQuery QueryInsertInto::tryExecQuery(const QSqlDatabase & db,
												const std::initializer_list<QVariant> & values){
			return tryExecQuery<const std::initializer_list<QVariant> &>(db, values);
		}

		QSqlQuery tryUpdate(const QSqlDatabase & database,
							const char * const & tableName,
							const std::initializer_list<const char *> & columns,
							const std::initializer_list<QVariant> & newValues,
							const char * const & condition,
							const std::initializer_list<QVariant> & conditionValues){
			QSUPPOSE(columns.size());
			QSUPPOSE(columns.size() == newValues.size());
			QString queryString { QString("UPDATE %1 SET ").arg(tableName) };
			queryString += *columns.begin();
			queryString += "=?";
			for (auto iterator { columns.begin() + 1 }; iterator != columns.end(); ++iterator){
				queryString += ',';
				queryString	+= *iterator;
				queryString += "=?";
			}
			queryString += " ";
			queryString += condition;
			queryString += ';';
			QSqlQuery query(database);
			if (!query.prepare(queryString)){
				throw MessageException(
					QString("failed to prepare update query (%1)")
						.arg(query.lastError().text()).toStdString()
				);
			}
			addBindValues(query, newValues);
			addBindValues(query, conditionValues);
			if (!query.exec()){
				throw MessageException(
					QString("failed to execute update query (%1)")
						.arg(query.lastError().text()).toStdString()
				);
			}
			return query;
		}

		QSqlQuery tryComputeValueFrom(const QSqlDatabase & database,
									  const char * const & tableName,
									  const char * const & column,
									  const char * const & function,
									  const char * const & condition,
									  const std::initializer_list<QVariant> & boundValues)
		{
			QString queryString {
				QString("SELECT %1(%2) FROM %3 ")
						.arg(function).arg(column).arg(tableName)
			};
			if (condition){
				queryString += condition;
			}
			queryString += ';';
			QSqlQuery query(database);
			if (!query.prepare(queryString)){
				throw MessageException(
					QString("failed to prepare select computation query (%1)")
						.arg(query.lastError().text()).toStdString()
				);
			}
			if (condition){
				addBindValues(query, boundValues);
			}
			if (!query.exec()){
				throw MessageException(
					QString("failed to execute select computation query (%1)")
						.arg(query.lastError().text()).toStdString()
				);
			}
			return query;
		}

		QSqlQuery tryDeleteFrom(const QSqlDatabase & database,
								const char * const & tableName,
								const char * const & condition,
								const std::initializer_list<QVariant> & boundValues)
		{
			QString queryString {
				QString("DELETE FROM %1 ").arg(tableName)
			};
			if (condition){
				queryString += condition;
				queryString += ";";
			}
			QSqlQuery query(database);
			if (!query.prepare(queryString)){
				throw MessageException(
					QString("failed to prepare delete query (%1)")
						.arg(query.lastError().text()).toStdString()
				);
			}
			if (condition){
				addBindValues(query, boundValues);
			}
			if (!query.exec()){
				throw MessageException(
					QString("failed to execute delete query (%1)")
						.arg(query.lastError().text()).toStdString()
				);
			}
			return query;
		}



	}
	namespace Thread {
		void deleteLaterOnFinished(QThread * const & thread){
			QObject::connect(
				thread,
				SIGNAL(finished()),
				thread,
				SLOT(deleteLater())
			);
		}
	}

	QMessageBox *throwNewMessageBox(
		const QMessageBox::Icon & icon,
		const QString & title,
		const QString & text,
		const QMessageBox::StandardButtons & buttons,
		QWidget * const & parent,
		const Qt::WindowFlags & flags)
	{
		QMessageBox * const messageBox {
			new QMessageBox(
				icon, title, text, buttons, parent, flags
			)
		};
		::Qext::Widget::deleteOnClosed(messageBox);
		messageBox->show();
		return messageBox;
	}

}










