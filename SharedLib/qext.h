﻿#ifndef QEXT
#define QEXT
#include <QMutex>
#include <QMutexLocker>
#include <QThread>
#include <QObject>
#include <QString>
#include <QException>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDomDocument>
#include <QSet>
#include <QFile>
#include <QByteArray>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QUndoCommand>
#include <QStatusBar>
#include <QPair>
#include <QLineEdit>
#include <QLabel>
#include <QStringListModel>
#include <QStackedWidget>
#include <QTimer>
#include <QColor>
#include <QGridLayout>
#include <QTextDocumentFragment>
#include <QTextFragment>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QTime>
#include <QMessageBox>


#include <utility>
#include <string>
#include <initializer_list>
#include <typeinfo>

#include "sharedlib_global.h"
#include "queue"
#include "error.h"
#include "location.h"
#include "message_exception.h"
#include "std_ext.h"

#define QSENDER(Type) static_cast<Type>(sender())
#define TR(...) (QObject::tr((__VA_ARGS__)))

// NO BREAK CYCLE
#define foreach_text_fragment(FRAGMENT_NAME, DOCUMENT)						     \
	if (QTextFragment FRAGMENT_NAME; true)									     \
	for (QTextBlock ____BLOCK##FRAGMENT_NAME { (DOCUMENT)->firstBlock() };       \
		____BLOCK##FRAGMENT_NAME.isValid();									     \
		____BLOCK##FRAGMENT_NAME = ____BLOCK##FRAGMENT_NAME.next())			     \
	for (auto ____ITERATOR##FRAGMENT_NAME { ____BLOCK##FRAGMENT_NAME.begin() };  \
			({____ITERATOR##FRAGMENT_NAME.atEnd()							     \
				? false														     \
				: (FRAGMENT_NAME = ____ITERATOR##FRAGMENT_NAME.fragment(), true);\
			});							                                         \
			++____ITERATOR##FRAGMENT_NAME)

#define mute_signals(QOBJECT) \
	for (::Qext::Object::LocalSigBlock _____SIG_BLOCKER(QOBJECT); _____SIG_BLOCKER.oneCycle(); )

#define suspend_updates(QWIDGET)\
	if (::Qext::Widget::LocalUpdatesBlock _____UPDATES_BLOCKER(QWIDGET); true)

#define synchronize(QMUTEX) \
	if (QMutexLocker _____MUTEX_LOCKER(QMUTEX); true)

namespace Qext SHAREDLIBSHARED_EXPORT{

	bool isSpacedIdentifier(const QString & suspect);


	QMessageBox * throwNewMessageBox(
		const QMessageBox::Icon & icon,
		const QString & title,
		const QString & text,
		const QMessageBox::StandardButtons & butons = QMessageBox::NoButton,
		QWidget * const & parent = nullptr,
		const Qt::WindowFlags & flags = Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint
	);

	template <typename TFirst, typename TSecond>
	inline QPair<TFirst, TSecond> makeQPair(TFirst && firstValue, TSecond && secondValue);

	namespace File {

		QString dirName(const QString & fullpath);

        QString read(const QString & path); // throws MessageException
        void write(const QString & src, const QString & path); // throws MessageException
    }

	namespace Set {

		template <typename TEntity, typename TCont, typename TCompar>
		QSet<TEntity> fromStdPriorityQueue(std::priority_queue<TEntity, TCont, TCompar> & que,
										   int amount);

		template <typename TEntity, typename TCont, typename TCompar>
		QSet<TEntity> fromStdPriorityQueue(std::priority_queue<TEntity, TCont, TCompar> & que);
	}

	namespace Dialog{
		void closeOnFinished(QDialog * const & dialog);
	}

	namespace Xml {
		class XmlException : public MessageException {
		private:
			Location<int> m_loc;
		public:
			inline Location<int> location();
			inline int y();
			inline int x();

			XmlException(const char * const & msg, const int &y, const int &x);
			XmlException(std::string msg, const int &y, const int &x);
			XmlException(const QString& msg, const int &y, const int &x);

		};


		QDomDocument readFromFile(const QString & path);
		void writeToFile(const QDomDocument & document, const QString & path);  // throws MessageException
	}

	namespace Json {

		class JsonException : public MessageException {
		private:
			int m_offset;
		public:
			inline int offset();
			JsonException(const char * const & msg, const int & offset);
			JsonException(std::string msg, const int & offset);
			JsonException(const QString &msg, const int & offset);
		};

		template<typename TContainer>
		QJsonArray toJsonArray(TContainer && container);

		QJsonDocument readFromFile(const QString & path); // throws JsonException
		void writeToFile(const QJsonDocument & document, const QString & path); // throws MessageException
		void writeToFile(const QJsonDocument & document, QFile & file); // throws MessageException

		bool objectContains(const QJsonObject & obj, const std::initializer_list<const char *> & fields);
		QJsonDocument read(const QByteArray & byteArray); /* throws MessageException */
		QJsonDocument read(const QString & string);

		template <typename TIntegral>
		std::enable_if_t<std::is_integral_v<TIntegral>, TIntegral>
		tryReadFromJsonValue(const QJsonValue & value);


		template <typename TReal>
		std::enable_if_t<std::is_floating_point_v<TReal>, TReal>
		tryReadFromJsonValue(const QJsonValue & value);

		template <typename TQJsonObject>
		std::enable_if_t<std::is_same<TQJsonObject, QJsonObject>::value, QJsonObject>
		tryReadFromJsonValue(const QJsonValue & value);

		template <typename TQJsonArray>
		std::enable_if_t<std::is_same<TQJsonArray, QJsonArray>::value, QJsonArray>
		tryReadFromJsonValue(const QJsonValue & value);

		template <typename TQString>
		std::enable_if_t<std::is_same<TQString, QString>::value, QString>
		tryReadFromJsonValue(const QJsonValue & value);

		template<typename TMinMaxEnum>
		TMinMaxEnum tryReadMinMaxEnum(const QJsonValue & value);

		template <typename TValue>
		inline std::enable_if_t<std::is_integral_v<TValue>, QPair<QString, QJsonValue>>
		keyValue(const QString & key, const TValue & value);

		template <typename TValue>
		inline std::enable_if_t<std::is_enum_v<TValue>, QPair<QString, QJsonValue>>
		keyValue(const QString & key, const TValue & value);

		template <typename TValue>
		inline std::enable_if_t<
				!std::is_integral_v<TValue>
				&& !std::is_enum_v<TValue>
			,
			QPair<QString, QJsonValue>
		>
		keyValue(const QString & key, const TValue & value);



	}

	namespace Thread{
		void deleteLaterOnFinished(QThread * const & thread);
	}

	namespace Key{
		bool isArrow(const int & key);


	}

	namespace Sbox {
		void setMaxUnlimited(QSpinBox & sbox);
		void setMaxUnlimited(QDoubleSpinBox & sbox);

		void setMinUnlimited(QSpinBox & sbox);
		void setMinUnlimited(QDoubleSpinBox & sbox);
	}

	namespace Sbar {
		void showUndone(const QUndoCommand & command, QStatusBar & bar);
		void showRedone(const QUndoCommand & command, QStatusBar & bar);
	}

	namespace Object{

		class LocalSigBlock {
			DECL_NO_COPY_AND_MOVE(LocalSigBlock)
		public:
			LocalSigBlock(QObject * const & obj);
			~LocalSigBlock();

			bool oneCycle();
		private:
			signed char m_cycleVariable = 1;
			QObject * m_obj;

		};

		void connectMultipleSignals(QObject * const & sender,
									const std::initializer_list<const char *> & sigs,
									QObject * const & receiver,
									const char * const & slot);

		void connectMultipleSenders(const std::initializer_list<QObject *> & senders,
									const char * const & signal,
									QObject * const & receiver,
									const char * const & slot);

		void connectMultipleSignalsToSlots(
			QObject * const & sender,
			QObject * const & receiver,
			const std::initializer_list<const char *> & signalsAndSlots
		);

		void disconnectMultipleSignalsFromSlots(
			QObject * const & sender,
			QObject * const & receiver,
			const std::initializer_list<const char *> & signalsAndSlots
		);
	}
	namespace Label{
		void clearLabels(std::initializer_list<QLabel *> labels);
	}
	namespace StringListModel{
		void prependString(QStringListModel & model,
						   const QString & string,
						   const int & role = Qt::DisplayRole);

		bool removeString(QStringListModel & model, const QString & string);
		bool addUniqueString(QStringListModel & model, const QString & string);
	}

	namespace StackedWidget{
		QWidget * removeLowestWidget(QStackedWidget & stackedWidget);
	}

	namespace String{
		template <typename TStringContainer>
		void appendCommaSeparatedList(QString & string,
									  const TStringContainer & strings);

		template <typename TValue>
		void appendCommaSeparatedValue(QString & string,
									   size_t amount,
									   const TValue & value);
	}

	namespace Color{
		QJsonValue toJsonValue(const QColor & color);
		/* throws MessageException */
		QColor fromJsonValue(const QJsonValue & value);
	}

	namespace Widget{
		void deleteOnClosed(QWidget * widget);

		class LocalUpdatesBlock {
			DECL_NO_COPY_AND_MOVE(LocalUpdatesBlock)
		public:
			LocalUpdatesBlock(QWidget * const & widget);
			~LocalUpdatesBlock();

		private:
			QWidget * m_widget;
		};

		template <typename... TQWidget>
		void disableWidgets(TQWidget &&... widgets);

		template <typename... TQWidget>
		void enableWidgets(TQWidget &&... widgets);

	}

	namespace GridLayout{

		void repositionWidget(QGridLayout & gridLayout,
							  QWidget * const & widget,
							  const int & row,
							  const int & column,
							  const int & rowSpan = -1,
							  const int & columnSpan = -1,
							  const Qt::Alignment & alignment = Qt::Alignment());
	}

	namespace Meta {

		template <typename TObj, typename TMethod, typename... Args>
		void scheduleRoutine(TObj && obj, TMethod && method, Args&&... args);

		template <typename TObj, typename TMethod, typename... Args>
		void scheduleRoutine(TObj * obj, TMethod && method, Args&&... args);

		template <typename TFunctor>
		void scheduleRoutine(const TFunctor & func);

		template <typename TContainer>
		void deleteLaterPtrs(const TContainer & container);
	}

	namespace LineEdit {
		template <typename TString>
		void setValidator(QLineEdit * const & ledit, const TString & regexStr);
	}


	namespace TextDocument{
		int lastIndex(QTextDocument * const & document);
		bool hasIndex(QTextDocument * const & document, const int & index);

		bool canRemove(QTextDocument * const & document,
					   const int & from,
					   const int & amount);

		void removeText(QTextDocument * const & document,
						const int & from,
						const int & amount);

		void insertFragment(QTextDocument * const & document,
						   const QTextDocumentFragment & fragment,
						   const int & index);

		bool isSurroundedByFinalText(QTextDocument * const & document, const int & index);
		bool containsFinalText(QTextDocument * const & document,
							   const int & from,
							   const int & amount);

		QTextCursor cursorSelect(QTextDocument * const & document,
								 const int & firstPosition,
								 const int & lastPosition);

		QTextCursor cursorSelectAll(QTextDocument * const & document);

		int fragmentSize(const QTextDocumentFragment & fragment);

	}

	namespace Sqlite{
		// returns executedQuery

		class QueryInsertInto{
			QString m_queryString;
		public:
			QueryInsertInto(const char * const & tableName,
							const std::initializer_list<const char *> & columns,
							const size_t & totalValues = 1);

			template <typename TQVariantsContainer>
			QSqlQuery tryExecQuery(const QSqlDatabase & db, TQVariantsContainer && values);

			QSqlQuery tryExecQuery(const QSqlDatabase & db,
								   const std::initializer_list<QVariant> & values);
		};



		template<typename TQVariantsContainer>
		QSqlQuery QueryInsertInto::tryExecQuery(const QSqlDatabase & db,
								   TQVariantsContainer && values){
			QSqlQuery query(db);
			if (!query.prepare(m_queryString)){
				throw MessageException(
					QString("query prepare failure (%1)")
						.arg(query.lastError().text()).toStdString()
				);
			}
			for (const auto & value : values){
				query.addBindValue(value);
			}
			QTime time;
			time.start();
			if (!query.exec()){
				throw MessageException(
					QString("query exec failure (%1)")
						.arg(query.lastError().text()).toStdString()
				);
			}
			int elapsed { time.elapsed() };
			QLOG("query exec elapsed ms-> " << elapsed);
			return query;
		}



		QSqlQuery tryInsertInto(const QSqlDatabase & database,
						   const char * const & tableName,
						   const std::initializer_list<const char *> & columns,
						   const std::initializer_list<QVariant> & values);

		QSqlQuery trySelectFrom(const QSqlDatabase & database,
								const char * const & tableName,
								const std::initializer_list<const char *> & columns,
								const char * const & condition,
								const std::initializer_list<QVariant> & boundValues,
								const char * const & ordering = nullptr);

		QSqlQuery tryComputeValueFrom(const QSqlDatabase & database,
									  const char * const & tableName,
									  const char * const & column,
									  const char * const & function,
									  const char * const & condition = nullptr,
									  const std::initializer_list<QVariant> & boundValues = std::initializer_list<QVariant>());

		QSqlQuery tryDeleteFrom(const QSqlDatabase & database,
								const char * const & tableName,
								const char * const & condition = nullptr,
								const std::initializer_list<QVariant> & boundValues = std::initializer_list<QVariant>());

		QSqlQuery tryUpdate(const QSqlDatabase & database,
							const char * const & tableName,
							const std::initializer_list<const char *> & columns,
							const std::initializer_list<QVariant> & newValues,
							const char * const & condition,
							const std::initializer_list<QVariant> & conditionValues);



	}

}


// ----------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------

template <typename TFirst, typename TSecond>
inline QPair<TFirst, TSecond> Qext::makeQPair(TFirst && firstValue, TSecond && secondValue){
	return QPair<TFirst, TSecond>(std::move(firstValue), std::move(secondValue));
}


template <typename TString>
void Qext::LineEdit::setValidator(QLineEdit * const & ledit,
									const TString & regexStr){
	ledit->setValidator(
		new QRegularExpressionValidator(
			QRegularExpression(
				regexStr
			),
			ledit
		)
	);
}

template <typename TFunctor>
void Qext::Meta::scheduleRoutine(const TFunctor & func){
	QTimer::singleShot(0, func);
}

template <typename TContainer>
void Qext::Meta::deleteLaterPtrs(const TContainer & container){
	for (auto ptr : container){
		ptr->deleteLater();
	}
}

template <typename TObj, typename TMethod, typename... Args>
void Qext::Meta::scheduleRoutine(TObj && obj, TMethod && method, Args&&... args){
	QTimer::singleShot(
		0,
		[obj, method, args...]() mutable {
			(obj.*method)(args...);
		}
	);
}

template <typename TObj, typename TMethod, typename... Args>
void Qext::Meta::scheduleRoutine(TObj * obj, TMethod && method, Args&&... args){
	QTimer::singleShot(
		0,
		[obj, method, args...]() mutable {
			(obj->*method)(args...);
		}
	);
}


template <typename TEntity, typename TCont, typename TCompar>
QSet<TEntity> Qext::Set::fromStdPriorityQueue(std::priority_queue<TEntity, TCont, TCompar> & que,
								   int amount){
	QSet<TEntity> set;
	while (amount-- && !que.empty()){
		set << que.top();
		que.pop();
	}
	return set;
}

template <typename TEntity, typename TCont, typename TCompar>
QSet<TEntity> Qext::Set::fromStdPriorityQueue(std::priority_queue<TEntity, TCont, TCompar> & que){
	return Qext::Set::fromStdPriorityQueue(que, que.size());
}

template <typename TIntegral>
std::enable_if_t<std::is_integral_v<TIntegral>, TIntegral>
Qext::Json::tryReadFromJsonValue(const QJsonValue & value){
	if (!value.isDouble()){
		throw MessageException(
			TR("non integral json value for instance of type %1")
					.arg(typeid(TIntegral).name()).toStdString()
		);
	}
	int valInteger { value.toInt() };
	if (!std_ext::isInRangeOf<TIntegral>(valInteger)){
		throw MessageException(
			TR("value is out of range of type %1")
				.arg(typeid(TIntegral).name()).toStdString()
		);
	}
	return static_cast<TIntegral>(valInteger);
}

template <typename TReal>
std::enable_if_t<std::is_floating_point_v<TReal>, TReal>
Qext::Json::tryReadFromJsonValue(const QJsonValue & value){
	if (!value.isDouble()){
		throw MessageException(
			TR("non floating point number json value for instance of type %1")
					.arg(typeid(TReal).name()).toStdString()
		);
	}
	double valDouble { value.toDouble() };
	if (!std_ext::isInRangeOf<TReal>(valDouble)){
		throw MessageException(
			TR("value is out of range of type %1")
				.arg(typeid(TReal).name()).toStdString()
		);
	}
	return static_cast<TReal>(valDouble);
}



template <typename TValue>
inline std::enable_if_t<std::is_enum_v<TValue>, QPair<QString, QJsonValue>>
Qext::Json::keyValue(const QString & key, const TValue & value){
	return QPair<QString, QJsonValue>(key, static_cast<qint64>(value));
}


template <typename TValue>
inline std::enable_if_t<std::is_integral_v<TValue>, QPair<QString, QJsonValue>>
Qext::Json::keyValue(const QString & key, const TValue & value){
	return QPair<QString, QJsonValue>(key, static_cast<qint64>(value));
}

template <typename TValue>
inline std::enable_if_t<
		!std::is_integral_v<TValue>
		&& !std::is_enum_v<TValue>,
		QPair<QString, QJsonValue>
>
Qext::Json::keyValue(const QString & key, const TValue & value){
	return QPair<QString, QJsonValue>(key, QJsonValue(value));
}

template <typename TQJsonObject>
std::enable_if_t<std::is_same<TQJsonObject, QJsonObject>::value, QJsonObject>
Qext::Json::tryReadFromJsonValue(const QJsonValue & value){
	if (!value.isObject()){
		throw MessageException("json object type expected");
	} else {
		return value.toObject();
	}
}

template <typename TQJsonArray>
std::enable_if_t<std::is_same<TQJsonArray, QJsonArray>::value, QJsonArray>
Qext::Json::tryReadFromJsonValue(const QJsonValue & value){
	if (!value.isArray()){
		throw MessageException("json array type expected");
	} else {
		return value.toArray();
	}
}

template <typename TQString>
std::enable_if_t<std::is_same<TQString, QString>::value, QString>
Qext::Json::tryReadFromJsonValue(const QJsonValue & value){
	if (!value.isString()){
		throw MessageException("json string type expected");
	} else {
		return value.toString();
	}
}

template<typename TMinMaxEnum>
TMinMaxEnum Qext::Json::tryReadMinMaxEnum(const QJsonValue & value){
	if (!value.isDouble()){
		throw MessageException("enumeration value of non integral type");
	}
	int enumInt { value.toInt() };
	if (!std_ext::enumContains<TMinMaxEnum>(enumInt)){
		throw MessageException("enumeration value is out of range");
	}
	return static_cast<TMinMaxEnum>(enumInt);
}

template <typename TStringContainer>
void Qext::String::appendCommaSeparatedList(QString & string,
											const TStringContainer & strings){
	if (strings.begin() == strings.end()){
		return;
	}
	string.append(*strings.begin());
	for (auto iterator { strings.begin() + 1 }; iterator != strings.end(); ++iterator){
		string.append(',');
		string.append(*iterator);
	}
}

template <typename TValue>
void Qext::String::appendCommaSeparatedValue(QString & string,
							   size_t amount,
							   const TValue & value){
	if (amount--){
		string.append(value);
	}
	while (amount--){
		string.append(',');
		string.append(value);
	}
}



static inline void setWidgetsEnabled(const bool & enabled,
					   QWidget * const & widget){
	widget->setEnabled(enabled);
}


template <typename... TQWidget>
void setWidgetsEnabled(const bool & enabled,
					  QWidget * const & widget,
					  TQWidget &&... widgets){
	widget->setEnabled(enabled);
	setWidgetsEnabled(enabled, widgets...);
}

template <typename... TQWidget>
void Qext::Widget::disableWidgets(TQWidget &&... widgets){
	setWidgetsEnabled(false, widgets...);
}

template <typename... TQWidget>
void Qext::Widget::enableWidgets(TQWidget &&... widgets){
	setWidgetsEnabled(true, widgets...);
}

template<typename TContainer>
QJsonArray Qext::Json::toJsonArray(TContainer && container){
	QJsonArray jsonArray;
	for (const auto & entity : container){
		jsonArray << entity.toJsonValue();
	}
	return jsonArray;
}







#endif // QEXT
















