#ifndef RANGE_H
#define RANGE_H
#include <cmath>

#include "sharedlib_global.h"
#include "std_ext.h"


template <typename TNumeric = int>
struct SHAREDLIBSHARED_EXPORT Range {
	DECL_DEFAULT_COPYING(Range)

	TNumeric min;
	TNumeric max;

	Range(const TNumeric & min, const TNumeric & max) : min(min), max(max) {}

	bool isValid() const;
	bool includes(const TNumeric & suspect) const;
	bool excludes(const TNumeric & suspect) const;

	// both ranges must be valid!
	static Range<TNumeric> intersect(const Range<TNumeric> & bibba, const Range<TNumeric> & bobba);
};


template <typename T>
Range<T> Range<T>::intersect(const Range<T> & bibba, const Range<T> & bobba){
	return Range<T>(std::max(bibba.min, bobba.min), std::min(bibba.max, bobba.max));
}

template<typename T>
bool Range<T>::isValid() const {
	return min <= max;
}

template<typename T>
bool Range<T>::excludes(const T & suspect) const {
	return !includes(suspect);
}

template<typename T>
bool Range<T>::includes(const T & suspect) const{
	return min <= suspect && suspect <= max;
}


#endif // RANGE_H

