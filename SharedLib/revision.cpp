#include <utility>
#include <QUndoStack>

#include "revision.h"
#include "server_pad.h"
#include "qext.h"

namespace Revision{

AddChars::AddChars(QTextDocument * const & document,
				   const int & index,
				   const int & amount,
				   QTextDocumentFragment && fragment)
	: m_document(document),
	  m_index(index),
	  m_amount(amount),
	  m_fragment(std::move(fragment)) {}


void AddChars::undo(){
	Qext::TextDocument::removeText(m_document, m_index, m_amount);
}

void AddChars::redo(){
	Qext::TextDocument::insertFragment(m_document, m_fragment, m_index);
}
RemoveChars::RemoveChars(QTextDocument * const &document,
						 const int & index,
						 const int & amount)
	: m_document(document),
	  m_index(index),
	  m_amount(amount),
	  m_fragment(
		Qext::TextDocument::cursorSelect(
			document,
			index,
			index + amount
		).selection()
	) {}

void RemoveChars::undo(){
	Qext::TextDocument::insertFragment(m_document, m_fragment, m_index);
}

void RemoveChars::redo(){
	Qext::TextDocument::removeText(m_document, m_index, m_amount);
}

}
