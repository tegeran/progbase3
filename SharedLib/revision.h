﻿#ifndef PAD_COMMAND_H
#define PAD_COMMAND_H

#include <QTextDocumentFragment>
#include <QUndoCommand>
#include <QTextDocument>
#include <QSqlDatabase>
#include "sharedlib_global.h"
#include "netpacket.h"

namespace Server SHAREDLIBSHARED_EXPORT {
	class Pad;
}

namespace Revision SHAREDLIBSHARED_EXPORT {


	enum class Type {
		AddChars,
		RemoveChars,

		Min = AddChars,
		Max = RemoveChars
	};

	class AddChars : public QUndoCommand {
	public:
		AddChars(QTextDocument * const & document,
				 const int & index,
				 const int & amount,
				 QTextDocumentFragment && fragment);
		~AddChars() = default;

		void undo() override;
		void redo() override;
	private:
		QTextDocument * const m_document;
		const int m_index;
		int m_amount;
		const QTextDocumentFragment m_fragment;
	};


	class RemoveChars : public QUndoCommand {
	public:
		RemoveChars(QTextDocument * const & document,
					const int & index,
					const int & amount);
		~RemoveChars() = default;

		void undo() override;
		void redo() override;
	private:
		QTextDocument * const m_document;
		const int m_index;
		int m_amount;
		const QTextDocumentFragment m_fragment;
	};

}

#endif // PAD_COMMAND_H
