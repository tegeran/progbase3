﻿#include <QSqlQuery>

#include <utility>
#include <typeinfo>

#include "server_pad.h"
#include "qext.h"
#include "data.h"
#include "message_exception.h"
#include "revision.h"

namespace Server {

DbManager * Pad::s_revisionsSaveManager = nullptr;
DbManager * Pad::s_revisionsLoadManager = nullptr;

Pad::Pad(QString &&   name,
		 const id_t &    id,
		 const id_t & ownerId,
		 const bool & isPublic,
		 const QString & initialDocHtml,
		 QSet<id_t> &&   admins,
		 QSet<id_t> &&   authors)
		: Client::Pad  (std::move(name), id, isPublic),
		  m_ownerId    (ownerId),
		  m_admins     (std::move(admins )),
		  m_authors    (std::move(authors)){
	if (!initialDocHtml.isEmpty()){
		m_document.setHtml(initialDocHtml);
	}
	setupSaveManager();
	setupLoadManager();

	QLOG("created new pad " << m_name);
}

Pad::Pad(QString && name,
		 const id_t & id,
		 User * const & owner,
		 const bool & isPublic,
		 const QString & initialDocHtml,
		 QSet<id_t> && admins,
		 QSet<id_t> && authors)
	: Pad(std::move(name),
		  id,
		  owner->id(),
		  isPublic,
		  initialDocHtml,
		  std::move(admins),
		  std::move(authors))
{
	connectMemberSignals(owner);
	owner->setCurrentPad(this);
	m_connectedUsers.insert(owner->id(), owner);
}




Pad::~Pad(){
	emit querySaveCurrentDocument(m_id, m_document.toHtml());
	QLOG("deleted pad " << m_name);
}

Pad::Pad(Client::Pad && clientData,
		 User * const & owner,
		 const QString & initialDocHtml,
		 QSet<id_t> && admins,
		 QSet<id_t> && authors)
	: Pad(
		 std::move(clientData.name()),
		 clientData.id(),
		 owner,
		 clientData.isPublic(),
		 initialDocHtml,
		 std::move(admins),
		 std::move(authors)
	){}

Pad::Pad(Client::Pad && clientData,
		 const id_t & ownerId,
		 const QString & initialDocHtml,
		 QSet<id_t> && admins,
		 QSet<id_t> && authors)
	: Pad(
		 std::move(clientData.name()),
		 clientData.id(),
		 ownerId,
		 clientData.isPublic(),
		 initialDocHtml,
		 std::move(admins),
		 std::move(authors)
	){}



const QHash<id_t, User *> & Pad::connectedUsers() const{ return m_connectedUsers; }
QHash<id_t, User *> & Pad::connectedUsers()			   { return m_connectedUsers; }

	  QSet<id_t> &Pad::admins()	          { return m_admins;     }
const QSet<id_t> &Pad::admins()     const { return m_admins;     }
const QSet<id_t> &Pad::authors()    const { return m_authors;    }

QSet<id_t> & Pad::authors()	   { return m_authors;    }

QTextDocument * Pad::document(){
	return &m_document;
}

id_t Pad::ownerId() const{
	return m_ownerId;
}

void Pad::setOwnerId(const id_t & id){
	m_ownerId = id;
}

void Pad::sendToAllConnectedUsers(const Netpacket & netpacket){
	for (User * const & user : m_connectedUsers){
		user->sendNetpacket(netpacket);
	}
}

void Pad::sendToAllConnectedUsersExcept(User * const & exception, const Netpacket & netpacket){
	for (User * const & user : m_connectedUsers){
		if (user != exception){
			user->sendNetpacket(netpacket);
		}
	}
}

void Pad::setRestoredDocument(User * const & senderUser,
							  const QString & documentHtml){
	emit querySaveRemoveCharsRevision(
		m_id,
		++m_lastRevisionTimeId,
		0,
		Qext::TextDocument::lastIndex(&m_document)
	);
	emit querySaveAddCharsRevision(
		m_id,
		++m_lastRevisionTimeId,
		0,
		documentHtml
	);
	m_document.setHtml(documentHtml);
	senderUser->sendNetpacket<Data::String>(
		Netpacket::Answer_RestoredPad,
		documentHtml
	);
	sendToAllConnectedUsersExcept<Data::IdAndString>(
		senderUser,
		Netpacket::Update_RestoredPad,
		senderUser->id(),
		documentHtml
	);
}

int Pad::activeQueries(){
	return m_activeQueries;
}

void Pad::addActiveQuery(){
	++m_activeQueries;
}

void Pad::scheduleDeletion(){
	m_isScheduledForDeletion = true;
	if (!m_activeQueries){
		deleteLater();
	}
}

bool Pad::isScheduledForDeletion() const{
	return m_isScheduledForDeletion;
}

void Pad::removeActiveQuery(){
	--m_activeQueries;
	if (isScheduledForDeletion() && !m_activeQueries){
		deleteLater();
	}
}

User::Level Pad::userLevelFromId(const id_t & id){
	return id == m_ownerId
			? Client::User::Owner
			: (m_admins.contains(id)
				? Client::User::Admin
				: (m_authors.contains(id)
				   ? Client::User::Author
				   : (m_connectedUsers.contains(id)
					 ? Client::User::Guest
					 : Client::User::Unknown)));
}

void Pad::setRevisionsSaveManager(DbManager * const & dbManager){
	s_revisionsSaveManager = dbManager;
}

void Pad::setRevisionsLoadManager(DbManager * const & dbManager){
	s_revisionsLoadManager = dbManager;
}

void Pad::connectUserToPad(User * const & newbieUser){
	QSUPPOSE(!m_connectedUsers.contains(newbieUser->id()));
	connectMemberSignals(newbieUser);
	newbieUser->setCurrentPad(this);
	sendToAllConnectedUsers<Data::ClientUser>(
		Netpacket::Update_UserConnectedToPad,
		static_cast<Client::User>(*newbieUser)
	);
	m_connectedUsers.insert(newbieUser->id(), newbieUser);
	newbieUser->sendNetpacket<Data::PadEnterData>(
		Netpacket::Answer_ConnectedToPad,
		newbieUser->level(),
		m_id,
		m_connectedUsers,
		m_document
	);
}



void Pad::disconnectUserFromPad(User * const & disconnectedUser){
	bool wasConnectedToPad {
		m_connectedUsers.remove(disconnectedUser->id())
	};
	QSUPPOSE(wasConnectedToPad);
	disconnectMemberSignals(disconnectedUser);
	if (disconnectedUser->currentPad() == this){
		disconnectedUser->setCurrentPad(nullptr);
	}
	if (!m_connectedUsers.empty()){
		sendToAllConnectedUsers<Data::Id>(
			Netpacket::Update_UserLeftPad,
			disconnectedUser->id()
		);
	} else {
		emit lastUserLeft();
	}
}




void Pad::handle_user_disconnected(){
	User * disconnectedUser { QSENDER(User *) };
	if (disconnectedUser->currentPad() == this){
		disconnectedUser->setCurrentPad(nullptr);
	} else {
		QLOG_ERROR("logical AHTUNG, user left from pad it was not supposed to");
	}
	m_connectedUsers.remove(disconnectedUser->id());
	disconnectMemberSignals(disconnectedUser);
	sendToAllConnectedUsers<Data::Id>(
		Netpacket::Update_UserDisconnected,
		disconnectedUser->id()
	);
	if (m_connectedUsers.empty()){
		emit lastUserLeft();
	}
}


void Pad::handle_user_receivedNetpacket(const Netpacket & netpacketConst){
	User * senderUser { QSENDER(User *)	};
	Netpacket netpacket { std::move(const_cast<Netpacket &>(netpacketConst)) };
	try {
		switch (netpacket.type()){		
			case Netpacket::Request_RemoveCharacters:
			{ processRequest_RemoveCharacters(senderUser, netpacket);   return;}

			case Netpacket::Request_AddCharacters:
			{ processRequest_AddCharacters(senderUser, netpacket);      return;}

			case Netpacket::Request_DisconnectFromPad:
			{ processRequest_DisconnectFromPad(senderUser, netpacket);  return;}

			case Netpacket::Request_SendChatMessage:
			{ processRequest_SendChatMessage(senderUser, netpacket);    return;}

			case Netpacket::Request_AvailableRevisions:
			{ processRequest_AvailableRevisions(senderUser, netpacket); return; }

			case Netpacket::Request_RevisionsRange:
			{ processRequest_RevisionsRange(senderUser, netpacket);		return; }

			case Netpacket::Request_FirstDocRevision:
			{ processRequest_FirstDocVersion(senderUser, netpacket);    return; }

			case Netpacket::Request_RestorePad:
			{ processRequest_RestorePad(senderUser, netpacket);         return; }

			default: return;
		}
	} catch(const MessageException & exception){
		senderUser->sendErrorReport(QString(exception.what()));
	}
}

void Pad::handle_user_changedColor(const QColor & color){
	User * senderUser {  QSENDER(User *) };
	sendToAllConnectedUsersExcept<Data::IdAndColor>(
		senderUser,
		Netpacket::Update_UserChangedColor,
		senderUser->id(),
		color
				);
}

void Pad::handle_user_changedLevel(const User::Level & previousLevel, User * const & initiator){
	User * senderUser{ QSENDER(User *) };
	QSUPPOSE(m_connectedUsers.contains(senderUser->id()));
	switch (previousLevel){
		case Client::User::Admin:{
			QSUPPOSE(m_admins.contains(senderUser->id()));
			m_admins.remove(senderUser->id());
			break;
		}
		case Client::User::Author:{
			QSUPPOSE(m_authors.contains(senderUser->id()));
			m_authors.remove(senderUser->id());
			break;
		}
		default: {
			QSUPPOSE(previousLevel == Client::User::Guest);
		}
	}
	switch(senderUser->level()){
		case Client::User::Admin:{
			m_admins.insert(senderUser->id());
			break;
		}
		case Client::User::Author:{
			m_authors.insert(senderUser->id());
			break;
		}
		default:{
			QSUPPOSE(senderUser->level() == Client::User::Guest);
		}
	}
	sendToAllConnectedUsers<Data::SenderIdAnd<Data::IdAndLevel>>(
		Netpacket::Update_ChangedUserLevel,
		initiator->id(),
		senderUser->id(),
		senderUser->level()
	);
}

void Pad::processRequest_RemoveCharacters(User * const & senderUser, Netpacket & netpacket){
	RETURN_IF(activeQueries());

	Data::IndexAndAmount dataPacket{
		netpacket.dataPacket<Data::IndexAndAmount>()
	};
	if (!senderUser->canRemoveChars(dataPacket.index, dataPacket.amount)){
		senderUser->sendErrorReport(
			Netpacket::Answer_RemovingCharsError,
			"user can't remove requested text"
		);
		return;
	}
	emit querySaveRemoveCharsRevision(
		m_id,
		++m_lastRevisionTimeId,
		dataPacket.index,
		dataPacket.amount
	);
	Qext::TextDocument::removeText(&m_document, dataPacket.index, dataPacket.amount);

	netpacket.setType(Netpacket::Answer_RemovedCharacters);
	senderUser->sendNetpacket(netpacket);

	netpacket.setType(Netpacket::Update_RemovedCharacters);
	sendToAllConnectedUsersExcept(senderUser, netpacket);
}

void Pad::processRequest_AddCharacters(User * const & senderUser, Netpacket & netpacket){
	RETURN_IF(activeQueries());

	Data::IndexAndTextFragment dataPacket {
		netpacket.dataPacket<Data::IndexAndTextFragment>()
	};
	if (!senderUser->canAddFragment(dataPacket.fragment, dataPacket.index)){
		senderUser->sendErrorReport(
			Netpacket::Answer_AddingCharsError,
			"user can't add request text"
		);
		return;
	}

	emit querySaveAddCharsRevision(
		m_id,
		++m_lastRevisionTimeId,
		dataPacket.index,
		dataPacket.fragment
	);

	Qext::TextDocument::insertFragment(&m_document, dataPacket.fragment, dataPacket.index);
	netpacket.setType(Netpacket::Answer_AddedCharacters);
	senderUser->sendNetpacket(netpacket);

	netpacket.setType(Netpacket::Update_AddedCharacters);
	sendToAllConnectedUsersExcept(senderUser, netpacket);
}

void Pad::processRequest_SendChatMessage(User * const & senderUser, Netpacket & netpacket){
	Data::String dataPacket{
		netpacket.dataPacket<Data::String>()
	};
	sendToAllConnectedUsersExcept<Data::IdAndString>(
		senderUser,
		Netpacket::Update_NewChatMessage,
		senderUser->id(),
		dataPacket.string
	);
	netpacket.setType(Netpacket::Answer_DeliveredChatMessage);
	senderUser->sendNetpacket(netpacket);
}

void Pad::processRequest_DisconnectFromPad(User * const & senderUser, const Netpacket &){
	disconnectUserFromPad(senderUser);
	senderUser->sendNetpacketSignal(Netpacket::Answer_DisconnectedFromPad);
}

void Pad::processRequest_AvailableRevisions(User * const & senderUser,
											const Netpacket &){
	senderUser->addActiveQuery();
	emit queryAvailableRevisions(senderUser, m_id);
}

void Pad::processRequest_RevisionsRange(User * const & senderUser,
										const Netpacket & netpacket){
	Data::TimeIdRange dataPacket {
		netpacket.dataPacket<Data::TimeIdRange>()
	};
	if (dataPacket.first > dataPacket.last
	  || dataPacket.last > m_lastRevisionTimeId){
		senderUser->sendErrorReport("invalid revisions range required");
		return;
	}
	senderUser->addActiveQuery();
	emit queryRevisionsRange(senderUser, m_id, dataPacket.first, dataPacket.last);
}

void Pad::processRequest_FirstDocVersion(User * const & senderUser,
										 const Netpacket & netpacket){
	Data::TimeIdRange dataPacket{
		netpacket.dataPacket<Data::TimeIdRange>()
	};
	if (dataPacket.first > dataPacket.last || dataPacket.last > m_lastRevisionTimeId){
		senderUser->sendErrorReport("invalid first version revision range required");
		return;
	}
	senderUser->addActiveQuery();
	emit queryFirstDocVersionRevisions(
		senderUser,
		m_id,
		dataPacket.first,
		dataPacket.last
	);
}

void Pad::processRequest_RestorePad(User * const & senderUser,
									const Netpacket & netpacket){
	if (!senderUser->canRestorePad()){
		senderUser->sendErrorReport("user doesn't have enough rights to restore pad");
		return;
	}
	Data::Integer<timeid_t> restorationPoint{
		netpacket.dataPacket<Data::Integer<timeid_t>>()
	};
	if (restorationPoint.integer > m_lastRevisionTimeId){
		senderUser->sendErrorReport("no such restoration point was found");
		return;
	}
	senderUser->addActiveQuery();
	addActiveQuery();
	emit queryRestorePad(
		this,
		senderUser,
		m_id,
		restorationPoint.integer
	);
}



void Pad::connectMemberSignals(User * const & member){
	Qext::Object::connectMultipleSignalsToSlots(
		member,
		this,
		{
			SIGNAL			(changedLevel(Client::User::Level,User*)),
			SLOT(handle_user_changedLevel(Client::User::Level,User*)),

			SIGNAL			(changedColor(QColor)),
			SLOT(handle_user_changedColor(QColor)),

			SIGNAL			(receivedNetpacket(Netpacket)),
			SLOT(handle_user_receivedNetpacket(Netpacket)),

			SIGNAL			(disconnected()),
			SLOT(handle_user_disconnected()),

		}
	);
}

void Pad::disconnectMemberSignals(User * const & member){
	disconnect(
		member,
		nullptr,
		this,
		nullptr
	);
}

void Pad::setupSaveManager(){
	Qext::Object::connectMultipleSignalsToSlots(
		this,
		s_revisionsSaveManager,
		{
			SIGNAL(querySaveAddCharsRevision(id_t,timeid_t,int,QString)),
			SLOT(querySaveAddCharsRevision(id_t,timeid_t,int,QString)),

			SIGNAL(querySaveRemoveCharsRevision(id_t,timeid_t,int,int)),
			SLOT  (querySaveRemoveCharsRevision(id_t,timeid_t,int,int)),

			SIGNAL(querySaveAddCharsRevision(id_t,timeid_t,int,QTextDocumentFragment)),
			SLOT  (querySaveAddCharsRevision(id_t,timeid_t,int,QTextDocumentFragment)),

			SIGNAL(querySaveCurrentDocument(id_t,QString)),
			SLOT  (querySaveCurrentDocument(id_t,QString))
		}
	);
}

void Pad::setupLoadManager(){
	Qext::Object::connectMultipleSignalsToSlots(
		this,
		s_revisionsLoadManager,
		{

			SIGNAL(queryRestorePad(Server::Pad*,Server::User*,id_t,timeid_t)),
			SLOT  (queryRestorePad(Server::Pad*,Server::User*,id_t,timeid_t)),

			SIGNAL(queryFirstDocVersionRevisions(Server::User*,id_t,timeid_t,timeid_t)),
			SLOT  (queryFirstDocVersionRevisions(Server::User*,id_t,timeid_t,timeid_t)),

			SIGNAL(queryRevisionsRange(Server::User*,id_t,timeid_t,timeid_t)),
			SLOT  (queryRevisionsRange(Server::User*,id_t,timeid_t,timeid_t)),

			SIGNAL(queryAvailableRevisions(Server::User*,id_t)),
			SLOT  (queryAvailableRevisions(Server::User*,id_t))
		}
	);
}

void Pad::setLastRevisionTimeId(const timeid_t & timeId){
	m_lastRevisionTimeId = timeId;
}


}






