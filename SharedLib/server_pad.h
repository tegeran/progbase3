#ifndef SERVER_PAD_H
#define SERVER_PAD_H

#include <QSet>
#include <QObject>
#include <QTextDocument>
#include <QSqlDatabase>
#include <QUndoStack>

#include "sharedlib_global.h"
#include "tcpserver.h"
#include "tcpsocket.h"
#include "client_pad.h"
#include "server_user.h"
#include "timer.h"
#include "dbmanager.h"

namespace Revision SHAREDLIBSHARED_EXPORT{
	class Command;
	class AddChars;
	class RemoveChars;
	enum class Type;
}

namespace Server SHAREDLIBSHARED_EXPORT{

class Pad : public QObject, public Client::Pad {
	Q_OBJECT
	friend class Revision::AddChars;
	friend class Revision::RemoveChars;
public:
	explicit Pad(   QString &&   name,
				 const id_t &    id          = nullid,
				 const id_t &    ownerId     = nullid,
				 const bool &    isPublic    = true,
				 const QString & initialDocHtml = QString(),
				 QSet<id_t> &&   admins      = QSet<id_t>(),
				 QSet<id_t> &&   authors     = QSet<id_t>());

	explicit Pad(   QString &&   name,
				 const id_t &    id,
				 ::Server::User * const & owner,
				 const bool &    isPublic    = true,
				 const QString & initialDocHtml = QString(),
				 QSet<id_t> &&   admins      = QSet<id_t>(),
				 QSet<id_t> &&   authors     = QSet<id_t>());


	~Pad();
	void setLastRevisionTimeId(const timeid_t & timeId);

	explicit Pad(
		Client::Pad && clientData,
		User * const &  owner,
		const QString & initialDocHtml = QString(),
		QSet<id_t> &&   admins      = QSet<id_t>(),
		QSet<id_t> &&   authors     = QSet<id_t>()
	);

	explicit Pad(
		Client::Pad && clientData,
		const id_t &    ownerId,
		const QString & initialDocHtml = QString(),
		QSet<id_t> &&   admins      = QSet<id_t>(),
		QSet<id_t> &&   authors     = QSet<id_t>()
	);

	const QHash<id_t, User *> & connectedUsers() const;
	QHash<id_t, User *> & connectedUsers();

	const QSet<id_t> & admins() const;
	const QSet<id_t> & authors() const;

	QSet<id_t> & admins();
	QSet<id_t> & authors();

	QTextDocument * document();

	id_t ownerId() const;
	void setOwnerId(const id_t & id);

	void sendErrorReport(User * const & dest, const QString & report);

	template <typename TDataPacket, typename... TPacketArgs>
	void sendToAllConnectedUsers(const Netpacket::Type & type,
								 TPacketArgs &&... args);

	template <typename TDataPacket, typename... TPacketArgs>
	void sendToAllConnectedUsersExcept(User * const & exception,
									   const Netpacket::Type & type,
									   TPacketArgs &&... args);


	void sendToAllConnectedUsers(const Netpacket & netpacket);
	void sendToAllConnectedUsersExcept(User * const & exception, const Netpacket & netpacket);

	void setRestoredDocument(
		Server::User * const & senderUser,
		const QString & documentHtml
	);

	int activeQueries();

	void addActiveQuery();
	bool isScheduledForDeletion() const;
	void removeActiveQuery();

	Client::User::Level userLevelFromId(const id_t & id);

	static void setRevisionsSaveManager(DbManager * const & dbManager);
	static void setRevisionsLoadManager(DbManager * const & dbManager);

signals:
	void lastUserLeft();

	void queryRestorePad(
		Server::Pad * const & pad,
		Server::User * const & senderUser,
		const id_t & padId,
		const timeid_t & restorationPoint
	);

	void queryRevisionsRange(Server::User * const & senderUser,
							 const id_t & padId,
							 const timeid_t & first,
							 const timeid_t & last);

	void queryFirstDocVersionRevisions(Server::User * const & senderUser,
									   const id_t & padId,
									   const timeid_t & first,
									   const timeid_t & last);


	void queryAvailableRevisions(Server::User * const & senderUser,
								 const id_t & padId);

	void querySaveCurrentDocument(const id_t & padId,
								  const QString & documentHtml);

	void querySaveAddCharsRevision(const id_t & padId,
								   const timeid_t & revisionTimeId,
								   const int & index,
								   const QTextDocumentFragment & fragment);

	void querySaveAddCharsRevision(const id_t & padId,
								   const timeid_t & revisionTimeId,
								   const int & index,
								   const QString & fragmentHtml);

	void querySaveRemoveCharsRevision(const id_t & padId,
									  const timeid_t & revisionTimeId,
									  const int & index,
									  const int & amount);

public slots:
	void connectUserToPad(User * const & user);
	void disconnectUserFromPad(User * const & user);
	void scheduleDeletion();

private slots:
	void handle_user_disconnected();
	void handle_user_receivedNetpacket(const Netpacket & netpacket);
	void handle_user_changedColor(const QColor & color);
	void handle_user_changedLevel(
		const Client::User::Level & previousLevel,
		User * const & initiator
	);


private:
	/* QString   m_name     */
	/*   id_t    m_id       */    // inherited members
	/*   bool    m_isPublic */
		 id_t    m_ownerId;
	QSet<id_t>   m_admins;
	QSet<id_t>   m_authors;
	QHash<id_t, User *> m_connectedUsers;
	QTextDocument m_document;
	timeid_t m_lastRevisionTimeId = nulltimeid;


	int m_activeQueries = 0;
	bool m_isScheduledForDeletion = false;

	static DbManager * s_revisionsSaveManager;
	static DbManager * s_revisionsLoadManager;

	void processRequest_RemoveCharacters(User * const & senderUser,
										 Netpacket & netpacket);
	void processRequest_AddCharacters(User * const & senderUser,
									  Netpacket & netpacket);


	void processRequest_SendChatMessage(User * const & senderUser,
										Netpacket & netpacket);

	void processRequest_DisconnectFromPad(User * const & senderUser,
										  const Netpacket &);

	void processRequest_AvailableRevisions(User * const & senderUser,
										   const Netpacket &);

	void processRequest_RevisionsRange(User * const & senderUser,
									   const Netpacket & netpacket);

	void processRequest_FirstDocVersion(User * const & senderUser,
										const Netpacket & netpacket);

	void processRequest_RestorePad(User * const & senderUser,
								   const Netpacket & netpacket);

	void connectMemberSignals(User * const & member);
	void disconnectMemberSignals(User * const & member);

	void tryAddNewRevisionIntoDatabase(const Revision::Type & type,
									   const id_t & revision_id);


	void setupSaveManager();
	void setupLoadManager();
};


template <typename TDataPacket, typename... TPacketArgs>
void Pad::sendToAllConnectedUsers(const Netpacket::Type & type,
								  TPacketArgs &&... args){
	Netpacket netpacket {
		type,
		TDataPacket::toJsonValue(std::forward<TPacketArgs>(args)...)
	};
	for(User * const & user : m_connectedUsers){
		user->sendNetpacket(netpacket);
	}
}

template <typename TDataPacket, typename... TPacketArgs>
void Pad::sendToAllConnectedUsersExcept(User * const & exception,
										const Netpacket::Type & type,
										TPacketArgs &&... args)
{
	Netpacket netpacket {
		type,
		TDataPacket::toJsonValue(std::forward<TPacketArgs>(args)...)
	};
	for(User * const & user : m_connectedUsers){
		if (user != exception){
			user->sendNetpacket(netpacket);
		}
	}
}

}



#endif // SERVER_PAD_H
