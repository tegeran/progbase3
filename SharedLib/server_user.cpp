#include <QTextFragment>

#include <utility>

#include "server_user.h"
#include "qnet.h"
#include "data.h"
#include "qext.h"
#include "server_pad.h"

namespace Server {

User::User(Pad * const &currentPad,
		   QString && name,
		   const id_t & id,
		   QColor && color)
	: Client::User(std::move(name), id, std::move(color)),
	  m_currentPad(currentPad)
{
	setupUserSocket();
}

User::User(Pad * const & currentPad, Client::User && user)
	: Client::User(std::move(user)),
	  m_currentPad(currentPad)
{
	setupUserSocket();
}


User::~User() {}

Pad * User::currentPad()  { return m_currentPad; }
const Pad *User::currentPad() const    { return m_currentPad; }
void User::setCurrentPad(Pad * const & pad){
	m_currentPad = pad;
	if(!pad){
		m_level = Unknown;
		return;
	}
	if (isOwnerOfPad(pad)){
		promoteToOwner();
	} else if (pad->admins().contains(m_id)){
		promoteToAdmin();
	} else if (pad->authors().contains(m_id)){
		promoteToAuthor();
	} else {
		promoteToGuest();
	}
}

bool User::isConnectedToPad() const {
	return m_currentPad;
}

bool User::hasAccessToPad(const Server::Pad * const & pad) const{
	QSUPPOSE(pad);
	return pad->isPublic()
			|| isOwnerOfPad(pad)
			|| pad->admins().contains(m_id)
			|| pad->authors().contains(m_id);
}

bool User::isOwnerOfPad(const Pad * const & pad) const{

	QSUPPOSE(pad);
	return pad->ownerId() == m_id;
}

void User::sendErrorReport(const QString & reason){
	if (m_isScheduledForDeletion){
		return;
	}

	QSUPPOSE(TcpSocket::canSendData());
	TcpSocket::sendNetpacket<Data::String>(Netpacket::Answer_Error, reason);
}

void User::sendErrorReport(const Netpacket::Type & errorType, const QString & reason){
	if (m_isScheduledForDeletion){
		return;
	}

	TcpSocket::sendNetpacket<Data::String>(errorType, reason);
}

void User::setClientData(Client::User && data){
	Client::User::operator=(std::move(data));
}


void User::setColor(QColor && color){
	if (m_isScheduledForDeletion){
		return;
	}
	Client::User::setColor(std::move(color));
	emit changedColor(m_color);
}

void User::addActiveQuery(){
	++m_activeQueries;
	QSUPPOSE(!m_isScheduledForDeletion);
}

void User::removeActiveQuery(){
	--m_activeQueries;
	QSUPPOSE(m_activeQueries >= 0);
	if (m_isScheduledForDeletion && !m_activeQueries){
		deleteLater();
	}
}

void User::scheduleDeletion(){
	TcpSocket::close();
	m_isScheduledForDeletion = true;
	if (!m_activeQueries){
		deleteLater();
	}
}

void User::setupUserSocket(){
	TcpSocket::setSocketOption(QAbstractSocket::LowDelayOption, true);
}

bool User::fragmentContainsOnlyFinalOrUserColor(const QTextDocumentFragment & fragment){
	QTextDocument document;
	QTextCursor cursor { &document };
	cursor.insertFragment(fragment);
	foreach_text_fragment(textFragment, &document){

		const QColor & color { textFragment.charFormat().background().color() };
		if (color != Client::User::finalColor() && color != m_color){
			return false;
		}
	}
	return true;
}

bool User::fragmentContainsOnlyUserColor(const QTextDocumentFragment & fragment){
	QTextDocument document;
	QTextCursor cursor { &document };
	cursor.insertFragment(fragment);
	foreach_text_fragment(textFragment, &document){

		if (textFragment.charFormat().background().color() != m_color){
			return false;
		}
	}
	return true;
}

bool User::canRemoveChars(const int & index, const int & amount){
	QSUPPOSE(m_currentPad);
	return Qext::TextDocument::canRemove(
				m_currentPad->document(),
				index,
				amount)
			&&
			(canModifyFinalText()
			 || !Qext::TextDocument::containsFinalText(
					m_currentPad->document(),
					index,
					amount));
}

bool User::canAddFragment(const QTextDocumentFragment & fragment, const int & index){
	QSUPPOSE(m_currentPad);
	if (!Qext::TextDocument::hasIndex(m_currentPad->document(), index)){
		return false;
	}
	if (canModifyFinalText()){
		return fragmentContainsOnlyFinalOrUserColor(fragment);
	} else {
		return !Qext::TextDocument::isSurroundedByFinalText(m_currentPad->document(), index)
				&& fragmentContainsOnlyUserColor(fragment);
	}
}

bool User::isScheduledForDeletion() const{
	return m_isScheduledForDeletion;
}

void User::setLevel(const Client::User::Level & previousLevel, User * const & initiator){
	if (m_level != previousLevel){
		Client::User::Level keeper {
			m_level
		};
		Client::User::setLevel(previousLevel);
		emit changedLevel(keeper, initiator);
	}
}


}
