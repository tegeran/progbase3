#ifndef SERVER_USER_H
#define SERVER_USER_H

#include <QString>
#include <QColor>
#include <QObject>
#include <QTextDocumentFragment>

#include <utility>

#include "sharedlib_global.h"
#include "client_user.h"
#include "std_ext.h"
#include "tcpsocket.h"
#include "netpacket.h"
#include "error.h"

namespace Server SHAREDLIBSHARED_EXPORT{

	class Pad; /* forward declaration in order to avoid cyclic inclusion */

class User : public TcpSocket, public Client::User {
	DECL_DEFAULT_MOVE_ONLY(User)
	Q_OBJECT
public:
	explicit User(Pad * const & currentPad = nullptr,
				  QString && name = "no_name",
				  const id_t & id = nullid,
				  QColor && color = Qt::blue);

	User(Pad * const & currentPad,
		 Client::User && user);

	~User();

	const Pad * currentPad() const;
	Pad * currentPad();
	void setCurrentPad(Pad * const & pad);

	bool isConnectedToPad() const;
	bool hasAccessToPad(const Server::Pad * const & pad) const;
	bool isOwnerOfPad(const Server::Pad * const & pad) const;

	void sendErrorReport(const QString & reason);
	void sendErrorReport(const Netpacket::Type & errorType, const QString & reason);

	void setClientData(Client::User && data);

	bool canRemoveChars(const int & index, const int & amount);
	bool canAddFragment(const QTextDocumentFragment & fragment, const int & index);
	bool isScheduledForDeletion() const;

	void setLevel(const Level & previousLevel, User * const & initiator);
	void setColor(QColor && color) override;
	void addActiveQuery();
	void removeActiveQuery();

signals:
	void changedColor(const QColor & newColor);
	void changedLevel(const Client::User::Level & level, User * const & initiator);
public slots:

	void scheduleDeletion();
private:
	Pad * m_currentPad;

	int m_activeQueries = 0;
	bool m_isScheduledForDeletion = false;

	void setupUserSocket();

	bool fragmentContainsOnlyFinalOrUserColor(const QTextDocumentFragment & fragment);
	bool fragmentContainsOnlyUserColor(const QTextDocumentFragment & fragment);


};

}

#endif // SERVER_USER_H
