#ifndef SHAREDLIB_GLOBAL_H
#define SHAREDLIB_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(SHAREDLIB_LIBRARY)
#  define SHAREDLIBSHARED_EXPORT Q_DECL_EXPORT
#else
#  define SHAREDLIBSHARED_EXPORT Q_DECL_IMPORT
#endif


typedef unsigned int id_t;
typedef unsigned long long timeid_t;

#define nullid (static_cast<id_t>(0))
#define nulltimeid (static_cast<timeid_t>(0))

#endif // SHAREDLIB_GLOBAL_H
