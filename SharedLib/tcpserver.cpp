#include "tcpserver.h"

Server::User * TcpServer::nextPendingUser(){
	return static_cast<Server::User *>(nextPendingConnection());
}

void TcpServer::incomingConnection(qintptr socketDescriptor){
	Server::User * newUser{ new Server::User };
	if (!newUser->setSocketDescriptor(socketDescriptor)){
		emit acceptError(newUser->error());
		delete newUser;
	} else {
		addPendingConnection(static_cast<QTcpSocket *>(newUser));
	}
}
