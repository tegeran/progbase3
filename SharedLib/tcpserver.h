#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QTcpServer>

#include "sharedlib_global.h"
#include "tcpsocket.h"
#include "server_user.h"

class SHAREDLIBSHARED_EXPORT TcpServer : public QTcpServer{
	Q_OBJECT
public:
	using QTcpServer::QTcpServer;

	Server::User * nextPendingUser();

	// QTcpServer interface
protected:
	void incomingConnection(qintptr socketDescriptor) override;
};

#endif // TCPSERVER_H
