#include <QHostAddress>

#include <algorithm>

#include "tcpsocket.h"
#include "data.h"


TcpSocket::TcpSocket(QObject * const & parent)
	: QTcpSocket(parent)
{
	connect(
		this,
		SIGNAL(readyRead()),
		this,
		SLOT(handle_super_readyRead())
	);
	connect(
		this,
		&QAbstractSocket::stateChanged,
		[this](const QAbstractSocket::SocketState & state){
			QLOG("socket state changed to " << state);
		}
	);
	m_responseTimer.setSingleShot(true);
	connect(
		&m_responseTimer,
		SIGNAL(timeout()),
		this,
		SLOT(handle_responseTimer_expired())
	);
	QLOG("socket created");
}

TcpSocket::~TcpSocket(){
	QLOG("socket destroyed");
}

bool TcpSocket::isConnected() const{
	return QTcpSocket::state() == QAbstractSocket::ConnectedState;
}

bool TcpSocket::isUnconnected() const{
	return QTcpSocket::state() == QAbstractSocket::UnconnectedState;
}

bool TcpSocket::isTryingToConnect() const{
	switch (QTcpSocket::state()){
		case QAbstractSocket::ConnectingState:
		case QAbstractSocket::HostLookupState:{
			return true;
		}
		default: return false;
	}
}

bool TcpSocket::canSendData() const{
	return Qnet::canWriteTo(static_cast<const QTcpSocket &>(*this));
}

void TcpSocket::sendNetpacket(const Netpacket & netpacket){
	if (Qnet::canWriteTo(*this)){
		netpacket.send(*this);
	}
}

void TcpSocket::sendNetpacketSignal(const Netpacket::Type & signal){
	sendNetpacket<Data::TypeOnly>(signal);
}

void TcpSocket::expectFastRespose(const std::chrono::milliseconds & msecTimeout){
	QTcpSocket::flush();
	++m_expectingResponses;
	if (!isExpectingResponse()){
		m_responseTimer.start(msecTimeout);
	};
}

const unsigned int &TcpSocket::expectingResponsesAmount() const{
	return m_expectingResponses;
}

bool TcpSocket::isExpectingResponse() const{
	return m_responseTimer.isActive();
}

bool TcpSocket::hasBufferedData() const{
	return m_buffer.size();
}

void TcpSocket::ungetData(const char * data, quint64 size){
	data += size - 1;
	while (size--){
		QTcpSocket::ungetChar(*data--);
	}
}

void TcpSocket::discardPendingData(){
	static_cast<void>(QTcpSocket::readAll());
}


void TcpSocket::handle_super_readyRead(){
	m_responseTimer.interruptExpiration();
	while (QTcpSocket::bytesAvailable() && mustReadMoreData()){
		QLOG("awaiting -> " << m_recvSize << " buffered -> " << m_buffer.size());

		m_buffer.append(QTcpSocket::read(m_recvSize - m_buffer.size()));
		if (hasBufferedUltimatePacket()){
			produceNetPacket();
			resetBuffer();
			std_ext::tryDecrement(m_expectingResponses);

		} else {
			QSUPPOSE(std_ext::isLess(m_buffer.size(), m_recvSize));
		}
	}
	if (m_expectingResponses){
		m_responseTimer.start();
	}
}

void TcpSocket::handle_responseTimer_expired(){
	QTcpSocket::abort();
	m_expectingResponses = 0;
	emit QTcpSocket::error(QAbstractSocket::SocketTimeoutError);
}

bool TcpSocket::hasBufferedUltimatePacket(){
	return m_buffer.size() == static_cast<qint64>(m_recvSize);
}

bool TcpSocket::mustReadMoreData(){
	if (!m_recvSize){
		if (std_ext::isLess(bytesAvailable(), sizeof(m_recvSize))){
			QLOG_ERROR(
				"received to little bytes to get recvSize, available -> "
				<< bytesAvailable()
			);
			return false;
		};
		QTcpSocket::read(
			reinterpret_cast<char *>(&m_recvSize),
			sizeof(m_recvSize)
		);
		QLOG("awaiting new message with size of " << m_recvSize << " bytes");
	}
	return m_recvSize;
}

void TcpSocket::produceNetPacket(){
	Netpacket packet { m_buffer };
	if (packet.isValid()){
		emit receivedNetpacket(packet);
	} else {
		QLOG_ERROR("received invalid netpacket");
	}
}

void TcpSocket::resetBuffer(){
	m_recvSize = 0;
	m_buffer.clear();
}

qint64 TcpSocket::writeData(const char * data, qint64 len){
	quint32 uint32Len { static_cast<quint32>(len) };
	return QTcpSocket::writeData(reinterpret_cast<const char *>(&uint32Len), sizeof(quint32))
		 + QTcpSocket::writeData(data, len);
}
