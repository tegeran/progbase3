#ifndef TCPSOCKET_H
#define TCPSOCKET_H

#include <QTcpSocket>
#include <QByteArray>

#include <utility>
#include <chrono>

#include "sharedlib_global.h"
#include "netpacket.h"
#include "qnet.h"
#include "std_ext.h"
#include "timer.h"

class SHAREDLIBSHARED_EXPORT TcpSocket : public QTcpSocket{
	Q_OBJECT
public:
	explicit TcpSocket(QObject * const & parent = nullptr);
	virtual ~TcpSocket();
	bool isConnected() const;
	bool isUnconnected() const;
	bool isTryingToConnect() const;

	bool canSendData() const;

	template <typename... TData>
	void sendRawData(TData &&... data);


	template <typename TDataPacket, typename... TPacketArgs>
	void sendNetpacket(const Netpacket::Type & type, TPacketArgs &&... args);

	virtual void sendNetpacket(const Netpacket & netpacket);
	virtual void sendNetpacketSignal(const Netpacket::Type & signal);


	/* aborts connection and emits error(QAbstractSocket::SocketTimeoutError) */
	/* if couldn't receive response in msecTimeout                            */
	void expectFastRespose(const std::chrono::milliseconds& msecTimeout);
	const unsigned int & expectingResponsesAmount() const;
	bool isExpectingResponse() const;
	bool hasBufferedData() const;

	void ungetData(const char * data, quint64 size);
	void discardPendingData();

signals:
	void receivedNetpacket(Netpacket packet);
private slots:	
	void handle_super_readyRead(); /* super stands for superclass */
	void handle_responseTimer_expired();
private:
	/*mutable*/ QByteArray m_buffer;
	/*mutable*/ quint32 m_recvSize = 0;
	/*mutable*/ Timer m_responseTimer;
	/*mutable*/ unsigned int m_expectingResponses = 0;

	bool hasBufferedUltimatePacket();
	bool mustReadMoreData();
	void produceNetPacket();
	void resetBuffer();


	// QIODevice interface
protected:
	qint64 writeData(const char * data, qint64 len) override;
};


template<typename... TData>
void TcpSocket::sendRawData(TData &&... data){
	Qnet::send(*this, std::forward<TData>(data)...);
}


template <typename TDataPacket, typename... TPacketArgs>
void TcpSocket::sendNetpacket(const Netpacket::Type & type, TPacketArgs &&... args){
	if (Qnet::canWriteTo(*this)){
		Netpacket(type, TDataPacket::toJsonValue(std::forward<TPacketArgs>(args)...)).send(*this);
	}
}


#endif // TCPSOCKET_H
