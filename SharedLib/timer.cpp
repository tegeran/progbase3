#include "timer.h"

Timer::Timer(QObject * const & parent)
    : QTimer(parent)
{
    connect(
        this,
        SIGNAL(timeout()),
        this,
        SLOT(handle_super_timeout())
    );
}

void Timer::interruptExpiration(){
    m_isInterrupted = true;
    QTimer::stop();
}

void Timer::handle_super_timeout(){
    if (m_isInterrupted){
        m_isInterrupted = false;
    } else {
        emit expired();
    }
}
