#ifndef TIMER_H
#define TIMER_H

#include <QTimer>


class Timer : public QTimer{
    Q_OBJECT
public:
	explicit Timer(QObject * const & parent = nullptr);

signals:
    void expired();

public slots:
    void interruptExpiration();
private slots:
    void handle_super_timeout();


private:
    bool m_isInterrupted = false;

};

#endif // TIMER_H
