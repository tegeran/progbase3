#pragma once 

constexpr const char * const GLOBAL_STYLESHEET {
	"QPushButton {"
	"	padding: 10px;"
	"	border-width: 2px;"
	"	border-style: solid;"
	"	border-color:rgb(46, 52, 54);"
	"	background-color:rgb(16, 42, 53);"
	"	color: white;"
	"}"
	"QWidget{ background-color:rgb(46, 52, 54);  color: white;}"
	"QPushButton:enabled:hover{"
	"background-color: rgb(66, 25, 99);"
	"font: bold;"
	"}"
	"QPushButton:disabled{"
	"background-color: rgba(46, 42, 42, 119);"
	"color:rgb(85, 87, 83);"
	"}"
	"QListView, QPlainTextEdit, QTextEdit"
	"	background-color: rgb(85, 87, 83);"
	"}"
};
