#include "netrequest.h"
#include <utility>

NetRequest::NetRequest(User * user, const Type & type, QByteArray && data)
	: m_user(user), m_type(type), m_data(std::move(data)){}

User * NetRequest::user() const{ return m_user; }
NetRequest::Type   NetRequest::type() const { return m_type; }
void NetRequest::setData(QByteArray && data){ m_data = data; }

void NetRequest::setUser(User * user){ m_user = user;       }
void NetRequest::setType(const Type & type){m_type = type;  }
const QByteArray & NetRequest::data() const{ return m_data; }




