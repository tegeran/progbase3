#ifndef NETREQUEST_H
#define NETREQUEST_H
#include "user.h"
#include <QByteArray>

class NetRequest
{
public:

	NetRequest(User * user, const Type & type, QByteArray && data = QByteArray());


	User * user() const;
	void setUser(User * user);

	Type type() const;
	void setType(const Type & type);

	const QByteArray & data() const;
	void setData(QByteArray && data);

private:
	User * m_user;
	Type m_type;
	QByteArray m_data;
};

#endif // NETREQUEST_H
